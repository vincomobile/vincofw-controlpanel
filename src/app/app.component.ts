import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {APP_CONFIG} from './extensions/config/app-config.model';
import {AppLoadService} from './vincofw/services/app-load.service';
import {EventAppLoadType} from './vincofw/models/event/event-app-load.model';
import {UiUxLoadService} from './ui-ux/services/ui-ux-load.service';
import {CustomLoadService} from './extensions/services/custom-load.service';
import {GlobalEventsService} from './vincofw/services/global-events.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy, AfterViewInit {
    appLoaded = 'LOADING';
    messagesLoaded = false;
    logoUrl = APP_CONFIG.APPLICATION_LOGO_URL;
    clientfullname = APP_CONFIG.CLIENT_FULL_NAME;

    constructor(
        appLoadService: AppLoadService,
        uiuxLoadService: UiUxLoadService,
        customLoadService: CustomLoadService,
        globalEventsService: GlobalEventsService
    ) {
        // FontAwesome icons
        appLoadService.addFontAwesomeIcons();
        uiuxLoadService.addFontAwesomeIcons();
        customLoadService.addFontAwesomeIcons();
        // Load events handler
        globalEventsService.loadEvents.subscribe({
            next: (event) => {
                console.log('AppComponent.events:: ' + event.eventType);
                switch (event.eventType) {
                    case EventAppLoadType.Error:
                        this.appLoaded = 'LOAD_FAIL';
                        break;
                    case EventAppLoadType.Messages:
                        this.messagesLoaded = true;
                        break;
                    case EventAppLoadType.Completed:
                        this.appLoaded = 'LOAD_COMPLETED';
                        globalEventsService.notifyAppStarted();
                        break;
                    case EventAppLoadType.ChangeTheme:
                        appLoadService.changeTheme(event.value, true);
                        break;
                    case EventAppLoadType.ChangeClient:
                        if (event.value) {
                            if (event.value.logo) {
                                APP_CONFIG.APPLICATION_LOGO_URL = event.value.logo;
                                this.logoUrl = APP_CONFIG.APPLICATION_LOGO_URL;
                            }
                            APP_CONFIG.CLIENT_FULL_NAME = event.value.name;
                            this.clientfullname = APP_CONFIG.CLIENT_FULL_NAME;
                        }
                        break;
                }
            }
        });
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }

    ngAfterViewInit(): void {
    }
}
