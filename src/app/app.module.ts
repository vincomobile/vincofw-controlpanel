import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {InjectableRxStompConfig, RxStompService, rxStompServiceFactory} from '@stomp/ng2-stompjs';
import {MomentModule} from 'ngx-moment';
import {AppRoutes} from './app-routing';
import {AppComponent} from './app.component';
import {VincofwModule} from './vincofw/vincofw.module';
import {EventAppLoadType} from './vincofw/models/event/event-app-load.model';
import {BaseSettingsModel} from './vincofw/models/base-settings.models';
import {AppLoadService} from './vincofw/services/app-load.service';
import {AuthHeaderInterceptor} from './vincofw/interceptors/auth-header.interceptor';
import {RequestLoadingInterceptor} from './vincofw/interceptors/request-loading.interceptor';
import {UiUxModule} from './ui-ux/ui-ux.module';
import {UiUxLoadService} from './ui-ux/services/ui-ux-load.service';
import {ExtensionsModule} from './extensions/extensions.module';
import {CustomLoadService} from './extensions/services/custom-load.service';
import {APP_CONFIG} from './extensions/config/app-config.model';
import {VINCOFW_RxStompConfig} from './vincofw/vincofw-constants';

// tslint:disable-next-line:typedef
export function initApplication(loadService: AppLoadService, uiuxLoadService: UiUxLoadService, customLoadService: CustomLoadService) {
    return () => {
        loadService.load(null)
            .then(appData => {
                loadService.setLoadStatus(EventAppLoadType.MainApp);
                uiuxLoadService.load(appData)
                    .then(uiuxData => {
                        loadService.setLoadStatus(EventAppLoadType.UiUx);
                        customLoadService.load(uiuxData)
                            .then(customData => {
                                loadService.setLoadStatus(EventAppLoadType.Custom);
                                loadService.setLoadStatus(EventAppLoadType.Completed);
                            })
                            .catch(error => {
                                loadService.setLoadStatus(EventAppLoadType.Error);
                            });
                    })
                    .catch(error => {
                        loadService.setLoadStatus(EventAppLoadType.Error);
                    });
            })
            .catch(error => {
                loadService.setLoadStatus(EventAppLoadType.Error);
            });
    };
}

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        CKEditorModule,
        FontAwesomeModule,
        NgbModule,
        MomentModule,
        VincofwModule,
        UiUxModule,
        ExtensionsModule,
        RouterModule.forRoot(AppRoutes, { useHash: true })
    ],
    providers: [
        AppLoadService,
        CustomLoadService,
        { provide: BaseSettingsModel, useValue: APP_CONFIG },
        { provide: HTTP_INTERCEPTORS, useClass: AuthHeaderInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: RequestLoadingInterceptor, multi: true },
        {
            provide: APP_INITIALIZER,
            useFactory: initApplication,
            deps: [AppLoadService, UiUxLoadService, CustomLoadService],
            multi: true
        },
        {
            provide: InjectableRxStompConfig,
            useValue: VINCOFW_RxStompConfig,
        },
        {
            provide: RxStompService,
            useFactory: rxStompServiceFactory,
            deps: [InjectableRxStompConfig],
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
