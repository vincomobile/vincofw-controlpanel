import {Routes} from '@angular/router';
import {LayoutContainerComponent} from './vincofw/components/layout/layout-container/layout-container.component';
import {AuthGuard} from './vincofw/interceptors/auth-guard.service';
import {TabManagerComponent} from './ui-ux/components/ui/tab-manager/tab-manager.component';

export const AppRoutes: Routes = [
    { path: 'login', component: LayoutContainerComponent },
    { path: 'home', component: LayoutContainerComponent, canActivate: [ AuthGuard ] },
    {
        path: 'tab',
        component: LayoutContainerComponent,
        canActivate: [ AuthGuard ],
        children: [
            {
                path: ':idMenu',
                component: TabManagerComponent
            }
        ]
    },
/*
    {
        path: '',
        component: LayoutContainerComponent,
        children: [
            {
                path: '',
                canActivate: [ AuthGuard ],
                loadChildren:
                    () => import('./ui-ux/ui-ux.module').then(m => m.UiUxModule)
            },
        ]
    },
*/
    { path: '**', component: LayoutContainerComponent }
];

