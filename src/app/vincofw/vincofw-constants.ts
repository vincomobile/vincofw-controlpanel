import {InjectableRxStompConfig} from '@stomp/ng2-stompjs';
import {environment} from '../../environments/environment';

export const VINCOFW_CONFIG = {

    REFERENCES: {
        TABLEDIR_CLIENT: 'A05ACA9B2E1A435CA2829CD738279512',

        TABLE_USER: 'A05ACA9B2E1A435CA2829CD738279532',

        LIST_BACKGROUND_EXECUTION_TYPE: 'ff808081539a160601539a1e472b0001',
        LIST_BACKGROUND_EXECUTION_DAYS: 'ff80808153b448ff0153b539f2ba0000',
        LIST_QUERY_LANGUAGES: 'ff808181718bf93b01718c936bec0002',
        LIST_CHART_TYPES: 'ff808181576c6b2801576c7a9b23003e'
    },

    TABLES: {
        AD_AUDIT: 'ff8081815f7c855f015f7c8e30d30004',
        AD_FILE: 'ff808081526d9f5c01526dce31a2007e',
        AD_IMAGE: 'ff808081526d9f5c01526dcc22e4005a',
        AD_MODULE: 'ff80808151382f2a0151385f138f0040'
    },

    TABS: {
        TAB_FORM_DESIGNER: 'ff8081815ed7f956015ed7fd60e70000'
    },

    sqlDependReferences: ['TABLE', 'TABLEDIR'],

    rabbitTopic: '/topic',
    rabbitTopicProcess: '/Process_'

};

export const VINCOFW_RxStompConfig: InjectableRxStompConfig = {
    // Which server?
    brokerURL: environment.rabbitUrl,

    // Headers
    // Typical keys: login, passcode, host
    connectHeaders: {
        login: environment.rabbitLogin,
        passcode: environment.rabbitPassword,
        host: '/'
    },

    // How often to heartbeat?
    // Interval in milliseconds, set to 0 to disable
    heartbeatIncoming: 0, // Typical value 0 - disabled
    heartbeatOutgoing: 20000, // Typical value 20000 - every 20 seconds

    // Wait in milliseconds before attempting auto reconnect
    // Set to 0 to disable
    // Typical value 500 (500 milli seconds)
    reconnectDelay: 200,

    // Will log diagnostics on console
    // It can be quite verbose, not recommended in production
    // Skip this key to stop logging to console
    debug: (msg: string): void => {
        console.log(new Date(), msg);
    },
};
