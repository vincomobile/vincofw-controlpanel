import {
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpResponse
} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {NotificationService} from '../../ui-ux/services/notification.service';
import {AppCacheService} from '../services/cache.service';
import {AuthService} from '../services/auth.service';

@Injectable({
    providedIn: 'root'
})
export class RequestLoadingInterceptor implements HttpInterceptor {

    constructor(
        private router: Router,
        private authService: AuthService,
        private notificationService: NotificationService,
        private cacheService: AppCacheService
    ) {

    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // TODO: Block UI
        return next.handle(req.clone()).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    // TODO: Unblock UI
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                // TODO: Unblock UI
                let msg = null;
                if (error.status === 0) {
                    if (error.name && error.name === 'HttpErrorResponse') {
                        msg = 'AD_GlobalServerConnectionError';
                    }
                } else if (error.status >= 400 && error.status < 599) {
                    if (
                        (typeof error.error === 'string' && error.error.indexOf('com.auth0.jwt.exceptions.TokenExpiredException') > 0) ||
                        (error.error.errCode === -1 || error.error.message !== 'INVALID_CREDENTIALS')
                    ) {
                        msg = this.checkLogin();
                    } else {
                        msg = 'AD_GlobalServerConnectionError';
                    }
                }
                if (msg !== null) {
                    this.notificationService.showError(this.cacheService.getTranslation(msg));
                }
                return throwError(error);
            })
        );
    }

    private checkLogin(): string {
        if (this.authService.isExpiredToken()) {
            return 'AD_GlobalServerConnectionError';
        }
        this.router.navigate(['/login']);
        return 'AD_GlobalUserNotAuth';
    }
}
