import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from '../services/auth.service';

/**
 * Checks if the route can be activated.
 * A route can be activated if the user is logged in
 */
@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(
        private authService: AuthService,
        private router: Router
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
        return new Promise<boolean>((resolve) => {
            const isLogged = this.authService.isLogged();
            if (!isLogged) {
                this.authService.autoLogin().then(success => {
                   if (!success) {
                       this.redirectToLogin();
                   }
                   resolve(success);
                });
            } else {
                resolve(true);
            }
        });
    }

    redirectToLogin(): void {
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
            this.router.navigate(['/login'])
        });
    }
}
