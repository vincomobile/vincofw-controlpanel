import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {LocalStoreService} from '../services/local-store.service';
import {AuthService} from '../services/auth.service';

@Injectable({
    providedIn: 'root'
})
export class AuthHeaderInterceptor implements HttpInterceptor {

    constructor(
        private authService: AuthService,
        private localStoreService: LocalStoreService
    ) {

    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.headers && req.headers.get('Authorization')) {
            return next.handle(req);
        } else {
            const jwtToken = this.localStoreService.getItem('jwtToken');
            if (jwtToken) {
                if (!this.authService.isExpiredToken()) {
                    const cloned = req.clone({
                        headers: req.headers.set('Authorization', 'Bearer ' + jwtToken)
                    });
                    return next.handle(cloned);
                } else {
                    const userInfo = this.authService.getUserInfo();
                    if (userInfo && userInfo.rememberMe) {
                        this.authService.autoLogin().then(success => {
                            if (!success) {
                                this.authService.clearCredentials();
                            }
                        });
                    } else {
                        this.authService.clearCredentials();
                    }
                }
            }
            return next.handle(req);
        }
    }
}
