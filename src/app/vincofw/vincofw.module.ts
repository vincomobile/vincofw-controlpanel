import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {VincoHostDirective} from './directives/vinco-host.directive';
import {VincoHostFirstDirective} from './directives/vinco-host-first.directive';
import {VincoHostFooterDirective} from './directives/vinco-host-footer.directive';
import {VincoHostSecondDirective} from './directives/vinco-host-second.directive';
import {VincoHostFifthDirective} from './directives/vinco-host-fifth.directive';
import {VincoHostFourthDirective} from './directives/vinco-host-fourth.directive';
import {VincoHostThirdDirective} from './directives/vinco-host-third.directive';
import {VincoHostTabDirective} from './directives/vinco-host-tab.directive';
import {VincoHostTabCustomDirective} from './directives/vinco-host-tab-custom.directive';
import {VincoHostWindowDirective} from './directives/vinco-host-window.directive';
import {ReferenceValuePipe} from './pipes/reference.value.pipe';
import {ReferenceRowValuePipe} from './pipes/reference-row-value.pipe';
import {SafeHtmlPipe} from './pipes/safe-html.pipe';
import {SafeStylePipe} from './pipes/safe-style.pipe';
import {TranslatePipe} from './pipes/translate.pipe';
import {NotFoundComponent} from './components/ui/not-found/not-found.component';
import {LayoutContainerComponent} from './components/layout/layout-container/layout-container.component';
import {LoginFormComponent} from './components/authentification/login-form.component';
import {EmptyComponent} from './components/ui/empty/empty.component';
import {CustomTabComponent} from './components/ui/widgets/custom-tab.component';
import {WidgetComponent} from './components/ui/widgets/widget.component';
import {WidgetBaseFormComponent} from './components/ui/widgets/widget-base-form.component';
import {WidgetTableBaseComponent} from './components/ui/widgets/widget-table-base.component';
import {NotificationBaseComponent} from './components/ui/notifications/notification-base.component';

@NgModule({
    declarations: [
        // Directives
        VincoHostDirective,
        VincoHostFirstDirective,
        VincoHostFooterDirective,
        VincoHostSecondDirective,
        VincoHostThirdDirective,
        VincoHostFourthDirective,
        VincoHostFifthDirective,
        VincoHostTabDirective,
        VincoHostTabCustomDirective,
        VincoHostWindowDirective,
        // Pipes
        ReferenceValuePipe,
        ReferenceRowValuePipe,
        SafeHtmlPipe,
        SafeStylePipe,
        TranslatePipe,
        // Components
        LayoutContainerComponent,
        LoginFormComponent,
        // Components - UI
        EmptyComponent,
        NotificationBaseComponent,
        NotFoundComponent,
        // Components - UI - Widgets
        CustomTabComponent,
        WidgetComponent,
        WidgetBaseFormComponent,
        WidgetTableBaseComponent
    ],
    entryComponents: [
        EmptyComponent,
        NotFoundComponent
    ],
    imports: [
        CommonModule,
        NgbModule,
        HttpClientModule,
        RouterModule
    ],
    providers: [],
    exports: [
        // Directives
        VincoHostDirective,
        VincoHostFirstDirective,
        VincoHostFooterDirective,
        VincoHostSecondDirective,
        VincoHostThirdDirective,
        VincoHostFourthDirective,
        VincoHostFifthDirective,
        VincoHostTabDirective,
        VincoHostTabCustomDirective,
        VincoHostWindowDirective,
        // Pipes
        ReferenceValuePipe,
        ReferenceRowValuePipe,
        SafeHtmlPipe,
        SafeStylePipe,
        TranslatePipe,
        // Components
        LayoutContainerComponent,
        LoginFormComponent,
        // Components - UI
        EmptyComponent,
        NotificationBaseComponent,
        NotFoundComponent,
        // Components - UI - Widgets
        CustomTabComponent,
        WidgetComponent,
        WidgetBaseFormComponent,
        WidgetTableBaseComponent
    ]
})
export class VincofwModule { }
