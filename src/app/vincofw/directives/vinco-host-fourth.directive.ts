import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[vincoHostFourth]'
})
export class VincoHostFourthDirective {

    constructor(public viewContainerRef: ViewContainerRef) { }

}
