import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[vincoHostWindow]'
})
export class VincoHostWindowDirective {

    constructor(public viewContainerRef: ViewContainerRef) { }

}
