import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[vincoHostSecond]'
})
export class VincoHostSecondDirective {

    constructor(public viewContainerRef: ViewContainerRef) { }

}
