import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[vincoHostTab]'
})
export class VincoHostTabDirective {

    constructor(public viewContainerRef: ViewContainerRef) { }

}
