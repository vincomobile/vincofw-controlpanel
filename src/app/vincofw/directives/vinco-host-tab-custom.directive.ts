import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[vincoHostTabCustom]'
})
export class VincoHostTabCustomDirective {

    constructor(public viewContainerRef: ViewContainerRef) { }

}
