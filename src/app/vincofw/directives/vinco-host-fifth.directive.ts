import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[vincoHostFifth]'
})
export class VincoHostFifthDirective {

    constructor(public viewContainerRef: ViewContainerRef) { }

}
