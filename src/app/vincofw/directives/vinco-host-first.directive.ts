import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[vincoHostFirst]'
})
export class VincoHostFirstDirective {

    constructor(public viewContainerRef: ViewContainerRef) { }

}
