import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[vincoHost]'
})
export class VincoHostDirective {

    constructor(public viewContainerRef: ViewContainerRef) { }

}
