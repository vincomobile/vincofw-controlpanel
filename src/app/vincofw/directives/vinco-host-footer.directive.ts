import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[vincoHostFooter]'
})
export class VincoHostFooterDirective {

    constructor(public viewContainerRef: ViewContainerRef) { }

}
