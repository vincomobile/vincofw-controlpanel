import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[vincoHostThird]'
})
export class VincoHostThirdDirective {

    constructor(public viewContainerRef: ViewContainerRef) { }

}
