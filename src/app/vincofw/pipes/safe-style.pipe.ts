import {DomSanitizer, SafeStyle} from '@angular/platform-browser';
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'safeStyle'})
export class SafeStylePipe implements PipeTransform {
    constructor(protected sanitized: DomSanitizer) {
    }

    transform(value): SafeStyle {
        return this.sanitized.bypassSecurityTrustStyle(value);
    }
}

