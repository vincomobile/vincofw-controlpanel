import {Pipe, PipeTransform} from '@angular/core';
import {AppCacheService} from '../services/cache.service';

/**
 * Translates the given key into the actual value
 * Takes an argument defining the key to search for as well as the reference ID where to search the key
 * This pipe returns an Observable<string>, so it should be used with async
 * Usage:
 *      key | referenceValue : idReference | async
 * Example
 *      {{ 'KK11EB083EC7E8C4891CD7B25B1E274E' | referenceValue : 'RRR1EB083EC7E8C4891CD7B25B1E274E' | async }}
 *      will search the reference RRR1EB083EC7E8C4891CD7B25B1E274E and try to obtain the value of the key KK11EB083EC7E8C4891CD7B25B1E274E
 */
@Pipe({
    name: 'referenceValue'
})
export class ReferenceValuePipe implements PipeTransform {

    constructor(private cacheService: AppCacheService) {

    }

    /**
     * Visualizes the transformed value of the reference
     *
     * @param key Reference key to transform
     * @param idReference Reference ID
     */
    transform(key: string, idReference: string): any {
        return this.cacheService.getReferenceValue(key, idReference);
    }
}
