import {Pipe, PipeTransform} from '@angular/core';
import {AppCacheService} from "../services/cache.service";

/**
 * Translates the given key into the actual value
 * Takes an argument defining whether the translated messages should be obtained from web or admin path
 * By default, takes the messages from the admin path
 * Usage:
 *      value | translate
 * Example
 *      {{ 'AD_WeekDayThursday' | translate }}
 *      formats to Thursday if the active language is English
 */
@Pipe({
    name: 'translate'
})
export class TranslatePipe implements PipeTransform {

    constructor(private cacheService: AppCacheService) {

    }

    /**
     * Performs a translation
     *
     * @param value Value to be translated
     * @param args Message arguments
     */
    transform(value: string, args: any[] = null): any {
        return this.cacheService.getTranslation(value, args);
    }
}
