import {Component, Input, OnInit} from '@angular/core';
import {VincoComponent} from '../../../models/component-register.model';

@Component({
  selector: 'vincofw-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements VincoComponent, OnInit {

    @Input() data: any;

    constructor() { }

    ngOnInit(): void {
    }

}
