import {Component, OnInit} from '@angular/core';
import {AdClientDtoModel, AdClientUiuxModel} from '../../../models/dto/ad-client-dto.model';
import {AuthService} from '../../../services/auth.service';
import {ToolService} from '../../../services/tool.service';
import {LocalStoreService} from "../../../services/local-store.service";

@Component({
    template: ``
})
export class NotificationBaseComponent implements OnInit {

    uiux: AdClientUiuxModel;

    constructor(
        protected localStore: LocalStoreService,
        protected authService: AuthService
    ) {

    }

    ngOnInit(): void {
        let clientGUI = this.authService.getClientGUI()
        if (ToolService.isNullOrUndefined(clientGUI)) {
            clientGUI = JSON.parse(this.localStore.getItem('clientGUI'));
            if (ToolService.isNullOrUndefined(clientGUI)) {
                clientGUI = new AdClientDtoModel();
                clientGUI.guiUx = 'tabs_fixed';
                clientGUI.primaryColor = 'blue';
                clientGUI.primaryColorVariant = '500';
                clientGUI.secondaryColor = 'blue';
                clientGUI.secondaryColorVariant = '200';
                clientGUI.msgInfoColor = 'blue';
                clientGUI.msgInfoColorVariant = '400';
                clientGUI.msgSuccessColor = 'green';
                clientGUI.msgSuccessColorVariant = '800';
                clientGUI.msgWarningColor = 'yellow';
                clientGUI.msgWarningColorVariant = '800';
                clientGUI.msgErrorColor = 'red';
                clientGUI.msgErrorColorVariant = 'A700';
            }
        }
        this.uiux = new AdClientUiuxModel(clientGUI);
    }

}
