import {Component, Input, OnInit} from '@angular/core';
import {VincoComponent} from '../../../models/component-register.model';

@Component({
  selector: 'vincofw-empty',
  templateUrl: './empty.component.html',
  styleUrls: ['./empty.component.scss']
})
export class EmptyComponent implements VincoComponent, OnInit {

    @Input() data: any;

    constructor() { }

    ngOnInit(): void {
    }

}
