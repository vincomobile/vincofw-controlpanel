import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {AdReferenceDtoModel} from '../../../models/dto/ad-reference-dto.model';
import {AdClientUiuxModel} from '../../../models/dto/ad-client-dto.model';
import {AdTabMainDtoModel} from '../../../models/dto/ad-tab-main-dto.model';
import {AdFieldDtoModel} from '../../../models/dto/ad-field-dto.model';
import {ToolService} from '../../../services/tool.service';
import {AutogenService} from '../../../services/autogen.service';
import {AppCacheService} from '../../../services/cache.service';
import {NotificationService} from '../../../../ui-ux/services/notification.service';
import {AuthService} from '../../../services/auth.service';

@Component({
    template: ``
})
export class WidgetComponent implements OnInit, OnDestroy {

    protected service: AutogenService;
    protected initialize = false;
    protected subscriptions: Subscription[] = [];

    tab: AdTabMainDtoModel;
    loadedReferences: AdReferenceDtoModel[];
    title: string;
    uiux: AdClientUiuxModel;
    filterFields: AdFieldDtoModel[] = [];

    @Input()
    set relatedTab(tab: AdTabMainDtoModel) {
        console.log('WidgetComponent (' + this.constructor.name + '): relatedTab(' + tab.name + ')');
        if (ToolService.isNullOrUndefined(tab.tab)) {
            this.cacheService.getTabInfo(tab.idWindow, tab.idTab).subscribe(data => {
                this.loadRelatedTab(data);
            });
        } else {
            this.loadRelatedTab(tab);
        }
    }

    @Input()
    set tabTitle(title: string) {
        this.title = title;
    }

    constructor(
        protected authService: AuthService,
        protected notificationService: NotificationService,
        protected cacheService: AppCacheService,
        protected autogenService: AutogenService
    ) {
        console.log(this.constructor.name + ' constructor ...');
    }

    ngOnInit(): void {
        console.log('WidgetComponent (' + this.constructor.name + '): ngOnInit(' + (this.tab ? this.tab.name : '') + ')');
        this.uiux = new AdClientUiuxModel(this.authService.getClientGUI());
        this.initialize = true;
    }

    ngOnDestroy(): void {
        console.log('WidgetComponent (' + this.constructor.name + '): ngOnDestroy(' + (this.tab ? this.tab.name : '') + ')');
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    /**
     * Execute when the related tab is loaded
     */
    protected relatedTabLoaded(): void {

    }

    private loadRelatedTab(tab: AdTabMainDtoModel): void {
        if (ToolService.isNullOrUndefined(this.tab) || this.tab.tab.table.idTable !== tab.tab.table.idTable) {
            this.tab = tab;
            if (tab.ttype !== 'USERDEFINED' && tab.ttype !== 'CHART') {
                this.service = this.autogenService.bindedService(tab.tab.table);
                const referenceIds = this.tab.tab.table.columns.map(current => current.reference.idReference);
                const distinct = [];
                referenceIds.forEach(current => {
                    if (ToolService.isNullOrUndefined(distinct.find(r => r === current))) {
                        distinct.push(current);
                    }
                });
                this.cacheService.getReferenceInfo(distinct).subscribe(references => {
                    this.loadedReferences = references;
                    this.tab.tab.table.columns.forEach(column => {
                        if (column.reference.rtype === 'BUTTON' || column.reference.rtype === 'LIST'
                            || column.reference.rtype === 'TABLE' || column.reference.rtype === 'TABLEDIR'
                        ) {
                            const ref = this.loadedReferences.find(reference => reference.idReference === column.reference.idReference);
                            if (ref) {
                                column.reference.refButton = ref.refButton;
                                column.reference.refList = ref.refList;
                                column.reference.refTable = ref.refTable;
                            }
                        }
                    });
                    this.buildFilters();
                });
            }
            if (tab.ttype === 'CHART') {
                this.buildFilters();
            }
        }
    }

    private buildFilters() {
        if (this.tab.tab.table && this.tab.tab.table.idTable) {
            const filterFields = this.tab.tab.fields.filter(
                fld => !fld.relatedColumn.groupedsearch && fld.showinsearch && fld.columnReferenceType !== 'FILE' && fld.columnReferenceType !== 'IMAGE' && fld.columnReferenceType !== 'IMAGEENCODE'
            ).sort((fld1, fld2) => fld1.filterSeqno >= fld2.filterSeqno ? 1 : -1);
            filterFields.forEach(fld => {
                const newField = new AdFieldDtoModel();
                newField.copyToFilter(fld);
                this.filterFields.push(newField);
            });
            const groupedSearchFields = this.tab.tab.table.columns.filter(column => column.groupedsearch);
            if (groupedSearchFields.length > 0) {
                const field = new AdFieldDtoModel();
                field.copyToFilterValues({
                    idField: 'SEARCH',
                    idColumn: '$search$',
                    caption: '',
                    filterSpan: 1,
                    filterSeqno: 0,
                    displaylogic: null,
                    filterRange: false,
                    filterDefaultValue: null,
                    filterDefaultValue2: null,
                    valuemin: null,
                    valuemax: null,
                    columnName: '$search$',
                    columnReferenceType: 'FLT_SEARCH'
                });
                this.filterFields.splice(0, 0, field);
            }
        } else if (this.tab.tab.chartFilters) {
            this.tab.tab.chartFilters.forEach(flt => {
                const field = new AdFieldDtoModel();
                field.copyToFilterValues({
                    idField: flt.idChartFilter,
                    idColumn: flt.idChartFilter,
                    caption: flt.caption,
                    filterSpan: 1,
                    filterSeqno: flt.seqno,
                    displaylogic: flt.displaylogic,
                    filterRange: flt.ranged,
                    filterDefaultValue: flt.valuedefault,
                    filterDefaultValue2: flt.valuedefault2,
                    valuemin: flt.valuemin,
                    valuemax: flt.valuemax,
                    columnName: flt.name,
                    columnReferenceType: flt.reference.rtype
                });
                this.filterFields.push(field);
            });
        }
        this.relatedTabLoaded();
    }
}
