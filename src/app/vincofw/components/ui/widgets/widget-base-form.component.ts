import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {AsyncSubject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {AdTabMainDtoModel} from '../../../models/dto/ad-tab-main-dto.model';
import {AdFormEditorModel} from '../../../models/ad/ad-form-editor.model';
import {AdFieldDtoModel} from '../../../models/dto/ad-field-dto.model';
import {AutogenUsedinchildModel} from '../../../models/autogen.model';
import {AdHookNames} from '../../../models/hook.model';
import {AdFieldGroupDtoModel} from '../../../models/dto/ad-field-group-dto.model';
import {AdBaseModel} from '../../../models/ad/ad-base.model';
import {AdReferenceKeyValueModel} from '../../../models/dto/ad-reference-key-value.model';
import {AuthService} from '../../../services/auth.service';
import {NotificationService} from '../../../../ui-ux/services/notification.service';
import {AppCacheService} from '../../../services/cache.service';
import {AutogenService} from '../../../services/autogen.service';
import {ConditionalService} from '../../../services/conditional.service';
import {UtilitiesService} from '../../../services/utilities.service';
import {AdReferenceService} from '../../../services/ad/ad-reference.service';
import {HookService} from '../../../services/hook.service';
import {GlobalEventsService} from '../../../services/global-events.service';
import {ToolService} from '../../../services/tool.service';
import {WidgetComponent} from './widget.component';
import {VINCOFW_CONFIG} from '../../../vincofw-constants';

@Component({
    template: ``
})
export class WidgetBaseFormComponent extends WidgetComponent {

    protected parentTab: AdTabMainDtoModel;

    formCaption = '';
    fieldIdentifier: string;
    isNew: boolean = null;
    usedWhenNew = false;
    autogenForm: FormGroup;
    currentItem: any = null;
    currentItemCompleted: boolean;
    serverErrorMsg: string = null;
    editors: AdFormEditorModel[];
    customButtons: AdFieldDtoModel[];
    pkField: AdFieldDtoModel;
    fields: AdFieldDtoModel[];

    @Input() parentItem: any;
    @Input() delay = 300;
    @Input() usedInChild: AutogenUsedinchildModel[];

    @Output() listItems: EventEmitter<any> = new EventEmitter();

    constructor(
        authService: AuthService,
        notificationService: NotificationService,
        cacheService: AppCacheService,
        autogenService: AutogenService,
        protected conditionalService: ConditionalService,
        protected utilitiesService: UtilitiesService,
        protected referenceService: AdReferenceService,
        protected hookService: HookService,
        protected globalEventsService: GlobalEventsService
    ) {
        super(authService, notificationService, cacheService, autogenService);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.customButtons = [];
        this.editors = [];
        if (ToolService.isNullOrUndefined(this.usedInChild)) {
            this.usedInChild = [];
        }
    }

    /**
     * Cancel element modifications and return to list
     */
    doCancel(): void {
        if (this.tab.tab.defaultEditor) {
            if (this.autogenForm.dirty) {
                const dialogRef = this.notificationService.confirmDialog(
                    this.cacheService.getTranslation('AD_GlobalConfirm'),
                    this.cacheService.getTranslation('AD_msgFormCancelChanges'),
                    'warning'
                );
                dialogRef.afterClosed().subscribe(result => {
                    if (result === 'OK') {
                        this.listItems.emit(null);
                    }
                });
            } else {
                this.listItems.emit(null);
            }
        }
    }

    /**
     * Get field to be edited
     */
    protected getFormFields(): AdFieldDtoModel[] {
        return [];
    }

    /**
     * Called when the form is build and ready to shown
     */
    protected formReady(): void {

    }

    /**
     * Execute when the related tab is loaded
     */
    protected relatedTabLoaded(): void {
        this.hookService.execHook(
            AdHookNames.AD_AFTER_LOAD_TAB, {
                tab: this.tab.tab
            }
        ).then(() => {
            // Create reactive form
            const formElements = {};
            const formOnChangeElements: AdFieldDtoModel[] = [];
            const subjectReferences = new AsyncSubject();
            this.fields = this.getFormFields();
            this.fields.forEach(fld => fld.loadComplete = (!fld.displayed || fld.relatedColumn.reference.rtype !== 'TABLEDIR'));
            this.fields.forEach(fld => {
                if (fld.displayed) {
                    // Filter custom buttons
                    if (fld.relatedColumn.reference.rtype === 'BUTTON' && fld.relatedColumn.reference.refButton.showMode === 'IN_FORM') {
                        fld.runtimeDisplayed = fld.displayed;
                        fld.runtimeDisplaylogic = fld.displaylogic;
                        fld.displayListeners = [];
                        this.customButtons.push(fld);
                    }
                    // Form elements
                    if (fld.relatedColumn.reference.rtype !== 'BUTTON') {
                        formElements[fld.relatedColumn.name] = this.utilitiesService.buildFormControl(
                            fld.relatedColumn.reference.rtype, fld.relatedColumn.mandatory,
                            fld.valuemin ? Number(fld.valuemin) : null,
                            fld.valuemax ? Number(fld.valuemax) : null,
                            fld.relatedColumn.lengthMin, fld.relatedColumn.lengthMax);
                        if (fld.relatedColumn.reference.rtype === 'TABLEDIR') {
                            this.cacheService.getReferenceValues(fld.relatedColumn.reference, this.parentItem, true).subscribe(() => {
                                fld.loadComplete = true;
                                subjectReferences.next(null);
                                const pend = this.fields.find(f => !f.loadComplete);
                                if (!pend) {
                                    subjectReferences.complete();
                                }
                            });
                        }
                        // Set editors
                        this.editors.push(new AdFormEditorModel(fld.relatedColumn.name));
                        if (fld.onchangefunction) {
                            formOnChangeElements.push(fld);
                        }
                    }
                }
                if (fld.relatedColumn.primaryKey) {
                    this.pkField = fld;
                }
            });
            const pending = this.fields.find(f => !f.loadComplete);
            if (!pending) {
                subjectReferences.next(null);
                subjectReferences.complete();
            }

            // Set editor dependency
            this.fields.forEach(fld => {
                const editor = this.editors.find(edt => edt.name === fld.columnName);
                if (editor) {
                    if (fld.displayed && fld.displaylogic) {
                        this.utilitiesService.addFieldDependency(
                            editor, this.conditionalService.getListenersField(fld.displaylogic), 'displayListeners'
                        );
                    }
                    if (!fld.readonly && fld.relatedColumn.readonlylogic) {
                        this.utilitiesService.addFieldDependency(
                            editor, this.conditionalService.getListenersField(fld.relatedColumn.readonlylogic), 'readonlyListeners'
                        );
                    }
                    if (VINCOFW_CONFIG.sqlDependReferences.indexOf(fld.relatedColumn.reference.rtype) >= 0) {
                        this.utilitiesService.addFieldDependency(
                            editor, this.conditionalService.getListenersField(fld.relatedColumn.reference.refTable.sqlwhere), 'sqlListeners'
                        );
                    }
                } else if (fld.relatedColumn.reference.rtype === 'BUTTON') {
                    const button = this.customButtons.find(btn => btn.columnName === fld.columnName);
                    if (button) {
                        this.utilitiesService.addFieldDependency(
                            button, this.conditionalService.getListenersField(fld.runtimeDisplaylogic), 'displayListeners'
                        );
                    }
                }
            });
            // Set field group dependency
            this.tab.tab.fieldGroups.forEach(group => {
                if (group.displaylogic) {
                    const fields = this.conditionalService.getListenersField(group.displaylogic);
                    fields.forEach(fld => {
                        const editor: AdFormEditorModel = this.editors.find(edt => edt.name === fld);
                        if (editor) {
                            this.utilitiesService.addFieldDependency(editor, [group.idFieldGroup], 'groupFieldListeners');
                        }
                    });
                }
            });
            // Set parent tab
            const window = this.cacheService.getWindowFromCache(this.tab.idWindow);
            this.parentTab = this.cacheService.getParentTab(window.mainTab, this.tab.idTab);

            subjectReferences.subscribe(() => {
                this.autogenForm = new FormGroup(formElements);
                // Listening on controls with 'onchangefunction' defined
                formOnChangeElements.forEach(fld => {
                    this.autogenForm.controls[fld.columnName].valueChanges.pipe(
                        debounceTime(this.delay),
                        distinctUntilChanged()
                    ).subscribe(data => {
                        this.onChangeFunction(fld, data);
                    });
                });
                // Listening to the changes in the form
                this.autogenForm.valueChanges.pipe(
                    debounceTime(this.delay),
                    distinctUntilChanged()
                ).subscribe(() => {
                    this.notifyChanges(this.getItemToSave());
                });
                this.formReady();
            });
        });
    }

    /**
     * Adjust field visibility
     *
     * @param field Field
     * @param data Form data
     */
    protected adjustFieldDisplayLogic(field: AdFieldDtoModel, data: any): void {
        field.runtimeDisplaylogic = this.conditionalService.replaceJSConditional(field.runtimeDisplaylogic, field.relatedColumn, null, data);
        if (this.conditionalService.isCompleteConditional(field.runtimeDisplaylogic)) {
            const isNew = this.isNew; // Do not remove (it will be used in expression evaluator)
            // tslint:disable-next-line:no-eval
            field.runtimeDisplayed = eval(field.runtimeDisplaylogic);
            field.runtimeDisplaylogic = field.displaylogic;
        } else {
            field.runtimeDisplayed = false;
        }
    }

    /**
     * Change tab visibility
     *
     * @param data Form data
     */
    protected adjustTabDisplayLogic(data: any): void {
        this.tab.visibleTabs.forEach(tab => {
            tab.displayed = true;
            if (this.usedWhenNew) {
                tab.displayed = !ToolService.isNullOrUndefined(tab.usedWhenNew) && tab.usedWhenNew;
            }
            if (tab.displaylogic) {
                const displayLogic = this.conditionalService.replaceJSConditional(tab.displaylogic, null, null, data);
                // tslint:disable-next-line:no-eval
                tab.displayed = eval(displayLogic);
            }
        });
    }

    /**
     * Adjust group fields visibility
     *
     * @param group Group fields
     * @param data Form data
     */
    protected adjustGroupFieldDisplayLogic(group: AdFieldGroupDtoModel, data: any): void {
        group.runtimeDisplaylogic = this.conditionalService.replaceJSConditional(group.runtimeDisplaylogic, null, null, data);
        if (this.conditionalService.isCompleteConditional(group.runtimeDisplaylogic)) {
            const isNew = this.isNew; // Do not remove (it will be used in expression evaluator)
            // tslint:disable-next-line:no-eval
            group.runtimeDisplayed = eval(group.runtimeDisplaylogic);
            group.runtimeDisplaylogic = group.displaylogic;
        } else {
            group.runtimeDisplayed = false;
        }
    }

    /**
     * Adjust field read only
     *
     * @param field Field
     * @param data Form data
     */
    protected adjustFieldReadonlyLogic(field: AdFieldDtoModel, data: any): void {
        if (this.tab.tab.uipattern === 'READONLY') {
            field.runtimeReadonly = true;
        } else if (ToolService.isNullOrUndefined(field.runtimeReadonlylogic) || field.runtimeReadonlylogic === '') {
            field.runtimeReadonly = field.readonly;
        } else {
            field.runtimeReadonlylogic = this.conditionalService.replaceJSConditional(field.runtimeReadonlylogic, field.relatedColumn, null, data);
            if (this.conditionalService.isCompleteConditional(field.runtimeReadonlylogic)) {
                const isNew = this.isNew; // Do not remove (it will be used in expression evaluator)
                // tslint:disable-next-line:no-eval
                field.runtimeReadonly = eval(field.runtimeReadonlylogic);
                field.runtimeReadonlylogic = field.relatedColumn.readonlylogic;
            } else {
                field.runtimeReadonly = true;
            }
        }
    }

    /**
     * Field changes listener and has linked 'onChangeFunction'
     *
     * @param field Field
     * @param data Data changed
     */
    protected onChangeFunction(field: AdFieldDtoModel, data: any): void {
        if (field.relatedColumn.reference.rtype === 'TABLE') {
            if (ToolService.isNullOrUndefined(data) || ToolService.isNullOrUndefined(data.key) || data.key === this.currentItem[field.columnName]) {
                return;
            }
        }
        this.globalEventsService.notifyFormFieldOnChangeFunction(
            field.onchangefunction, this.isNew, this.autogenForm,
            this.getItemToSave(), data, field, this.tab.tab.fields
        );
    }

    /**
     * Get item values to save register
     */
    protected getItemToSave(): AdBaseModel {
        const itemToSave = new AdBaseModel();
        this.tab.tab.fields.forEach(fld => {
            const key = fld.columnName;
            if (this.autogenForm.get(key) !== null) {
                if (this.autogenForm.value[key] instanceof AdReferenceKeyValueModel) {
                    itemToSave[key] = this.autogenForm.value[key].key;
                } else {
                    if (fld.relatedColumn.reference.rtype === 'TABLE' && fld.readonly) {
                        itemToSave[key] = this.currentItem[key];
                    } else {
                        itemToSave[key] = this.utilitiesService.getFormValue(this.autogenForm.value[key], fld);
                    }
                }
            } else {
                itemToSave[key] = this.utilitiesService.getFormValue(this.currentItem[key], fld);
            }
        });
        return itemToSave;
    }

    /**
     * Show server error
     *
     * @param error Response error
     */
    protected showServerError(error: HttpErrorResponse): void {
        if (error.error) {
            this.serverErrorMsg = this.utilitiesService.getValidationsErrors(error, this.tab.tab.table.columns, this.tab.tab.fields);
        } else {
            this.serverErrorMsg = error.message ? error.message : null;
        }
        if (!this.serverErrorMsg) {
            this.serverErrorMsg = this.cacheService.getTranslation('AD_ErrValidationServerError');
        }
    }

    /**
     * Form changes listener
     *
     * @param data Form data
     */
    protected notifyChanges(data: any): void {
        this.editors.forEach(edt => {
            if (edt.isModify(data)) {
                // Read only logic
                const dependReadonlyEditors = this.editors.filter(depEdt => depEdt.readonlyListeners.find(lst => lst === edt.name));
                dependReadonlyEditors.forEach(depEdt => {
                    const field = this.tab.tab.fields.find(fld => fld.columnName === depEdt.name);
                    this.adjustFieldReadonlyLogic(field, data);
                });
                // Field Display logic
                const dependDisplayEditors = this.editors.filter(depEdt => depEdt.displayListeners.find(lst => lst === edt.name));
                dependDisplayEditors.forEach(depEdt => {
                    const field = this.tab.tab.fields.find(fld => fld.columnName === depEdt.name);
                    this.adjustFieldDisplayLogic(field, data);
                });
                const dependDisplayButtons = this.customButtons.filter(depEdt => depEdt.displayListeners.find(lst => lst === edt.name));
                dependDisplayButtons.forEach(depEdt => {
                    const field = this.tab.tab.fields.find(fld => fld.columnName === depEdt.columnName);
                    this.adjustFieldDisplayLogic(field, data);
                });
                // Group field Display logic
                edt.groupFieldListeners.forEach(idFieldGroup => {
                    const group = this.tab.tab.fieldGroups.find(g => g.idFieldGroup === idFieldGroup);
                    this.adjustGroupFieldDisplayLogic(group, data);
                });
                // Tab Display logic
                edt.tabListeners.forEach(idTab => {
                    const tab = this.tab.visibleTabs.find(t => t.idTab === idTab);
                    const displayLogic = this.conditionalService.replaceJSConditional(tab.displaylogic, null, null, data);
                    // tslint:disable-next-line:no-eval
                    tab.displayed = eval(displayLogic);
                });
                // SQL where logic
                const dependSQLEditors = this.editors.filter(depEdt => depEdt.sqlListeners.find(lst => lst === edt.name));
                dependSQLEditors.forEach(depEdt => {
                    const field = this.tab.tab.fields.find(fld => fld.columnName === depEdt.name);
                    if (field.runtimeDisplayed) {
                        if (field.relatedColumn.reference.rtype === 'TABLEDIR') {
                            this.referenceService.loadTabledir(this.tab.tab.fields, field.relatedColumn.reference, this.getItemToSave()).subscribe( values => {
                                this.hookService.execHook(
                                    AdHookNames.AD_LIST_CHANGE_VALUES, {
                                        fieldName: field.columnName,
                                        values: values
                                    }
                                ).then();
                            });
                        } else if (field.relatedColumn.reference.rtype === 'TABLE') {
                            this.currentItem = this.getItemToSave();
                            field.relatedColumn.reference.refTable.runtimeSqlwhere = null;
                        }
                    }
                });
            }
        });
    }

    /**
     * Get edit item with default values
     *
     * @param item Pre loaded values
     */
    protected getDefaultItem(item: any): any {
        item = item || {};
        this.tab.tab.fields.forEach(fld => {
            if (fld.valuedefault) {
                const value = this.conditionalService.replaceJSConditional(fld.valuedefault, null, null, this.parentItem);
                // tslint:disable-next-line:no-eval
                item[fld.columnName] = fld.relatedColumn.reference.rtype === 'YESNO' ? eval(value) : value;
            } else if (ToolService.isNullOrUndefined(item[fld.columnName])) {
                item[fld.columnName] = fld.relatedColumn.reference.rtype === 'YESNO' ? false : null;
            }
        });
        let linkedToParent = this.tab.tab.table.columns.filter(column => column.linkParent);
        if (linkedToParent.length > 1 && this.parentTab) {
            const pkName = this.parentTab.tab.table.primaryKey.name;
            linkedToParent = linkedToParent.filter(c => c.name === pkName);
        }
        if (this.parentItem) {
            linkedToParent.forEach(column => {
                const linkColumn = column.linkColumn ? column.linkColumn : column.name;
                item[linkColumn] = this.parentItem[linkColumn];
            });
        }
        return item;
    }

    /**
     * Adjust field and field groups for read only and display logic
     */
    protected adjustReadOnlyAndDisplayLogic(): void {
        // Adjust field display logic and readonly logic
        this.fields.forEach(field => {
            if (field.runtimeDisplaylogic) {
                this.adjustFieldDisplayLogic(field, this.currentItem);
            }
            this.adjustFieldReadonlyLogic(field, this.currentItem);
        });
        // Adjust field groups run time display logic
        this.tab.tab.fieldGroups.forEach(group => {
            group.runtimeDisplayed = true;
            group.runtimeDisplaylogic = group.displaylogic;
            if (group.runtimeDisplaylogic) {
                this.adjustGroupFieldDisplayLogic(group, this.currentItem);
            }
        });
    }

    /**
     * Load current item to form
     */
    protected loadCurrentItem(): void {
        // Set used in child fields
        this.tab.tab.fields.forEach(field => {
            if (field.usedInChild) {
                this.usedInChild.push({ tablevel: this.tab.tab.tablevel, name: field.columnName, value: this.currentItem[field.columnName] });
            }
        });
        this.usedInChild.forEach(used => {
            if (used.tablevel < this.tab.tab.tablevel && ToolService.isNullOrUndefined(this.currentItem[used.name])) {
                this.currentItem[used.name] = used.value;
            }
        });
        // Adjust tab run time display logic
        this.adjustTabDisplayLogic(this.currentItem);
        // Set form run time display and readonly logic
        for (let objKey in this.autogenForm.controls) {
            if (this.autogenForm.controls.hasOwnProperty(objKey)) {
                const field = this.fields.find(fld => fld.columnName === objKey);
                if (field) {
                    field.runtimeDisplayed = field.displayed;
                    field.runtimeDisplaylogic = field.displaylogic;
                    field.runtimeReadonly = field.readonly;
                    field.runtimeReadonlylogic = field.relatedColumn.readonlylogic;
                }
            }
        }
        this.adjustReadOnlyAndDisplayLogic();
        // Set form values
        const promiseValues = [];
        for (let objKey in this.autogenForm.controls) {
            if (this.autogenForm.controls.hasOwnProperty(objKey)) {
                const field = this.fields.find(fld => fld.columnName === objKey);
                if (field) {
                    promiseValues.push(
                        this.utilitiesService.setFormValue(this.autogenForm, objKey, field.relatedColumn.reference, field.runtimeReadonly, this.currentItem, field.relatedColumn).then()
                    );
                }
            }
        }

        Promise.all(promiseValues).then(() => {
            this.hookService.execHook(
                AdHookNames.AD_AFTER_LOAD_ITEM, {
                    tab: this.tab.tab,
                    item: this.currentItem,
                    form: this.autogenForm,
                    isNew: this.isNew
                }
            ).then(() => {
                this.globalEventsService.notifyItemChanged(this.tab.idTab, this.currentItem);
                this.autogenForm.markAsPristine();
                this.currentItemCompleted = true;
            });
        });
    }

    /**
     * Get item to save calling all hooks
     */
    protected getProcessedItemToSave(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.serverErrorMsg = null;
            this.hookService.execHook(
                AdHookNames.AD_FORM_BUTTON_SAVE, {
                    tableName: this.tab.tab.table.name,
                }
            ).then(response => {
                if (response.success) {
                    this.autogenForm.updateValueAndValidity();
                    const invalidFields = [];
                    for (let objKey in this.autogenForm.controls) {
                        if (this.autogenForm.controls.hasOwnProperty(objKey)) {
                            this.autogenForm.controls[objKey].markAsTouched();
                            if (this.autogenForm.controls[objKey].invalid) {
                                invalidFields.push(objKey);
                            }
                        }
                    }
                    if (this.autogenForm.valid) {
                        const itemToSave = this.getItemToSave();
                        this.hookService.execHook(
                            AdHookNames.AD_BEFORE_SAVE_ITEM, {
                                tableName: this.tab.tab.table.name,
                                tab: this.tab.tab,
                                item: itemToSave,
                                isNew: this.isNew
                            }
                            // tslint:disable-next-line:no-shadowed-variable
                        ).then(resp => {
                            if (resp.success) {
                                resolve(itemToSave);
                            } else {
                                this.notificationService.showError(this.cacheService.getTranslation('AD_ErrorHookExec'));
                                reject();
                            }
                        }).catch((error) => {
                            this.serverErrorMsg = error && error.error ? error.error : this.cacheService.getTranslation('AD_ErrorInvalidFields');
                            reject();
                        });
                    } else {
                        this.serverErrorMsg = `${this.cacheService.getTranslation('AD_ErrorInvalidFields')}: `;
                        invalidFields.forEach(key => {
                            const fld = this.tab.tab.fields.find(f => f.columnName === key);
                            if (fld) {
                                this.serverErrorMsg += fld.caption + ', ';
                            }
                        });
                        this.serverErrorMsg = this.serverErrorMsg.replace(/,\s*$/, ''); // remove last comma
                        reject();
                    }
                } else {
                    reject();
                }
            }).catch((error) => {
                this.serverErrorMsg = error && error.error ? error.error : this.cacheService.getTranslation('AD_ErrorInvalidFields');
                reject();
            });
        });
    }
}
