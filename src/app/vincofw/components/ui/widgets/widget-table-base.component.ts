import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Router} from '@angular/router';
import {Page, PagedContentModel, SortData} from '../../../models/page.model';
import {AdTableActionDtoModel} from '../../../models/dto/ad-table-action-dto.model';
import {AdColumnDtoModel} from '../../../models/dto/ad-column-dto.model';
import {AdTabMainDtoModel} from '../../../models/dto/ad-tab-main-dto.model';
import {ToolService} from '../../../services/tool.service';
import {AdMenuService} from '../../../services/ad/ad-menu.service';
import {NotificationService} from '../../../../ui-ux/services/notification.service';
import {GlobalEventsService} from '../../../services/global-events.service';
import {UtilitiesService} from '../../../services/utilities.service';
import {ConditionalService} from '../../../services/conditional.service';
import {AppCacheService} from '../../../services/cache.service';
import {AdClientUiuxModel} from "../../../models/dto/ad-client-dto.model";
import {PageEvent} from "@angular/material/paginator";

@Component({
    template: ``
})
export class WidgetTableBaseComponent {

    _page = new Page();
    _content: PagedContentModel<any>;
    actions: AdTableActionDtoModel[];
    visibleColumns: AdColumnDtoModel[];
    tab: AdTabMainDtoModel;
    pageSizeOptions = [10, 20, 40, 75, 100, 150, 200];

    @Input() uiux: AdClientUiuxModel;

    @Input()
    set page(page: Page) {
        this._page = page;
        this.pageChanged.emit(page);
    }

    get page(): Page {
        return this._page;
    }

    @Input()
    set content(content: PagedContentModel<any>) {
        this._content = content;
        // Assign the data to the data source for the table to render
        this.page.pageNumber = this._content.number;
        this.page.totalElements = this._content.totalElements;
        if (this.tab) {
            this.setPageContent();
        }
    }

    get content(): PagedContentModel<any> {
        return this._content;
    }

    @Input()
    set relatedTab(tab: AdTabMainDtoModel) {
        if (ToolService.isNullOrUndefined(this.tab) || this.tab.tab.table.idTable !== tab.tab.table.idTable) {
            this.tab = tab;
            this.actions = this.tab.tab.table.actions;
            this.refreshVisibleColumns();
            this.setPageContent();
            this.relatedTabLoaded();
        }
    }

    @Output() pageChanged = new EventEmitter<Page>();
    @Output() sortChanged = new EventEmitter<SortData[]>();
    @Output() selectedElementsChanged: EventEmitter<any[]> = new EventEmitter();
    @Output() editSelected: EventEmitter<any[]> = new EventEmitter();

    constructor(
        protected router: Router,
        protected menuService: AdMenuService,
        protected notificationService: NotificationService,
        protected globalEventsService: GlobalEventsService,
        protected utilitiesService: UtilitiesService,
        protected conditionalService: ConditionalService,
        protected cacheService: AppCacheService
    ) {
        this.page.pageNumber = 0;
        this.page.size = 20;
        this.visibleColumns = [];
        this._content = new PagedContentModel<any>();
    }

    /**
     * Execute custom actions
     *
     * @param action Table action
     * @param item Row item
     */
    showActionButton(action: AdTableActionDtoModel, item: any): void {
        return item.$action$[action.idTableAction];
    }

    /**
     * Execute custom actions
     *
     * @param action Table action
     * @param item Row item
     */
    doCustomAction(action: AdTableActionDtoModel, item: any): void {
        switch (action.atype) {
            case 'PROCESS':
            case 'PROCESS_QUICK':
                this.globalEventsService.notifyProcessOpen(action.idProcess, action.atype, item);
                break;

            case 'LINK':
                if (action.url === '@currentFile@') {
                    window.open(item.fileUrl, action.target);
                } else {
                    window.open(action.url, action.target);
                }
                break;

            case 'WINDOW':
                const menu = this.menuService.getMenuByWindow(action.idWindow);
                if (menu) {
                    this.router.navigate(['tab', menu.idMenu]);
                } else {
                    this.notificationService.showError(this.cacheService.getTranslation('AD_ErrMenuNotAvailable'));
                }
                break;

            default:
                this.notificationService.showWarning('Action type: ' + action.atype + ' not implemented');
        }
    }

    /**
     * Handles the pagination change logic
     * @param pageEvent Pagination information
     */
    changePageInfo(pageEvent: PageEvent): void {
        this.page.pageNumber = pageEvent.pageIndex;
        this.page.size = pageEvent.pageSize;
        this.pageChanged.emit(this.page);
    }

    /**
     * Set page content information
     */
    protected setPageContent(): void {
        if (this.tab.tab.table.actions && this.tab.tab.table.actions.length > 0) {
            this._content.content.forEach(row => {
                row.$action$ = [];
                this.tab.tab.table.actions.forEach(action => {
                    let displayed = true;
                    if (action.displaylogic) {
                        const runtimeDisplaylogic = this.conditionalService.replaceJSConditional(action.displaylogic, null, null, row);
                        if (this.conditionalService.isCompleteConditional(runtimeDisplaylogic)) {
                            try {
                                // tslint:disable-next-line:no-eval
                                displayed = eval(runtimeDisplaylogic);
                            } catch (e) {
                                console.error('Invalid display logic: ' + action.displaylogic);
                                console.error('Eval expression: ' + runtimeDisplaylogic);
                                displayed = false;
                            }
                        }
                    }
                    row.$action$[action.idTableAction] = displayed;
                });
            });
        }
    }

    /**
     * Updates the visible columns
     */
    protected refreshVisibleColumns(): void {
        this.visibleColumns = this.utilitiesService.getGridColumns(this.tab.tab.table.columns, this.tab.tab.fields, this.tab.tab.table.actions);
    }

    /**
     * This function is called when related tab was loaded
     */
    protected relatedTabLoaded(): void {

    }
}
