import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AdClientUiuxModel} from '../../../models/dto/ad-client-dto.model';
import {AdTabMainDtoModel} from '../../../models/dto/ad-tab-main-dto.model';
import {VincoComponent} from "../../../models/component-register.model";

@Component({
    template: ``
})
export class CustomTabComponent implements VincoComponent, OnInit, OnDestroy {

    name: string;
    tab: AdTabMainDtoModel;
    title: string;
    uiux: AdClientUiuxModel;
    parentItem: any;
    loadOnInit: boolean;
    showInPopup: boolean;

    @Input() data: any;

    constructor() {
        console.log(this.constructor.name + ' constructor ...');
    }

    ngOnInit(): void {
        console.log('CustomTabComponent (' + this.constructor.name + '): ngOnInit(' + (this.tab ? this.tab.name : '') + ')');
        this.name = this.data.name;
        this.tab = this.data.tab;
        this.title = this.data.title;
        this.uiux = this.data.uiux;
        this.parentItem = this.data.parentItem;
        this.loadOnInit = this.data.loadOnInit;
        this.showInPopup = this.data.showInPopup;
    }

    ngOnDestroy(): void {
        console.log('CustomTabComponent (' + this.constructor.name + '): ngOnDestroy(' + (this.tab ? this.tab.name : '') + ')');
    }

}
