import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {VincoHostDirective} from '../../../directives/vinco-host.directive';
import {ComponentCategory} from '../../../models/component-register.model';
import {ComponentsService} from '../../../services/components.service';
import {AuthService} from '../../../services/auth.service';
import {AppCacheService} from '../../../services/cache.service';
import {GlobalEventsService} from '../../../services/global-events.service';

@Component({
    selector: 'vincofw-layout-container',
    templateUrl: './layout-container.component.html',
    styleUrls: ['./layout-container.component.scss']
})
export class LayoutContainerComponent implements OnInit, OnDestroy {

    subscriptions: Subscription[] = [];

    @ViewChild(VincoHostDirective, { static: true }) vincoHost: VincoHostDirective;

    constructor(
        private location: Location,
        private router: Router,
        private cacheService: AppCacheService,
        private authService: AuthService,
        private globalEventsService: GlobalEventsService,
        private componentsService: ComponentsService
    ) {
        console.info('LayoutContainerComponent constructor ...');
        // App started and ready
        this.subscriptions.push(
            this.authService.loginModified$.subscribe(isLogged => {
                this.openWindowOnStart('LoginModified', isLogged);
            })
        );
        this.subscriptions.push(
            this.globalEventsService.appStarted$.subscribe(() => {
                const isLogged = this.authService.isLogged();
                this.openWindowOnStart('AppStarted', isLogged);
            })
        );
    }

    ngOnInit(): void {
        if (this.authService.isLogged()) {
            this.componentsService.loadComponent(this.vincoHost, ComponentCategory.LayoutApp);
        } else {
            this.componentsService.loadComponent(this.vincoHost, ComponentCategory.LayoutLogin);
        }
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    private openWindowOnStart(context: string, isLogged: boolean): void {
        const path = this.location.path(true);
        console.info(`LayoutContainerComponent::${context} -> Path: ${path}, Logged: ${isLogged}`);
        const tabIndex = path.indexOf('/tab/');
        if ((tabIndex === -1 || context === 'LoginModified') && isLogged) {
            this.cacheService.getPreference('WindowOpenOnStart').subscribe(preference => {
                console.info(`LayoutContainerComponent: WindowOpenOnStart = ${preference.value}`);
                if (preference.value) {
                    this.router.navigate(['tab', preference.value]);
                }
            });
        }
    }
}
