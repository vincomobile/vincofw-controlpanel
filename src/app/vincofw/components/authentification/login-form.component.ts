import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {AdClientDtoModel} from '../../models/dto/ad-client-dto.model';
import {AuthService} from '../../services/auth.service';
import {AdTerminalService} from '../../services/ad/ad-terminal.service';
import {AppCacheService} from '../../services/cache.service';
import {AppLoadService} from '../../services/app-load.service';

@Component({
    template: ``
})
export class LoginFormComponent {

    serverMessage = '';
    showLoginError: boolean;
    showRecoverInfo: boolean;
    showRoleEmptyError: boolean;
    showServerError: boolean;
    loginForm: FormGroup;
    recoverForm: FormGroup;
    mode = 'login';
    client: AdClientDtoModel;

    constructor(
        protected router: Router,
        protected authService: AuthService,
        protected cacheService: AppCacheService,
        protected terminalService: AdTerminalService,
        protected appLoadService: AppLoadService
    ) {
        this.client = this.authService.getClientGUI();
        this.loginForm = new FormGroup({
            user: new FormControl('', [
                Validators.required
            ]),
            password: new FormControl('', [
                Validators.required
            ]),
            rememberMe: new FormControl('', [
            ])
        });
        this.recoverForm = new FormGroup({
            email: new FormControl('', [
                Validators.required, Validators.email
            ])
        });
        this.showLoginError = false;
        this.showRecoverInfo = false;
        this.showRoleEmptyError = false;
        this.showServerError = false;
    }

    createLoginFormGroup(): FormGroup {
        return
    }

    get user(): AbstractControl | null {
        return this.loginForm.get('user');
    }

    get password(): AbstractControl | null  {
        return this.loginForm.get('password');
    }

    get rememberMe(): AbstractControl | null  {
        return this.loginForm.get('rememberMe');
    }

    get email(): AbstractControl | null  {
        return this.recoverForm.get('email');
    }

    onInit(): void {
        if (this.authService.isLogged()) {
            this.router.navigate(['/']);
        } else {
            if (!this.client) {
                this.appLoadService.getClientForUrl(resp => {
                    this.client = resp != null ? resp : this.authService.getClientDefault();
                });
            }
        }
    }

    doLogin(): void {
        this.doLoginWithValues(null);
    }

    doLoginWithValues(values: string[]): void {
        this.showLoginError = false;
        this.showRecoverInfo = false;
        this.loginForm.markAllAsTouched();
        if (this.loginForm.valid) {
            this.authService.getJWT(this.client.idClient, this.user.value, this.password.value, this.rememberMe.value, values).subscribe(token => {
                if (token) {
                    this.authService.login(this.user.value)
                        .then(result => {
                            if (result.isLoggedIn) {
                                this.authService.loadPermissions().then(() => {
                                    const ta = this.authService.hasPermission('TerminalAuthentication');
                                    this.terminalService.checkTerminalAuthentication(ta).then(success => {
                                        if (success) {
                                            this.authService.notifyLogin(true);
                                            this.router.navigateByUrl('/', { skipLocationChange: true })
                                                .then(() => this.router.navigate(['/home']));
                                        } else {
                                            this.mode = 'terminal';
                                        }
                                    });
                                });
                            } else {
                                this.showLoginError = true;
                            }
                        })
                        .catch(error => {
                            if (error && error.rolesEmpty) {
                                this.showRoleEmptyError = true;
                            } else {
                                this.showServerError = true;
                            }
                        });
                } else {
                    this.showLoginError = true;
                }
            });
        }
    }

    doRetrievePassword(): void {
        this.showLoginError = false;
        this.recoverForm.markAllAsTouched();
        if (this.recoverForm.valid) {
            this.authService.recoverPassword(this.client.idClient, this.email.value).then(
                (response) => {
                    if (response.success) {
                        this.showRecoverInfo = true;
                        this.showLogin();
                    } else {
                        this.serverMessage = this.cacheService.getTranslation(response.message);
                        this.showLoginError = true;
                    }
                }, (response) => {
                    this.serverMessage = this.cacheService.getTranslation(response.message);
                    this.showLoginError = true;
                }
            );
        }
    }

    showRecover(): void {
        this.showLoginError = false;
        this.mode = 'recover';
    }

    showLogin(): void {
        this.showLoginError = false;
        this.mode = 'login';
    }
}
