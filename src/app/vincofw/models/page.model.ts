export class SortParamModel {
    field: string;
    start: number;
    step: number;
    ids: string[];
}

export class SortData {
    property: string;
    direction: SortOrder;
}

export enum SortOrder { ASC, DESC}

export class Page {
    size = 20;          // The number of elements in the page
    totalElements = 0;  // The total number of elements
    totalPages = 0;     // The total number of pages
    pageNumber = 0;     // The current page number
}

export class PagedContentModel<T> {
    size = 0;
    number = 0;
    totalPages = 0;
    firstPage = true;
    totalElements = 0;
    lastPage = true;
    numberOfElements = 0;
    content: T[] = [];
}

