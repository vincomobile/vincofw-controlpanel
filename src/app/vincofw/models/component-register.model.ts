import {Type} from '@angular/core';

export enum ComponentCategory {
    Empty = 'Empty',
    NotFound = 'NotFound',
    LayoutApp = 'LayoutApp',
    LayoutLogin = 'LayoutLogin',
    LoginForm = 'LoginForm',
    LoginInfo = 'LoginInfo',
    Header = 'Header',
    HeaderContent = 'HeaderContent',
    SideMenu = 'SideMenu',
    BarMenu = 'BarMenu',
    Footer = 'Footer',
    ContentPanel = 'ContentPanel'
}

export interface VincoComponent {
    data: any;
}

export class ComponentModel {
    constructor(public component: Type<any>, public data: any) {}
}

export class ComponentRegisterModel {
    register: Map<ComponentCategory, ComponentModel>;
    registerCustom: Map<string, ComponentModel>;

    constructor() {
        this.register = new Map<ComponentCategory, ComponentModel>();
        this.registerCustom = new Map<string, ComponentModel>();
    }

    setComponent(category: ComponentCategory, component: ComponentModel): void {
        this.register.set(category, component);
    }

    getComponent(category: ComponentCategory): ComponentModel {
       return this.register.get(category);
    }

    setCustomComponent(name: string, component: ComponentModel): void {
        this.registerCustom.set(name, component);
    }

    getCustomComponent(name: string): ComponentModel {
        return this.registerCustom.get(name);
    }
}

