export class BaseSettingsModel {
    REST_HOST_PATH: string;
    REST_HOST_CORE: string;
    REST_HOST_LOGIN: string;
    ID_CLIENT: string;
    ID_LANGUAGE: string;
    LANGUAGE: string;
    APP_NAME: string;
    CLIENT_FULL_NAME: string;
    COUNTRY_CODE: string;
    APPLICATION_LOGO_URL: any;
    LOGO_CLIENT_PATH: string;
    APPLICATION_ICON_URL: string;
    SECRET_KEY: string;
    TABLE_PREFIX_VERSIONS: string[];
    SHOW_MENU_FILTER: boolean;
}
