export class HookModel {
    id: string;
    name: string;

    constructor(id: string, name: string) {
        this.id = id;
        this.name = name;
    }

    handler(args): Promise<any> {
        return null;
    }

    identifier(): string {
        return this.name + ' (' + this.id + ')';
    }
}

export class HookExecResult {
    success: boolean;
    results: any[];
}

export class AdHookNames {

    // When: In form editor after item values are loaded
    //
    // Arguments:
    //      tab: Tab information
    //      item: Current item values
    //      form: Form
    //      isNew: Is new item
    static readonly AD_AFTER_LOAD_ITEM = 'AdAfterLoadItem';

    // When: In form editor after tab is loaded
    //
    // Arguments:
    //      tab: Tab information
    static readonly AD_AFTER_LOAD_TAB = 'AdAfterLoadTab';

    // When: After a new item is created (Button '+'), before is shown in form editor
    //
    // Arguments:
    //      tableName: Table name
    //      tab: Tab information
    //      item: New empty item
    static readonly AD_AFTER_NEW_ITEM = 'AdAfterNewItem';

    // When: Before the item will be send to server
    //
    // Arguments:
    //      tableName: Table name
    //      tab: Tab information
    //      item: Item to be saved
    //      isNew: Is new item
    static readonly AD_BEFORE_SAVE_ITEM = 'AdBeforeSaveItem';

    // When: Button '+' is pressed
    //
    // Arguments:
    //      tab: Tab information
    //      origin: LIST or FORM
    // Return:
    //      If not successful the event is cancelled
    static readonly AD_CAN_NEW_ITEM = 'AdCanNewItem';

    // When: In form editor push save button
    //
    // Arguments:
    //      tableName: Table name
    static readonly AD_FORM_BUTTON_SAVE = 'AdFormButtonSave';

    // When: Need global parameters to conditional replacements (displaylogic, readonlylogig, defaultvalues)
    //
    // Arguments:
    //      parameters: Global parameters objects
    static readonly AD_GET_GLOBAL_PARAMETERS = 'AdGetGlobalParameters';

    // When: Editor list values must be change
    //
    // Arguments:
    //      fieldName: Field name
    //      values: New values
    static readonly AD_LIST_CHANGE_VALUES = 'AdListChangeValues';

    // When: Before list table values
    //
    // Arguments:
    //      tableName: Table name
    //      constraints: Constraints to be applied
    // Return:
    //      Value to be add to constraints or null
    static readonly AD_QUERY_TABLE = 'AdQueryTable';

    // When: Load multiselect values and table do not have linked parent column
    //
    // Arguments:
    //      tableName: Table name
    //      item: Item to be edited
    // Return:
    //      Multiselect parameters: { idParentField, value }
    static readonly AD_GET_MULTISELECT_PARAMS = 'AdGetMultiselectParams';

    static readonly AD_BEFORE_LOAD_PREFERENCE = 'AdBeforeLoadPreference';
}

