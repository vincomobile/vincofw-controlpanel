import {AdReferenceDtoModel} from './ad-reference-dto.model';

export class AdProcessParamDtoModel {
    idProcessParam: string;
    name: string;
    description: string;
    valuemin: string;
    valuemax: string;
    valuedefault: string;
    valuedefault2: string;
    mandatory: boolean;
    ranged: boolean;
    displayed: boolean;
    saveType: string;
    ptype: string;
    listType: string;
    caption: string;
    displaylogic: string;
    seqno: number;
    span: number;
    reference: AdReferenceDtoModel;

    // Client side
    cssColspan: string;
    value: string;
    runtimeDisplaylogic: string;
    runtimeDisplayed: boolean;
}
