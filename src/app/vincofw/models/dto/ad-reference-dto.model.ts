import {AdRefTableDtoModel} from './ad-ref-table-dto.model';
import {AdRefListDtoModel} from './ad-ref-list-dto.model';
import {AdRefButtonDtoModel} from './ad-ref-button-dto.model';

export class AdReferenceDtoModel {
    idReference: string;
    rtype: string;
    base: boolean;
    grouped: boolean;
    multiple: boolean;
    refTable?: AdRefTableDtoModel;
    refList?: AdRefListDtoModel[];
    refButton?: AdRefButtonDtoModel;

    // Client field
    autoLoadValues?: boolean;

    constructor(item: any) {
        this.idReference = item.idReference;
        this.rtype = item.rtype;
        this.grouped = item.grouped;
        this.refTable = item.refTable;
        this.refList = item.refList;
        this.refButton = item.refButton;
    }

    isComplex(): boolean {
        return this.rtype === 'BUTTON' || this.rtype === 'LIST' || this.rtype === 'TABLE' || this.rtype === 'TABLEDIR' || this.rtype === 'SEARCH';
    }
}
