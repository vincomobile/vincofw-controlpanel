import {AdTabDtoModel} from './ad-tab-dto.model';

export class AdTabMainDtoModel {
    // Server side fields
    idTab: string;
    idTable: string;
    idWindow: string;
    name: string;
    displaylogic: string;
    ttype: string;
    uipattern: string;
    usedWhenNew: boolean;
    tab: AdTabDtoModel;
    childTabs: AdTabMainDtoModel[];
    // Client side fields
    visibleTabs: AdTabMainDtoModel[];
    displayed: boolean;
}


