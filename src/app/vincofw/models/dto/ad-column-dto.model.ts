import {AdReferenceDtoModel} from './ad-reference-dto.model';
import {AdFieldDtoModel} from './ad-field-dto.model';
import {SortData} from '../page.model';

export class AdColumnDtoModel {
    idTable: string;
    idColumn: string;
    name: string;
    description: string;
    ctype: string;
    cformat: string;
    seqno: number;
    primaryKey: boolean;
    mandatory: boolean;
    readonlylogic: string;
    groupedsearch: boolean;
    linkParent: boolean;
    linkColumn: string;
    translatable: boolean;
    lengthMin: number;
    lengthMax: number;
    reference: AdReferenceDtoModel;
    // Client side fields
    relatedField: AdFieldDtoModel;
    sortable: boolean;
    sort: SortData;
}
