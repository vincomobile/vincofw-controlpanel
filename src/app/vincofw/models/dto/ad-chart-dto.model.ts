import {AdReferenceDtoModel} from './ad-reference-dto.model';
import {AdChartResultModel} from '../ad/ad-chart-result.model';
import {AdRowColumnModel} from '../ad/ad-grid.model';

export class AdChartColumnDtoModel {
    name: string;
    caption: string;
    displayed: string;
    reference: AdReferenceDtoModel;
}

export class AdChartDtoModel extends AdRowColumnModel {
    idChart: string;
    ctype: string;
    displaylogic: string;
    mode: string;
    name: string;
    keyField: string;
    seqno: number;

    animations: boolean;
    colorScheme: string;
    lengendPosition: string;
    showLegend: boolean;
    disableTooltip: boolean;
    gradient: boolean;

    showXAxis: boolean;
    showYAxis: boolean;
    showTitleX: boolean;
    showTitleY: boolean;
    showGridLines: boolean;
    showDataLabel: boolean;
    roundDomains: boolean;
    roundBarEdges: boolean;
    hideBarZero: boolean;

    pieDoughnut: boolean;
    pieExplodeSlices: boolean;
    pieArcWidth: number;
    pieShowLabels: boolean;

    showReferenceLines: boolean;
    showReferenceLabels: boolean;
    autoscale: boolean;
    timeLine: boolean;
    lineInterpolation: string;

    columns: AdChartColumnDtoModel[];
    linked: string[];

    // Client side fields
    columnReferenceType: string;
    chartType: string;
    gridColumnsStyle: string;
    runtimeResult: AdChartResultModel;
    runtimeColumns: AdChartColumnDtoModel[];
    runtimeListValues: any[];
    runtimeError: string;
    runtimeFilter: string;
}
