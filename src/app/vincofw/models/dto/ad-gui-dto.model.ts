export class AdGuiDtoModel {
    button: string;
    formField: string;
    filterApply: string;
    filterMode: string;
    filterColumns: string;
    tableMode: string;
    tableType: string;
}
