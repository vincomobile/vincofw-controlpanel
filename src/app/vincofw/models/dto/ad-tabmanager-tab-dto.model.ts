import {AdWindowDtoModel} from './ad-window-dto.model';
import {AdProcessDtoModel} from './ad-process-dto.model';

export class AdTabmanagerTabDtoModel {
    idTab: string;
    mode: string;
    caption: string;
    window: AdWindowDtoModel;
    process: AdProcessDtoModel;
    manualCommand: string;
    manualModule: string;
    manualWindow: string;
}
