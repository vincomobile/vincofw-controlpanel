import {AdClientUiuxModel} from './ad-client-dto.model';

export class AdProcessLogDtoModel {
    idProcess: string;
    idProcessExec: string;
    eventType: string;
    logType: string;
    date: Date;
    info: string;
    outParamName: string;
    outParamValue: any;

    cssClass: string;

    constructor(msg: string, uiux: AdClientUiuxModel) {
        const body = JSON.parse(msg);
        this.idProcess = body.idProcess;
        this.idProcessExec = body.idProcessExec;
        this.eventType = body.eventType;
        this.logType = body.logType;
        this.date = new Date(body.date);
        this.info = body.info;
        this.cssClass = this.logType === 'SUCCESS' ? uiux.getCssSuccess() : (this.logType === 'ERROR' ? uiux.getCssError() : (this.logType === 'WARN' ? uiux.getCssWarning() : ''));
    }
}
