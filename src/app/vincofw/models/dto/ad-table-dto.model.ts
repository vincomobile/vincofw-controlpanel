import {AdColumnDtoModel} from './ad-column-dto.model';
import {AdTableActionDtoModel} from './ad-table-action-dto.model';

export class AdTableDtoModel {
    idTable: string;
    idSortColumn: string;
    restPath: string;
    name: string;
    standardName: string;
    dataOrigin: string;
    isview: boolean;
    audit: boolean;
    seqno: number;
    sortable: boolean;
    privilege: string;
    version: number;
    columns: AdColumnDtoModel[];
    actions: AdTableActionDtoModel[];
    // Client fields
    primaryKey: AdColumnDtoModel;
}
