import {AdReferenceDtoModel} from './ad-reference-dto.model';

export class AdChartFilterDtoModel {
    idChartFilter: string;
    caption: string;
    name: string;
    description: string;
    displaylogic: string;
    displayed: boolean;
    mandatory: boolean;
    ranged: boolean;
    saveType: string;
    seqno: number;
    valuedefault: string;
    valuedefault2: string;
    valuemax: string;
    valuemin: string;
    reference: AdReferenceDtoModel;
}
