import {AdColumnDtoModel} from './ad-column-dto.model';
import {AdReferenceKeyValueModel} from './ad-reference-key-value.model';
import {AdRowColumnModel} from '../ad/ad-grid.model';
import {AdReferenceDtoModel} from "./ad-reference-dto.model";

export class AdFieldDtoModel extends AdRowColumnModel {
    idField: string;
    idColumn: string;
    idFieldGroup?: string;
    caption: string;
    placeholder?: string;
    hint?: string;
    seqno: number;
    gridSeqno?: number;
    readonly: boolean;
    showinsearch: boolean;
    showingrid: boolean;
    showinstatusbar: boolean;
    usedInChild: boolean;
    onchangefunction?: string;
    displaylogic?: string;
    filterRange: boolean;
    filterType: string;
    filterSeqno: number;
    filterSpan: number;
    filterDefaultValue?: string;
    filterDefaultValue2?: string;
    sortable: boolean;
    cssClass?: string;
    multiSelect?: boolean;
    valuemin?: string;
    valuemax?: string;
    valuedefault?: string;
    guiFlexGrow?: string;
    guiMinWidth?: string;
    guiMaxWidth?: string;
    guiWidth?: string;
    guiCustomWidth?: string;
    columnName: string;

    // Client side fields
    relatedColumn?: AdColumnDtoModel;
    referenceValues?: AdReferenceKeyValueModel[];
    displayListeners: string[];
    designerSelected?: boolean;
    designerModify?: boolean;
    runtimeReadonly?: boolean;
    runtimeReadonlylogic?: string;
    runtimeDisplayed?: boolean;
    runtimeDisplaylogic?: string;
    cssContainerClass?: string;
    cssItemClass?: string;
    loadComplete?: boolean;
    mandatory?: boolean;

    copyToFilter(fld: AdFieldDtoModel): void {
        this.idField = fld.idField
        this.idColumn = fld.idColumn;
        this.caption = fld.caption;
        this.span = fld.filterSpan;
        this.filterSeqno = fld.filterSeqno;
        this.displayed = true;
        this.displaylogic = fld.displaylogic;
        this.filterType = fld.filterType;
        this.filterRange = fld.filterRange;
        this.filterDefaultValue = fld.filterDefaultValue;
        this.filterDefaultValue2 = fld.filterDefaultValue2;
        this.valuemin = fld.valuemin;
        this.valuemax = fld.valuemax;
        this.columnName = fld.columnName;
        this.columnReferenceType = fld.columnReferenceType;
        this.addToFilterColumn();
        this.relatedColumn.reference = new AdReferenceDtoModel({
            idReference: fld.relatedColumn.reference.idReference,
            rtype: fld.relatedColumn.reference.rtype,
            grouped: fld.relatedColumn.reference.grouped,
            refTable: fld.relatedColumn.reference.refTable,
            refList: fld.relatedColumn.reference.refList,
            refButton: fld.relatedColumn.reference.refButton
        });
    }

    copyToFilterValues(item: any) : void {
        this.idField = item.idField
        this.idColumn = item.idColumn;
        this.caption = item.caption;
        this.span = item.filterSpan;
        this.filterSeqno = item.filterSeqno;
        this.displayed = true;
        this.displaylogic = item.displaylogic;
        this.filterRange = item.filterRange;
        this.filterDefaultValue = item.filterDefaultValue;
        this.filterDefaultValue2 = item.filterDefaultValue2;
        this.valuemin = item.valuemin;
        this.valuemax = item.valuemax;
        this.columnName = item.columnName;
        this.columnReferenceType = item.columnReferenceType;
        this.addToFilterColumn();
    }

    addToFilterColumn() {
        const item = new AdColumnDtoModel();
        item.idColumn = this.idColumn;
        item.name = this.columnName;
        item.reference = new AdReferenceDtoModel({
            idReference: this.idColumn,
            rtype: this.columnReferenceType,
            base: true,
            grouped: false,
            multiple: false
        });
        item.relatedField = this;
        this.relatedColumn = item;
    }
}
