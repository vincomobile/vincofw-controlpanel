import {AdTabMainDtoModel} from './ad-tab-main-dto.model';

export class AdWindowDtoModel {
    idWindow: string;
    name: string;
    wtype: string;
    mainTab: AdTabMainDtoModel;
}
