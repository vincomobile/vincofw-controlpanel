import {AdGuiDtoModel} from './ad-gui-dto.model';
import {AdProcessParamDtoModel} from './ad-process-param-dto.model';

export class AdProcessDtoModel {
    idProcess: string;
    name: string;
    description: string;
    module: string;
    ptype: string;
    uipattern: string;
    pdf: boolean;
    excel: boolean;
    html: boolean;
    word: boolean;
    format: string;
    privilege: string;
    privilegeDesc: string;
    showConfirm: boolean;
    confirmMsg: string;
    gui: AdGuiDtoModel;
    params: AdProcessParamDtoModel[];
}
