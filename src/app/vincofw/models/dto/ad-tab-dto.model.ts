import {AdGuiDtoModel} from './ad-gui-dto.model';
import {AdTableDtoModel} from './ad-table-dto.model';
import {AdFieldDtoModel} from './ad-field-dto.model';
import {AdChartDtoModel} from './ad-chart-dto.model';
import {AdChartFilterDtoModel} from './ad-chart-filter-dto.model';
import {AdFieldGroupDtoModel} from './ad-field-group-dto.model';

export class AdTabDtoModel {
    idTab: string;
    tablevel: number;
    sortColumns: number;
    seqno: number;
    gridColumns: string;
    uipattern: string;
    ttype: string;
    sqlwhere: string;
    sqlorderby: string;
    displaylogic: string;
    command: string;
    idParentTable: string;
    viewMode: string;
    viewDefault: boolean;
    groupMode: string;
    hasFilters: boolean;
    columnName: string;
    defaultEditor: boolean;
    reloadAfterSave: boolean;

    gui: AdGuiDtoModel;
    table: AdTableDtoModel;
    fields: AdFieldDtoModel[];
    charts: AdChartDtoModel[];
    chartFilters: AdChartFilterDtoModel[];
    fieldGroups: AdFieldGroupDtoModel[];

    // Client side fields
    ungroupedFields: AdFieldDtoModel[];
    runtimeSqlwhere: string;
}
