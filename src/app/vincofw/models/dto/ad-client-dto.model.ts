export class AdClientDtoModel {
    idClient: string;
    name: string;
    nick: string;
    urlPattern: string;
    logo: string;
    logoFaviIcon: string;
    guiButton: string;
    guiFormField: string;
    guiColorTheme: string;
    guiWidth: string;
    guiUx: string;
    toolbarPosition: string;
    navbarPosition: string;
    navbarPrimaryColor: string;
    navbarPrimaryColorVariant: string;
    navbarSecondaryColor: string;
    navbarSecondaryColorVariant: string;
    primaryColor: string;
    primaryColorVariant: string;
    secondaryColor: string;
    secondaryColorVariant: string;
    msgInfoColor: string;
    msgInfoColorVariant: string;
    msgSuccessColor: string;
    msgSuccessColorVariant: string;
    msgWarningColor: string;
    msgWarningColorVariant: string;
    msgErrorColor: string;
    msgErrorColorVariant: string;
    footerHidden: boolean;
    footerPosition: string;
}

export class AdClientUiuxModel {

    guiUx: string;
    formField: string;
    buttonType: string;
    primaryColor: string;
    primaryColorVariant: string;
    secondaryColor: string;
    secondaryColorVariant: string;
    msgInfoColor: string;
    msgInfoColorVariant: string;
    msgSuccessColor: string;
    msgSuccessColorVariant: string;
    msgWarningColor: string;
    msgWarningColorVariant: string;
    msgErrorColor: string;
    msgErrorColorVariant: string;

    constructor(client: AdClientDtoModel) {
        this.guiUx = client.guiUx;
        this.formField = client.guiFormField;
        this.buttonType = client.guiButton;
        this.primaryColor = client.primaryColor;
        this.primaryColorVariant = client.primaryColorVariant;
        this.secondaryColor = client.secondaryColor;
        this.secondaryColorVariant = client.secondaryColorVariant;
        this.msgInfoColor = client.msgInfoColor;
        this.msgInfoColorVariant = client.msgInfoColorVariant;
        this.msgSuccessColor = client.msgSuccessColor;
        this.msgSuccessColorVariant = client.msgSuccessColorVariant;
        this.msgWarningColor = client.msgWarningColor;
        this.msgWarningColorVariant = client.msgWarningColorVariant;
        this.msgErrorColor = client.msgErrorColor;
        this.msgErrorColorVariant = client.msgErrorColorVariant;
    }

    getCssPrimary(): string {
        return this.getCss(this.primaryColor, this.primaryColorVariant);
    }

    getCssPrimaryBg(): string {
        return this.getCssBg(this.primaryColor, this.primaryColorVariant);
    }

    getCssSecundary(): string {
        return this.getCss(this.secondaryColor, this.secondaryColorVariant);
    }

    getCssSecundaryBg(): string {
        return this.getCssBg(this.secondaryColor, this.secondaryColorVariant);
    }

    getCssInfo(): string  {
        return this.getCss(this.msgInfoColor, this.msgInfoColorVariant);
    }

    getCssInfoBg(): string  {
        return this.getCssBg(this.msgInfoColor, this.msgInfoColorVariant);
    }

    getCssSuccess(): string  {
        return this.getCss(this.msgSuccessColor, this.msgSuccessColorVariant);
    }

    getCssSuccessBg(): string  {
        return this.getCssBg(this.msgSuccessColor, this.msgSuccessColorVariant);
    }

    getCssWarning(): string  {
        return this.getCss(this.msgWarningColor, this.msgWarningColorVariant);
    }

    getCssWarningBg(): string  {
        return this.getCssBg(this.msgWarningColor, this.msgWarningColorVariant);
    }

    getCssError(): string  {
        return this.getCss(this.msgErrorColor, this.msgErrorColorVariant);
    }

    getCssErrorBg(): string  {
        return this.getCssBg(this.msgErrorColor, this.msgErrorColorVariant);
    }

    private getCss(color: string, variant: string) {
        return `${color}-${variant}`;
    }

    private getCssBg(color: string, variant: string) {
        return `${color}-${variant}-bg`;
    }
}
