export class AuthUserModel {
    idUserLogged: string;
    idClient: string;
    name: string;
    email: string;
    username: string;
    crytpo: string;
    rememberMe: boolean;
    photoUrl?: string;
    defaultRole?: string;
    defaultLanguage?: string;
    privileges: string[];
    roles: AuthRoleModel[];
    loginValues?: string[];
}

export class AuthRoleModel {
    idClient: string;
    idRole: string;
    roleType: string;
    name: string;
    description: string;
}

export class AuthLoginResponse {
    isLoggedIn: boolean;
    serverResponse: any;
}
