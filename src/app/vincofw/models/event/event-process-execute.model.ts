export class EventProcessExecuteModel {
    idProcess: string;
    success: boolean;
}

export class EventProcessOpenModel {
    idProcess: string;
    buttonType: string;
    item?: any;
}
