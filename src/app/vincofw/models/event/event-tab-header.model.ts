export enum EventTabHeaderType {
    FirstRecord,
    PreviousRecord,
    NextRecord,
    LastRecord,
    SaveRecord,
    CancelRecord,
    NewRecord,
    ListRecords,
    ReloadRecord,
    ChangeCaption
}

export enum EventTabHeaderMode {
    Notify,
    Emit
}

export class EventTabHeaderModel {
    event: EventTabHeaderType;
    mode: EventTabHeaderMode;
    idTab: string;
    caption: string;

    constructor(event: EventTabHeaderType, mode = EventTabHeaderMode.Notify, idTab: string, caption: string = null) {
        this.event = event;
        this.mode = mode;
        this.idTab = idTab;
        this.caption = caption;
    }
}
