export enum EventAppLoadType {
    Error = 'APP_LOAD_ERROR',
    Completed = 'APP_LOAD_COMPLETED',
    Messages = 'APP_LOAD_MESSAGES',
    MainApp = 'APP_LOAD_MAINAPP',
    UiUx = 'APP_LOAD_UIUX',
    Custom = 'APP_LOAD_CUSTOM',
    ChangeTheme = 'APP_LOAD_CHANGE_THEME',
    ChangeClient = 'APP_LOAD_CHANGE_CLIENT'
}

export class EventAppLoad {
    eventType: EventAppLoadType;
    value: any;
}
