import {AdBaseModel} from './ad-base.model';
import {AdReferenceKeyValueModel} from '../dto/ad-reference-key-value.model';
import {AdColumnDtoModel} from '../dto/ad-column-dto.model';

export class AdFieldModel extends AdBaseModel {
    idField: string;
    idColumn: string;
    idFieldGroup?: string;
    caption: string;
    placeholder?: string;
    hint?: string;
    seqno: number;
    span: number;
    gridSeqno?: number;
    readonly: boolean;
    displayed: boolean;
    startnewrow: boolean;
    showinsearch: boolean;
    showingrid: boolean;
    showinstatusbar: boolean;
    usedInChild: boolean;
    onchangefunction?: string;
    displaylogic?: string;
    filterRange: boolean;
    filterType: string;
    filterSeqno: number;
    filterSpan: number;
    filterDefaultValue?: string;
    filterDefaultValue2?: string;
    sortable: boolean;
    cssClass?: string;
    multiSelect?: boolean;
    valuemin?: string;
    valuemax?: string;
    valuedefault?: string;
    guiFlexGrow?: string;
    guiMinWidth?: string;
    guiMaxWidth?: string;
    guiWidth?: string;
    guiCustomWidth?: string;
    columnName: string;

    // Client side fields
    designerModify: boolean;
    designerSelected: boolean;
    columnReferenceType: string;
    relatedColumn?: AdColumnDtoModel;
    referenceValues?: AdReferenceKeyValueModel[];
}
