import {AdBaseModel} from './ad-base.model';

export class AdLanguageModel extends AdBaseModel {
    idLanguage: string;
    name: string;
    iso2: string;
    iso3: string;
    countrycode: string;
}

