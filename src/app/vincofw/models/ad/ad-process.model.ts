import {AdBaseModel} from './ad-base.model';

export class AdProcessModel extends AdBaseModel{
    idProcess: string;
    name: string;
    description: string;
    ptype: string;
    classname: string;
    classmethod: string;
    background: boolean;
    uipattern: string;
    jrxml: string;
    jrxmlExcel: string;
    jrxmlWord: string;
    pdf: boolean;
    excel: boolean;
    html: boolean;
    word: boolean;
    multiple: boolean;
    evalSql: string;
    privilege: string;
    privilegeDesc: string;
    showConfirm: boolean;
    confirmMsg: string;
    cronExpression: string;
    executingIdUser: string;
    executingIdClient: string;
}

export class AdProcessResponse {
    body: Blob;
    fileName: string;
}
