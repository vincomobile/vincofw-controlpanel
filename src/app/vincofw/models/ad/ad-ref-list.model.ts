import {AdBaseModel} from './ad-base.model';

export class AdRefListModel extends AdBaseModel  {
    idRefList: string;
    idReference: string;
    value: string;
    name: string;
    seqno: number;
    extraInfo: string;
}
