export class AdProcessLogModel {
    idProcess: string;
    idProcessExec: string;
    ltype: string;
    created: Date;
    log: string;

    cssClass: string;
}
