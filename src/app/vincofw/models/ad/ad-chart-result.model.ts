export class AdChartValueModel {
    id: string;
    x: string;
    fullX: string;
    value: string;
}

export class AdChartSerieModel {
    color: string;
    legend: string;
    values: AdChartValueModel[];
}

export class AdChartValueColumnModel {
   id: string;
   values: any[];
}

export class AdChartResultModel {
    idChart: string;
    success: boolean;
    error: string;
    ctype: string;
    keyField: string;
    nameField: string;
    dateMode: string;
    year: number;
    titleX: string;
    titleY: string;
    showTitleX: boolean;
    showTitleY: boolean;
    series: AdChartSerieModel[];
    values: AdChartValueColumnModel[];
}

