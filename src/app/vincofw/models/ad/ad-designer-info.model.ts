export class AdDesignerFieldModel {
    idField: string;
    caption: string;
    idFieldGroup: string;
    span: number;
    seqno: number;
    gridSeqno: number;
    guiCustomWidth: string;
    displayed: boolean;
    showingrid: boolean;
    startnewrow: boolean;
    sortable:  boolean;
    displaylogic: string;
    placeholder: string;
    hint: string;
    valuedefault: string;
    valuemin: string;
    valuemax: string;
    active: boolean;
    readonly: boolean;
    showinstatusbar: boolean;
    usedInChild: boolean;
    showinsearch: boolean;
    filterRange: boolean;
    filterType: string;
    filterSeqno: number;
    filterSpan: number;
    filterDefaultValue: string;
    filterDefaultValue2: string;
}

export class AdDesignerInfoModel {
    gridColumns: string;
    fields: AdDesignerFieldModel[];
}
