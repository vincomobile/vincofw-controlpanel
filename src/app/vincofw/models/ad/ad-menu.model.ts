import {AdBaseModel} from './ad-base.model';
import {AdProcessModel} from './ad-process.model';
import {AdWindowModel} from './ad-window.model';

export class AdMenuModel extends AdBaseModel {
    idMenu: string;
    idModule: string;
    idModuleMenu: string;
    value: string;
    name: string;
    action: string;
    parentMenu: string;
    icon: string;
    idWindow: string;
    idProcess: string;
    idTable: string;
    manualCommand: string;
    seqno: number;
    window: AdWindowModel;
    process: AdProcessModel;
    subMenus: AdMenuModel[];
}
