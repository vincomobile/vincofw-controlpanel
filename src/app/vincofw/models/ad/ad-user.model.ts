import {AdBaseModel} from './ad-base.model';

export class AdUserModel extends AdBaseModel {
    idUser: string;
    name: string;
    username: string;
    password: string;
    phone: string;
    phoneMobile: string;
    email: string;
    fax: string;
    defaultIdLanguage: string;
    photoUrl: string;
    photo: string;
    defaultIdRole: string;
    currentIdLanguage: string;
    currentIdRole: string;
}
