import {AdBaseModel} from './ad-base.model';

export class AdPrivilegeModel extends AdBaseModel  {
    idPrivilege: string;
    name: string;
    description: string;
}
