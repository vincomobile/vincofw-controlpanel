export class AdBaseModel {
    id: string;
    idClient: string;
    active: boolean;
    created: Date;
    updated: Date;
}

