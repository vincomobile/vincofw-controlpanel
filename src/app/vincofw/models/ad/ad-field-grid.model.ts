import {AdBaseModel} from './ad-base.model';

export class AdFieldGridModel extends AdBaseModel {
    idFieldGrid: string;
    idModule: string;
    idUser: string;
    idTab: string;
    idField: string;
    seqno: number;
    guiWidth: number;
    guiFlexGrow: number;
}
