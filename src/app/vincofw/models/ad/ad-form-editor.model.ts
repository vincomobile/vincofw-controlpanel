export class AdFormEditorModel {
    name: string;
    pristine: boolean;
    lastValue: any;
    readonlyListeners: string[];
    displayListeners: string[];
    groupFieldListeners: string[];
    tabListeners: string[];
    sqlListeners: string[];

    constructor(name: string) {
        this.name = name;
        this.pristine = true;
        this.lastValue = null;
        this.readonlyListeners = [];
        this.displayListeners = [];
        this.groupFieldListeners = [];
        this.tabListeners = [];
        this.sqlListeners = [];
    }

    public isModify(values: any): boolean {
        const value = values[this.name];
        const result = (value === null && this.lastValue !== null) || (value !== null && this.lastValue === null) || value !== this.lastValue;
        if (result) {
            this.lastValue = value;
        }
        return result;
    }
}

