import {AdBaseModel} from './ad-base.model';

export class AdWindowModel extends AdBaseModel{
    idWindow: string;
    name: string;
    wtype: string;
}
