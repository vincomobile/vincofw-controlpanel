import {AdFieldDtoModel} from '../dto/ad-field-dto.model';
import {AdChartDtoModel} from '../dto/ad-chart-dto.model';

export class AdRowColumnModel {
    displayed: boolean;
    startnewrow: boolean;
    span: number;
    cssGridArea?: string;
    columnReferenceType?: string;
}

export class AdRowFieldModel {
    cssGrid: string;
    cssGridTemplateColumns: string;
    colWidth: number;
    columns: AdFieldDtoModel[] | AdChartDtoModel[];
}

export class AdRowGridTemplateColumnsModel {
    columns: number;
    gridColumns: string;
}
