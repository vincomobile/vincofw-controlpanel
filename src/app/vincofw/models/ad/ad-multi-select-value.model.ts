export class AdMultiSelectValueModel {
    id: string;
    name: string;
    selected: boolean;
}

export class AdSelectValueModel {
    value: string;
    caption: string;
}
