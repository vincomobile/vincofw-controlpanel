import {AdBaseModel} from './ad-base.model';

export class AdReferenceModel extends AdBaseModel  {
    idReference: string;
    value: string;
    name: string;
    rtype: string;
    base: boolean;
}
