import {AdBaseModel} from './ad-base.model';
import {AdLanguageModel} from './ad-language.model';

export class AdTranslationModel extends AdBaseModel {
    idTranslation: string;
    translation: string;
    language: AdLanguageModel;
}
