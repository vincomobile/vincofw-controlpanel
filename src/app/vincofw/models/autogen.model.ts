export class AutogenEditElementsModel {
    items: any[];
    index: number;
    isNew: boolean;
}

export class AutogenUsedinchildModel {
    tablevel: number;
    name: string;
    value: any;
}
