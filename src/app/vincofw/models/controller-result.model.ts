export class ControllerResultModel {
    success: boolean;
    message: string;
    errCode: number;
    properties: any;
}
