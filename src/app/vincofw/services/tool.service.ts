import {Injectable} from '@angular/core';
import * as FileSaver from 'file-saver';
import * as CryptoJS from 'crypto-js';
import * as moment_ from 'moment';

const moment = moment_;

@Injectable({
    providedIn: 'root'
})
export class ToolService {

    constructor() {

    }

    public static S4(): string {
        return (Math.floor((1 + Math.random()) * 0x10000)).toString(16).substring(1).toLowerCase();
    }

    public static isNullOrUndefined(value: any): boolean {
        return value === null || value === undefined;
    }

    /**
     * Return boolean value for a string
     *
     * @param value String value
     */
    public static getBooleanValue(value: string): boolean {
        if (ToolService.isNullOrUndefined(value)) {
            return false;
        }
        const val = value.toUpperCase();
        return  val === 'Y' || val === 'S' || val === '1' || val === 'TRUE' || val === 'YES' || val === 'SI' || val === 'SÍ';
    }

    /**
     * Get moment date from value
     *
     * @param value
     */
    public static getMomentDate(value): any {
        if (ToolService.isNullOrUndefined(value)) {
            return null;
        }
        if (typeof value === 'number') {
            return moment(value);
        }
        if (typeof value === 'string') {
            value = value.trim();
            if (value === '') {
                return null;
            }
            if (value.indexOf('T') === 10) {
                return moment(value.substring(0, 19), 'YYYY-MM-DDTHH:mm:ss');
            }
            if (value.indexOf('/') > 0) {
                return moment(value, value.length > 10 ? 'DD/MM/YYYY HH:mm:ss' : 'DD/MM/YYYY');
            }
            if (value.length <= 5 && value.indexOf(':') > 0) {
                return moment(value, 'HH:mm');
            }
            if (value.length <= 8 && value.indexOf(':') > 0) {
                return moment(value, 'HH:mm:ss');
            }
            return moment(value);
        }
        if (value instanceof Date) {
            return moment(value);
        }
        return value;
    }

    /**
     * Check if value is empty
     *
     * @param value Value
     */
    public static isEmptyValue(value: any): boolean {
        if (!ToolService.isNullOrUndefined(value)) {
            if (typeof value === 'string') {
                return value.trim() === '';
            }
            if (value instanceof moment) {
                return !moment(value).isValid();
            }
            return false;
        }
        return true;
    }

    /**
     * Download a file from server
     *
     * @param data Blob data from server
     * @param fileName Local file name
     */
    public static downloadFile(data: any, fileName: string): void {
        const blob = new Blob([data], { type: data.type });
        FileSaver.saveAs(blob, fileName);
    }

    /**
     * Download a JSON from server
     *
     * @param data Blob data from server
     */
    public static downloadJson(data: any): Promise<any> {
        return new Promise<any>((resolve) => {
            const reader = new FileReader();
            reader.onload = () => {
                resolve(JSON.parse(reader.result.toString()));
            };
            reader.readAsText(data);
        });
    }

    /**
     * Delay a timeout
     *
     * @param ms Timeout in mili seconds
     */
    public static delay(ms: number): Promise<void> {
        return new Promise( resolve => setTimeout(resolve, ms) );
    }

    /**
     * Generates a UUID
     *
     * @returns {string}
     */
    public static generateUID(): string {
        let array, uuid = '', i, digit = '';
        if (window.crypto && window.crypto.getRandomValues) {
            array = new Uint8Array(16);
            window.crypto.getRandomValues(array);

            for (i = 0; i < array.length; i++) {
                digit = array[i].toString(16).toLowerCase();
                if (digit.length === 1) {
                    digit = '0' + digit;
                }
                uuid += digit;
            }

            return uuid;
        }

        return (ToolService.S4() + ToolService.S4() + ToolService.S4() + ToolService.S4() +
            ToolService.S4() + ToolService.S4() + ToolService.S4() + ToolService.S4());
    }

    /**
     * Encrypt a password
     *
     * @param passPhrase Password phrase
     * @param plainText Clear text
     */
    public static encryptPassword(passPhrase: string, plainText: string): string {
        const iv = CryptoJS.lib.WordArray.random(128 / 8).toString(CryptoJS.enc.Hex);
        const salt = CryptoJS.lib.WordArray.random(128 / 8).toString(CryptoJS.enc.Hex);
        const key = CryptoJS.PBKDF2(passPhrase, CryptoJS.enc.Hex.parse(salt), { keySize: 4, iterations: 1000 });
        const encrypted = CryptoJS.AES.encrypt(plainText, key, { iv: CryptoJS.enc.Hex.parse(iv) });
        return btoa(iv + '::' + salt + '::' + encrypted.ciphertext.toString(CryptoJS.enc.Base64));
    }

    /**
     * Decrypt a password
     *
     * @param passPhrase Password phrase
     * @param cipherText Cipher text
     */
    public static decryptPassword(passPhrase: string, cipherText: string): string {
        cipherText = atob(cipherText);
        const tokens = cipherText.split('::');
        if (tokens.length === 3) {
            const iv = tokens[0];
            const salt = tokens[1];
            const key = CryptoJS.PBKDF2(passPhrase, CryptoJS.enc.Hex.parse(salt), { keySize: 4, iterations: 1000 });
            const cipherParams = CryptoJS.lib.CipherParams.create({ ciphertext: CryptoJS.enc.Base64.parse(tokens[2]) });
            const decrypted = CryptoJS.AES.decrypt(cipherParams, key, { iv: CryptoJS.enc.Hex.parse(iv) });
            return decrypted.toString(CryptoJS.enc.Utf8);
        }
        return null;
    }
}
