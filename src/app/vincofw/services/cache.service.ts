import {Injectable} from '@angular/core';
import {DatePipe, DecimalPipe, registerLocaleData} from '@angular/common';
import localeEs from '@angular/common/locales/es';
import localeEn from '@angular/common/locales/en';
import localeIt from '@angular/common/locales/it';
import localeCa from '@angular/common/locales/ca';
import localeBr from '@angular/common/locales/br';
import localeFr from '@angular/common/locales/fr';
import localeDe from '@angular/common/locales/de';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';
import {sprintf} from 'sprintf-js';
import {AdWindowDtoModel} from '../models/dto/ad-window-dto.model';
import {AdReferenceDtoModel} from '../models/dto/ad-reference-dto.model';
import {AdReferenceKeyValueModel} from '../models/dto/ad-reference-key-value.model';
import {AdPreferenceValueModel} from '../models/ad/ad-preference-value.model';
import {AdTabMainDtoModel} from '../models/dto/ad-tab-main-dto.model';
import {AdColumnDtoModel} from '../models/dto/ad-column-dto.model';
import {AdTableDtoModel} from '../models/dto/ad-table-dto.model';
import {AdProcessDtoModel} from '../models/dto/ad-process-dto.model';
import {AdPreferenceService} from './ad/ad-preference.service';
import {AdReferenceService} from './ad/ad-reference.service';
import {AdTableService} from './ad/ad-table.service';
import {AdTabService} from './ad/ad-tab.service';
import {AdProcessService} from './ad/ad-process.service';
import {AdWindowService} from './ad/ad-window.service';
import {AdTranslationService} from './ad/ad-translation.service';
import {LocalStoreService} from './local-store.service';
import {ConditionalService} from './conditional.service';
import {AutogenService} from './autogen.service';
import {AuthService} from './auth.service';
import {AppConfigService} from './app-config.service';
import {ToolService} from './tool.service';

registerLocaleData(localeEn);
registerLocaleData(localeIt);
registerLocaleData(localeBr);
registerLocaleData(localeCa);
registerLocaleData(localeEn);
registerLocaleData(localeFr);
registerLocaleData(localeEs);
registerLocaleData(localeDe);

@Injectable({
    providedIn: 'root'
})
export class AppCacheService {
    private cachedMessages: any;
    private cachedLanguage: string;
    private cachedTables: Map<string, AdTableDtoModel>;
    private cachedProcess: Map<string, AdProcessDtoModel>;
    private cachedWindows: Map<string, AdWindowDtoModel>;
    private cachedPreferences: Map<string, AdPreferenceValueModel>;
    private cachedReferences: Map<string, AdReferenceDtoModel>;
    private tableDirValues: Map<string, AdReferenceKeyValueModel[]>;

    public onMessagesReloaded: BehaviorSubject<any> = new BehaviorSubject(null);

    constructor(
        private authService: AuthService,
        private config: AppConfigService,
        private localStore: LocalStoreService,
        private translationService: AdTranslationService,
        private referenceService: AdReferenceService,
        private tableService: AdTableService,
        private processService: AdProcessService,
        private tabService: AdTabService,
        private windowService: AdWindowService,
        private preferenceService: AdPreferenceService,
        private conditionalService: ConditionalService,
        private autogenService: AutogenService
    ) {
        console.log('AppCacheService created ...');
        this.clearCache();
        this.authService.loginModified$.subscribe(() => {
            this.cachedPreferences = new Map<string, AdPreferenceValueModel>();
            if (this.authService.isLogged()) {
                this.preferenceService.preferences(this.authService.selectedRole).subscribe(result => {
                    result.forEach(current => this.cachedPreferences.set(current.property, current));
                });
                if (this.cachedLanguage !== this.config.idLanguage) {
                    this.getMessages(true).then(response => {
                        console.log(`AppCacheService:: Message reloaded`);
                    });
                }
            }
        });
    }

    /**
     * Check if messages are loaded
     */
    isLoadMessages(): boolean {
        return !ToolService.isNullOrUndefined(this.cachedMessages.messages);
    }

    /**
     * Clear all cached information
     */
    clearCache(): void {
        this.cachedMessages = {};
        this.cachedTables = new Map<string, AdTableDtoModel>();
        this.cachedProcess = new Map<string, AdProcessDtoModel>();
        this.cachedWindows = new Map<string, AdWindowDtoModel>();
        this.tableDirValues = new Map<string, AdReferenceKeyValueModel[]>();
        this.cachedReferences = new Map<string, AdReferenceDtoModel>();
        this.cachedPreferences = new Map<string, AdPreferenceValueModel>();
    }

    /**
     * Retrieves the messages from the backend by calling the 'messages' action
     */
    getMessages(ignoreCache: boolean = false): Promise<any> {
        return new Promise((resolve, reject) => {
            if (!ignoreCache && !this.isLoadMessages()) {
                resolve(this.cachedMessages);
            } else {
                this.translationService.messages(this.config.idLanguage).subscribe(result => {
                    this.cachedMessages = result;
                    this.cachedLanguage = this.config.idLanguage;
                    resolve(result);
                }, error => {
                    reject(error);
                });
            }
        });
    }

    /**
     * Gets the translation searching in cached messages
     *
     * @param value Translation key
     * @param args Arguments
     */
    getTranslation(value: string, args: any[] = null): string {
        if (
            ToolService.isNullOrUndefined(this.cachedMessages) || ToolService.isNullOrUndefined(this.cachedMessages.messages)
            || ToolService.isNullOrUndefined(this.cachedMessages.messages[value])
        ) {
            return value;
        }
        if (args && args.length > 0) {
            switch (args.length) {
                case 1:
                    return sprintf(this.cachedMessages.messages[value], args[0]);
                case 2:
                    return sprintf(this.cachedMessages.messages[value], args[0], args[1]);
                case 3:
                    return sprintf(this.cachedMessages.messages[value], args[0], args[1], args[2]);
                case 4:
                    return sprintf(this.cachedMessages.messages[value], args[0], args[1], args[2], args[3]);
                case 5:
                    return sprintf(this.cachedMessages.messages[value], args[0], args[1], args[2], args[3], args[4]);
                case 6:
                    return sprintf(this.cachedMessages.messages[value], args[0], args[1], args[2], args[3], args[4], args[5]);
            }
        }
        return sprintf(this.cachedMessages.messages[value]);
    }

    /**
     * Get parent Tab (Recursive)
     *
     * @param tabMain Main tab information
     * @param idTab Tab identifier
     */
    getParentTab(tabMain: AdTabMainDtoModel, idTab: string): AdTabMainDtoModel {
        const childTab: AdTabMainDtoModel = tabMain.childTabs ? tabMain.childTabs.find(tab => tab.idTab === idTab) : null;
        if (!childTab) {
            if (tabMain.childTabs) {
                for (const tab of tabMain.childTabs) {
                    const parentTab = this.getParentTab(tab, idTab);
                    if (parentTab) {
                        return parentTab;
                    }
                }
            }
        } else {
            return tabMain;
        }
        return null;
    }

    /**
     * Load table information
     *
     * @param idTable Table identifier
     * @param ignoreCache Ignore cache and force reload
     */
    loadTableInfo(idTable: string, ignoreCache: boolean = false): Observable<AdTableDtoModel> {
        if (this.cachedTables.has(idTable) && !ignoreCache) {
            return of(this.cachedTables.get(idTable));
        } else {
            return this.tableService.load(idTable).pipe(map(data => {
                this.cachedTables.set(idTable, data);
                return data;
            }));
        }
    }

    /**
     * Get table information from cache
     *
     * @param idTable Table identifier
     */
    getTableInfo(idTable: string): AdTableDtoModel {
        return this.cachedTables.has(idTable) ? this.cachedTables.get(idTable) : null;
    }

    /**
     * Load process information
     *
     * @param idProcess Process identifier
     * @param ignoreCache Ignore cache and force reload
     */
    getProcessInfo(idProcess: string, ignoreCache: boolean = false): Observable<AdProcessDtoModel> {
        if (this.cachedProcess.has(idProcess) && !ignoreCache) {
            return of(this.cachedProcess.get(idProcess));
        } else {
            return this.processService.load(idProcess).pipe(map(data => {
                this.cachedProcess.set(idProcess, data);
                return data;
            }));
        }
    }

    /**
     * Load window information
     *
     * @param idWindow Window identifier
     * @param ignoreCache Ignore cache and force reload
     */
    getWindowInfo(idWindow: string, ignoreCache: boolean = false): Observable<AdWindowDtoModel> {
        if (this.cachedWindows.has(idWindow) && !ignoreCache) {
            return of(this.cachedWindows.get(idWindow));
        } else {
            return this.windowService.load(idWindow).pipe(map(data => {
                this.cachedWindows.set(idWindow, data);
                return data;
            }));
        }
    }

    /**
     * Get window information from application cache
     *
     * @param idWindow Window identifier
     */
    getWindowFromCache(idWindow: string): AdWindowDtoModel {
        return this.cachedWindows.get(idWindow);
    }

    /**
     * Remove window information from cache
     *
     * @param idWindow Window identifier
     */
    removeWindowFromCache(idWindow: string): void {
        this.cachedWindows.delete(idWindow);
    }

    /**
     * Load tab information (tab level > 0)
     *
     * @param idWindow Window identifier
     * @param idTab Tab identifier
     */
    getTabInfo(idWindow: string, idTab: string): Observable<AdTabMainDtoModel> {
        if (this.cachedWindows.has(idWindow)) {
            const window = this.cachedWindows.get(idWindow);
            const result: AdTabMainDtoModel = this.getTabModel(window.mainTab.childTabs, idTab);
            if (result) {
                if (result.tab) {
                    return of(result);
                } else {
                    return this.tabService.load(idWindow, idTab).pipe(map(data => {
                        this.setTabModel(window.mainTab.childTabs, data);
                        return data;
                    }));
                }
            } else {
                return of(null);
            }
        } else {
            return of(null);
        }
    }

    /**
     * Get tab from child tabs (recursive)
     *
     * @param childTabs Child tab list
     * @param idTab Tab identifier
     */
    getTabModel(childTabs: AdTabMainDtoModel[], idTab: string): AdTabMainDtoModel {
        let result: AdTabMainDtoModel = childTabs.find(tab => tab.idTab === idTab);
        if (!result) {
            for (const tab of childTabs) {
                result = this.getTabModel(tab.childTabs, idTab);
                if (result) {
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Set tab to child tabs (recursive)
     *
     * @param childTabs Child tab list
     * @param tab Tab model
     */
    setTabModel(childTabs: AdTabMainDtoModel[], tab: AdTabMainDtoModel): boolean {
        if (childTabs) {
            const index = childTabs.findIndex(t => t.idTab === tab.idTab);
            if (index < 0) {
                for (const t of childTabs) {
                    if (this.setTabModel(t.childTabs, tab)) {
                        break;
                    }
                }
            }
            if (index >= 0) {
                childTabs[index] = tab;
                return true;
            }
        }
        return false;
    }

    /**
     * Load preference
     *
     * @param key Preference search key
     * @param ignoreCache Ignore cache and force reload
     */
    getPreference(key: string, ignoreCache: boolean = false): Observable<AdPreferenceValueModel> {
        if (!ignoreCache && this.cachedPreferences.has(key)) {
            return of(this.cachedPreferences.get(key));
        }
        return this.preferenceService.preference(key, this.authService.selectedRole).pipe(map(current => {
            this.cachedPreferences.set(key, current);
            return current;
        }));
    }

    /**
     * Load preference value
     *
     * @param key Preference search key
     * @param defaultValue Default value
     */
    getPreferenceValue(key: string, defaultValue: any = null): any {
        const value = this.cachedPreferences.get(key);
        return value ? value : defaultValue;
    }

    /**
     * Get reference
     *
     * @param references Reference identifier
     * @param ignoreCache Ignore cache and force reload
     */
    getReferenceInfo(references: string[] = [], ignoreCache: boolean = false): Observable<AdReferenceDtoModel[]> {
        let nonCached, cached;
        if (ignoreCache) {
            nonCached = references;
            cached = [];
        } else {
            nonCached = references.filter(current => !this.cachedReferences.has(current));
            const cachedIds = references.filter(current => this.cachedReferences.has(current));
            cached = cachedIds.map(current => this.cachedReferences.get(current));
        }
        if (nonCached.length === 0) {
            return of(cached);
        } else {
            return this.referenceService.loadReferences(nonCached).pipe(map(refs => {
                refs.forEach(current => this.cachedReferences.set(current.idReference, current));
                return cached.concat(refs);
            }));
        }
    }

    /**
     * Get references values
     *
     * @param reference Reference model
     * @param item Current item (entity)
     * @param ignoreCache Ignore cache and force reload
     */
    getReferenceValues(reference: AdReferenceDtoModel, item?: any, ignoreCache: boolean = false): Observable<AdReferenceKeyValueModel[]> {
        let keyReference = reference.idReference;
        if (reference.refTable && reference.refTable.sqlwhere) {
            keyReference += '_' + this.conditionalService.replaceSQLConditional(reference.refTable.sqlwhere, [], item);
        }

        if (!ignoreCache && this.tableDirValues.has(keyReference)) {
            return of(this.tableDirValues.get(keyReference));
        }

        switch (reference.rtype) {
            case 'TABLEDIR':
                return this.referenceService.loadTabledir([], reference, item).pipe(map(result => {
                    this.tableDirValues.set(keyReference, result);
                    return result;
                }));
            case 'LIST':
                return of(reference.refList.map(current => {
                    const keyValue = new AdReferenceKeyValueModel();
                    keyValue.value = current.name;
                    keyValue.key = current.value;
                    return keyValue;
                }));
            default:
                return of([]);
        }
    }

    /**
     * Visualizes the transformed value of the reference
     *
     * @param key Reference key to transform
     * @param idReference Reference ID
     * @param row Table row value
     * @param column Column information
     */
    getReferenceValue(key: string, idReference: string, row: any = null, column: AdColumnDtoModel = null): Promise<string> {
        return new Promise((resolve, reject) => {
            this.getReferenceInfo([idReference]).subscribe(references => {
                const reference = references[0];
                const countrycode = this.localStore.getItem('countryCode') || 'es';
                switch (reference.rtype) {
                    case 'YESNO':
                        const label = !key || key.toString() === false.toString() ? 'AD_labelNo' : 'AD_labelYes';
                        resolve(this.getTranslation(label));
                        break;

                    case 'LIST':
                    case 'TABLEDIR':
                        this.getReferenceValues(reference, row).pipe(
                            map(values => {
                                const found = values.find(current => current.key === key);
                                if (!ToolService.isNullOrUndefined(found)) {
                                    return found.value;
                                }
                                return key;
                            })
                        ).subscribe(
                        value => resolve(value),
                        error => {
                            console.error(error);
                            reject(error);
                        });
                        break;

                    case 'TABLE':
                        let tableValue = null;
                        if (reference.refTable && row[reference.refTable.tableFldName]) {
                            tableValue = row[reference.refTable.tableFldName];
                        }
                        if (tableValue && key === tableValue[reference.refTable.keyName]) {
                            resolve(tableValue[reference.refTable.displayName]);
                        } else {
                            if (key && typeof key === 'string') {
                                this.loadTableInfo(reference.refTable.idTable).subscribe(table => {
                                    if (table) {
                                        const autogen = this.autogenService.bindedService(table);
                                        autogen.get(key).subscribe(data => {
                                            if (data && data[reference.refTable.displayName]) {
                                                resolve(data[reference.refTable.displayName]);
                                            } else {
                                                resolve(key);
                                            }
                                        });
                                    } else {
                                        resolve(key);
                                    }
                                });
                            } else {
                                resolve('');
                            }
                        }
                        break;

                    case 'PASSWORD':
                        resolve('*****');
                        break;

                    case 'NUMBER':
                        this.getPreference('NumberDecimalPlaces').subscribe(result => {
                                const parsed = +key;
                                const decimalPipe: DecimalPipe = new DecimalPipe(countrycode);
                                resolve(decimalPipe.transform(parsed, result.value));
                            },
                            error => reject(error)
                        );
                        break;

                    case 'DATE':
                        this.getPreference('DateFormat').subscribe(result => {
                                const datePipe: DatePipe = new DatePipe(countrycode);
                                resolve(datePipe.transform(key, result.value));
                            },
                            error => reject(error)
                        );
                        break;

                    case 'TIME':
                        this.getPreference('TimeFormat').subscribe(result => {
                                const datePipe: DatePipe = new DatePipe(countrycode);
                                resolve(datePipe.transform(key, result.value));
                            },
                            error => reject(error)
                        );
                        break;

                    case 'TIMESTAMP':
                        this.getPreference('DateTimeFormat').subscribe(result => {
                                const datePipe: DatePipe = new DatePipe(countrycode);
                                resolve(datePipe.transform(key, result.value));
                            },
                            error => reject(error)
                        );
                        break;

                    case 'ID':
                    case 'IMAGE':
                    case 'FILEURL':
                    case 'FILE':
                    case 'TEXT':
                    case 'EMAIL':
                    case 'INTEGER':
                    case 'STRING':
                        resolve(key);
                        break;
                    default:
                        resolve(key);
                        break;
                }
            },
            error => {
                console.error(error);
                reject(error);
            });
        });
    }
}
