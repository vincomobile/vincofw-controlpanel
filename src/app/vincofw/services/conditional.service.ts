import {Injectable} from '@angular/core';
import {AdColumnDtoModel} from '../models/dto/ad-column-dto.model';
import {AdFieldDtoModel} from '../models/dto/ad-field-dto.model';
import {AuthService} from './auth.service';

import * as moment_ from 'moment';

const moment = moment_;

@Injectable({
    providedIn: 'root'
})
export class ConditionalService {

    constructor(private authService: AuthService) {

    }

    /**
     * Complete conditional expression for predefined values (@value@) and permissions (#permission#)
     *
     * @param conditional Expression
     */
    replaceFixedConditional(conditional: string): string {
        // Predefined replace patterns
        let pattern = new RegExp('\\@(.*?)\\@', 'g');
        while (pattern.test(conditional)) {
            const values = conditional.match(pattern);
            values.forEach(val => {
                if (val.length > 2) {
                    const item = val.substr(1, val.length - 2);
                    const replacePattern = new RegExp('\\@' + item + '\\@');
                    let value, day;
                    switch (item) {
                        case 'idClient':
                            value = this.authService.getLoggedIdClient();
                            break;
                        case 'idUserLogged':
                            value = this.authService.getLoggedIdUser();
                            break;
                        case 'now':
                            value = moment().format('DD/MM/YYYY HH:mm:ss');
                            break;
                        case 'today':
                            value = moment().format('DD/MM/YYYY');
                            break;
                        case 'yesterday':
                            value = moment().subtract(1, 'days').format('DD/MM/YYYY');
                            break;
                        case 'tomorrow':
                            value = moment().add(1, 'days').format('DD/MM/YYYY');
                            break;
                        case 'businessDay':
                            day = moment();
                            while (day.isoWeekday() > 5) {
                                day = day.add(1, 'days');
                            }
                            value = day.format('DD/MM/YYYY');
                            break;
                        case 'nextBusinessDay':
                            day = moment().add(1, 'days');
                            while (day.isoWeekday() > 5) {
                                day = day.add(1, 'days');
                            }
                            value = day.format('DD/MM/YYYY');
                            break;
                        case 'year':
                            value = moment().year();
                            break;
                        case 'month':
                            value = moment().month() + 1;
                            break;
                        case 'week':
                            value = moment().week();
                            break;
                        case 'firstMonthDay':
                            const today = new Date();
                            value = moment([today.getFullYear(), today.getMonth()]).format('DD/MM/YYYY');
                            break;
                        case 'lastMonthDay':
                            value = moment().endOf('month').format('DD/MM/YYYY');
                            break;
                        case 'firstYearDay':
                            value = moment([new Date().getFullYear()]).format('DD/MM/YYYY');
                            break;
                        case 'lastYearDay':
                            value = moment().endOf('year').format('DD/MM/YYYY');
                            break;
                        case 'true':
                            value = true;
                            break;
                        case 'false':
                            value = false;
                            break;
                        case 'null':
                            value = null;
                            break;
                        default:
                            value = item;
                            let itemParts;
                            if (item.indexOf('_daysago') > 0) {
                                itemParts = item.split('_');
                                if (itemParts.length === 2) {
                                    value = moment().subtract(parseInt(itemParts[0], 10), 'days').format('DD/MM/YYYY');
                                }
                            }
                            if (item.indexOf('nextdays_') === 0) {
                                itemParts = item.split('_');
                                if (itemParts.length === 2) {
                                    value = moment().add(parseInt(itemParts[1], 10), 'days').format('DD/MM/YYYY');
                                }
                            }
                    }
                    conditional = conditional.replace(replacePattern, value);
                }
            }, this);
        }
        // Permissions
        pattern = new RegExp('\\#(.*?)\\#', 'g');
        while (pattern.test(conditional)) {
            const permissions = conditional.match(pattern);
            permissions.forEach(val => {
                if (val.length > 2) {
                    const item = val.substr(1, val.length - 2);
                    const replacePattern = new RegExp('\\#' + item + '\\#');
                    const perm = this.authService.hasPermission(item);
                    conditional = conditional.replace(replacePattern, perm ? 'true' : 'false');
                }
            }, this);
        }
        return conditional;
    }

    /**
     * Complete JS conditional expression
     *
     * @param conditional Expression
     * @param column Column information
     * @param value Current value of field
     * @param itemValue Item object value
     */
    replaceJSConditional(conditional: string, column: AdColumnDtoModel, value: string, itemValue: any): string {
        conditional = this.replaceFixedConditional(conditional);
        if (conditional === 'null') {
            return null;
        }
        let pattern;
        // Column and value
        if (column) {
            pattern = new RegExp('\\{' + column.name + '\\}', 'g');
            if (pattern.test(conditional)) {
                return conditional.replace(pattern, value);
            }
        }
        // Item fields
        if (itemValue) {
            conditional = this.replaceItemValue(conditional, '{', '}', itemValue);
            conditional = this.replaceItemValue(conditional, '*', '*', itemValue);
        }

        return conditional;
    }

    /**
     * Replace Regular Expression
     *
     * @param conditional Expression
     * @param patternLeftChar Left pattern char { or *
     * @param patternRightChar Left pattern char { or *
     * @param itemValue Item object value
     */
    replaceItemValue(conditional: string, patternLeftChar: string, patternRightChar: string,  itemValue: any): string {
        const pattern = new RegExp(`\\${patternLeftChar}(.*?)\\${patternRightChar}`, 'g');
        while (pattern.test(conditional)) {
            const values = conditional.match(pattern);
            values.forEach(val => {
                if (val.length > 2) {
                    const item = val.substr(1, val.length - 2);
                    const replacePattern = new RegExp(`\\${patternLeftChar}${item}\\${patternRightChar}`);
                    conditional = conditional.replace(replacePattern, itemValue[item]);
                }
            }, this);
        }
        return conditional;
    }

    /**
     * Replace field value in conditional string, for example:
     *
     *    SQL condition: base = 1 and rtype = '{idReference.rtype}'
     *
     * @param conditional Conditional string
     * @param fields Field list
     * @param value Current value of field
     * @returns {*}
     */
    replaceSQLConditional(conditional: string, fields: AdFieldDtoModel[], value): string {
        conditional = this.replaceFixedConditional(conditional);
        if (value) {
            const condFields = this.getListenersField(conditional);
            condFields.forEach(key => {
                let pattern = new RegExp('\\{' + key + '\\}', 'g');
                if (pattern.test(conditional) && value[key]) {
                    conditional = conditional.replace(pattern, value[key]);
                }
                if (fields.length > 0) {
                    pattern = new RegExp('\\{' + key + '\\.(.*?)\\}', '');
                    if (pattern.test(conditional)) {
                        const values = conditional.match(pattern);
                        const field = fields.find(fld => fld.columnName === key);
                        conditional = conditional.replace(pattern, '#(' + field.relatedColumn.reference.idReference + ';' + value[key] + ').' + values[1] + '#');
                    }
                }
            });
        }
        return conditional;
    }

    /**
     * Verify if conditional string have replace all parameters
     *
     * @param conditional Conditional string
     * @returns boolean
     */
    isCompleteConditional(conditional): boolean {
        return !/\{(.*?)\}/.test(conditional) && !/\@(.*?)\@/.test(conditional);
    }

    /**
     * Get field names included in conditional
     *
     * @param conditional Conditional string
     */
    getListenersField(conditional): string[] {
        const result = [];
        const pattern = new RegExp('\\{(.*?)\\}', 'g');
        if (pattern.test(conditional)) {
            const values = conditional.match(pattern);
            values.forEach(val => {
                if (val.length > 2) {
                    let field = val.substr(1, val.length - 2);
                    if (field.indexOf('.') > 0) {
                        field = field.substring(0, field.indexOf('.'));
                    }
                    if (result.indexOf(field) < 0) {
                        result.push(field);
                    }
                }
            });
        }
        return result;
    }
}
