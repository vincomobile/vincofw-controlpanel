import {Injectable} from '@angular/core';
import {FontAwesomeIcons} from './interface.service';
import {FaIconLibrary} from '@fortawesome/angular-fontawesome';

@Injectable({
    providedIn: 'root'
})
export class AppFontawesomeService {

    icons: FontAwesomeIcons[] = [];

    constructor(
        private iconLibrary: FaIconLibrary
    ) {
        console.log('AppFontawesomeService constructor ...');
    }

    /**
     * Add Font Awesome Icons to library
     */
    addFontAwesomeIcons(icons: FontAwesomeIcons): void {
        console.log(`AppFontawesomeService.addFontAwesomeIcons:: ${icons.module} - ${icons.iconDefinition.length} icons added`);
        this.icons.push(icons);
        icons.iconDefinition.forEach(icon => {
            this.iconLibrary.addIcons(icon);
        });
    }
}
