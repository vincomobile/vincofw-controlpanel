import {Injectable} from '@angular/core';
import {BaseSettingsModel} from '../models/base-settings.models';

@Injectable({
    providedIn: 'root'
})
export class AppConfigService {

    public MAX_SEQNO = 1000000000;

    constructor(public settings: BaseSettingsModel) {
        console.log('AppCoreSettings constructor ...');
    }

    get restHostPath(): string {
        return this.settings.REST_HOST_PATH;
    }

    get restHostCore(): string {
        return this.settings.REST_HOST_CORE;
    }

    get restHostLogin(): string {
        return this.settings.REST_HOST_LOGIN;
    }

    get idClient(): string {
        return this.settings.ID_CLIENT;
    }

    set idClient(idClient: string) {
        this.settings.ID_CLIENT = idClient;
    }

    get appName(): string {
        return this.settings.APP_NAME;
    }

    set appName(appName: string) {
        this.settings.APP_NAME = appName;
    }

    get secretKey(): string {
        return this.settings.SECRET_KEY;
    }

    get appLogoUrl(): string {
        return this.settings.APPLICATION_LOGO_URL;
    }

    set appLogoUrl(appLogoUrl: string) {
        this.settings.APPLICATION_LOGO_URL = appLogoUrl;
    }

    get idLanguage(): string {
        return this.settings.ID_LANGUAGE;
    }

}
