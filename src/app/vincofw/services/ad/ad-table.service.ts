import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AdUserModel} from '../../models/ad/ad-user.model';
import {AdTableDtoModel} from '../../models/dto/ad-table-dto.model';
import {AdBaseService} from './ad-base.service';
import {AppConfigService} from '../app-config.service';

@Injectable({
    providedIn: 'root'
})
export class AdTableService extends AdBaseService<AdUserModel> {

    constructor(
        http: HttpClient,
        config: AppConfigService,
    ) {
        super(http, config);
        this.url = config.restHostCore + 'ad_table/';
    }

    public load(idTable: string): Observable<AdTableDtoModel> {
        return this.http.get(this.getUrlWithClient() + idTable + '/load', { }).pipe(
            map(response => {
                return response as AdTableDtoModel;
            })
        );
    }
}
