import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AdBaseModel} from '../../models/ad/ad-base.model';
import {AdChartResultModel} from '../../models/ad/ad-chart-result.model';
import {AdBaseService} from './ad-base.service';
import {AppConfigService} from '../app-config.service';

@Injectable({
    providedIn: 'root'
})
export class AdChartService extends AdBaseService<AdBaseModel> {

    constructor(
        http: HttpClient,
        config: AppConfigService
    ) {
        super(http, config);
        this.url = config.restHostCore + 'ad_chart/';
    }

    evaluate(idChart: string, idUser: string, constraints: string): Observable<AdChartResultModel> {
        // Initialize Params Object
        let params = new HttpParams();
        params = params.append('idLanguage', this.config.idLanguage);
        params = params.append('idUser', idUser);
        params = params.append('q', constraints);
        return this.http.get(this.getUrlWithClient() + 'evaluate/' + idChart, { params: params }).pipe(
            map(response => {
                return response as AdChartResultModel;
            })
        );
    }

}
