import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';
import {TranslationModel} from '../../models/translation.model';
import {AdTranslationModel} from '../../models/ad/ad-translation.model';
import {AdBaseService} from './ad-base.service';
import {AppConfigService} from '../app-config.service';
import {ToolService} from '../tool.service';

export interface IAdTranslationService {
    messages(idLanguage: string, idClient: string): Observable<TranslationModel>;
}

@Injectable({
    providedIn: 'root'
})
export class AdTranslationService extends AdBaseService<AdTranslationModel> implements IAdTranslationService {

    private translations: TranslationModel;
    private webTranslations: TranslationModel;

    constructor(
        http: HttpClient,
        config: AppConfigService,
    ) {
        super(http, config);
        this.url = config.restHostCore + 'ad_translation/';
    }

    public clearTranslations(): void {
        this.translations = null;
        this.webTranslations = null;
    }

    /**
     * Calls the backend 'messages' method
     *
     * @param idLanguage Language identifier
     * @param idClient Client identifier
     */
    public messages(idLanguage: string, idClient: string = '0'): Observable<TranslationModel> {
        if (!ToolService.isNullOrUndefined(this.translations)) {
            return of(this.translations);
        }
        // Initialize Params Object
        let params = new HttpParams();
        params = params.append('onlyMsg', 'true');
        if (!ToolService.isNullOrUndefined(idLanguage)) {
            params = params.append('idLanguage', idLanguage);
        }
        const url = this.url + idClient + '/';
        return this.http.get(url + 'messages', { params: params }).pipe(
            map(response => {
                this.translations = response['properties'] as TranslationModel;
                return this.translations;
            })
        );
    }

    /**
     * Get translations for element
     *
     * @param idClient Client identifier
     * @param idTable Table identifier
     * @param idColumn Column identifier
     * @param rowkey Row identifier
     */
    public getTranslations(idClient: string, idTable: string, idColumn: string, rowkey: string): Observable<AdTranslationModel[]> {
        let params = new HttpParams();
        params = params.append('q', `idClient=${idClient},idTable=${idTable},idColumn=${idColumn},rowkey=${rowkey}`);
        return this.http.get(this.url + idClient + '/', { params: params }).pipe(
            map(response => {
                return response['content'] as AdTranslationModel[];
            })
        );
    }

    public saveTranslations(item: any): Observable<void> {
        return this.http.put(this.getUrlWithClient() + 'save_translations', item).pipe(
            map(() => null)
        );
    }

}
