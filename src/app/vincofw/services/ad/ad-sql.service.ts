import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AdBaseModel} from '../../models/ad/ad-base.model';
import {ControllerResultModel} from '../../models/controller-result.model';
import {AdBaseService} from './ad-base.service';
import {AppConfigService} from '../app-config.service';

export interface SqlParams {
    sqlType: string;
    sqlQuery: string;
}

@Injectable({
    providedIn: 'root'
})
export class AdSqlService extends AdBaseService<AdBaseModel> {

    constructor(
        http: HttpClient,
        config: AppConfigService
    ) {
        super(http, config);
        this.url = config.restHostCore + 'ad_sql_exec/';
    }

    public exec(sql: SqlParams): Observable<ControllerResultModel> {
        return this.http.post(this.getUrlWithClient() + 'exec', sql).pipe(
            map(response => {
                return response as ControllerResultModel;
            })
        );
    }

}
