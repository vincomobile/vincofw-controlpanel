import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AdProcessLogModel} from '../../models/ad/ad-process-log.model';
import {AdBaseService} from './ad-base.service';
import {AppConfigService} from '../app-config.service';

@Injectable({
    providedIn: 'root'
})
export class AdProcessLogService extends AdBaseService<AdProcessLogModel> {

    constructor(
        http: HttpClient,
        config: AppConfigService
    ) {
        super(http, config);
        this.url = config.restHostCore + 'ad_process_log/';
    }
}
