import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AdUserModel} from '../../models/ad/ad-user.model';
import {ControllerResultModel} from '../../models/controller-result.model';
import {ToolService} from '../tool.service';
import {AppConfigService} from '../app-config.service';
import {AdBaseService} from './ad-base.service';

@Injectable({
    providedIn: 'root'
})
export class AdUserService extends AdBaseService<AdUserModel> {

    constructor(
        http: HttpClient,
        config: AppConfigService
    ) {
        super(http, config);
        this.url = config.restHostCore + 'ad_user/' + this.idClient;
    }

    public recover(idClient: string, email: string, languageCode: string): Observable<ControllerResultModel> {
        // Initialize Params Object
        let params = new HttpParams();
        params = params.append('idClient', idClient);
        params = params.append('email', email);
        if (!ToolService.isNullOrUndefined(languageCode)) {
            params = params.append('language', languageCode);
        }

        return this.http.get(this.getUrlWithClient() + 'recover', { params: params }).pipe(
            map(response => {
                return response as ControllerResultModel;
            })
        );
    }

    public changeProfile(idUser: string, idLanguage: string, idRole: string): Observable<void> {
        // Initialize Params Object
        let params = new HttpParams();
        params = params.append('idLanguage', idLanguage).append('idRole', idRole);

        return this.http.post(this.getUrlWithClient() +  idUser + '/change_role', null, { params: params }).pipe(
            map(() => {
                return;
            })
        );
    }

    public updateProfile(model: AdUserModel): Observable<AdUserModel> {
        // Initialize Params Object
        return this.http.put(this.getUrlWithClient() + model['id'] + '/update_profile', model).pipe(
            map(() => model)
        );
    }

    protected transformModel(model: AdUserModel): void {
        model.currentIdRole = model.defaultIdRole;
        model.currentIdLanguage = model.defaultIdLanguage;
    }

}
