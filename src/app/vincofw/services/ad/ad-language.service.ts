import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';
import {AdLanguageModel} from '../../models/ad/ad-language.model';
import {ToolService} from '../tool.service';
import {AdBaseService} from './ad-base.service';
import {AppConfigService} from '../app-config.service';
import {LocalStoreService} from '../local-store.service';

export interface IAdLanguageService {
    getClientLanguages(): Observable<AdLanguageModel[]>;

    getLanguage(idLanguage: string): Observable<AdLanguageModel>;
}

@Injectable({
    providedIn: 'root'
})
export class AdLanguageService extends AdBaseService<AdLanguageModel> implements IAdLanguageService {

    private languages: AdLanguageModel[];
    private urlClientLanguage: string;

    constructor(
        http: HttpClient,
        config: AppConfigService,
        private localStore: LocalStoreService
    ) {
        super(http, config);
        this.url = config.restHostCore + 'ad_language/';
    }

    getCachedLanguage(idLanguage: string): AdLanguageModel {
        return this.languages.find(current => current.idLanguage === idLanguage);
    }

    getLanguage(idLanguage: string): Observable<AdLanguageModel> {
        return this.getClientLanguages().pipe(
            map(response => {
                return response.find(current => {
                    return current.idLanguage === idLanguage;
                })
            })
        );
    }

    getClientLanguages(idClient: string = null, ignoreCache: boolean = false): Observable<AdLanguageModel[]> {
        if (!ignoreCache && !ToolService.isNullOrUndefined(this.languages)) { // static data
            return of(this.languages);
        }

        this.urlClientLanguage = this.config.restHostCore + 'ad_client_language/' + (idClient ? idClient : this.config.idClient);
        let params = new HttpParams();
        params = params.append('q', 'active=true');
        return this.http.get(this.urlClientLanguage + '/list', { params: params }).pipe(
            map(response => {
                this.languages = response['content'] as AdLanguageModel[];
                return this.languages;
            })
        );
    }

    set idLanguage(idLanguage: string) {
        if (idLanguage !== this.config.idLanguage) {
            const language = this.getCachedLanguage(idLanguage);
            this.localStore.setItem('idLanguage', idLanguage);
            this.localStore.setItem('language', language.iso2);
            this.localStore.setItem('countryCode', language.countrycode);
            this.config.settings.ID_LANGUAGE = idLanguage;
        }
    }

}
