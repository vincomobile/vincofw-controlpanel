import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AdUserModel} from '../../models/ad/ad-user.model';
import {AdPrivilegeModel} from '../../models/ad/ad-privilege.model';
import {AdBaseService} from './ad-base.service';
import {AppConfigService} from '../app-config.service';

@Injectable({
    providedIn: 'root'
})
export class AdPrivilegeService extends AdBaseService<AdPrivilegeModel> {

    constructor(
        http: HttpClient,
        config: AppConfigService
    ) {
        super(http, config);
        this.url = config.restHostCore + 'ad_privilege/';
    }

    users(privilege: string): Observable<AdUserModel[]> {
        return this.http.get(this.getUrlWithClient() + privilege + '/users', { }).pipe(
            map((response: any) => {
                return response as AdUserModel[];
            })
        );
    }

}
