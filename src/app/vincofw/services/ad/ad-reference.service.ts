import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';
import {AdHookNames} from '../../models/hook.model';
import {AdReferenceModel} from '../../models/ad/ad-reference.model';
import {AdReferenceDtoModel} from '../../models/dto/ad-reference-dto.model';
import {AdFieldDtoModel} from '../../models/dto/ad-field-dto.model';
import {AdReferenceKeyValueModel} from '../../models/dto/ad-reference-key-value.model';
import {SortData, SortOrder} from '../../models/page.model';
import {AppConfigService} from '../app-config.service';
import {AdBaseService} from './ad-base.service';
import {ToolService} from '../tool.service';
import {HookService} from '../hook.service';
import {ConditionalService} from '../conditional.service';

@Injectable({
    providedIn: 'root'
})
export class AdReferenceService extends AdBaseService<AdReferenceModel> {

    constructor(
        http: HttpClient,
        config: AppConfigService,
        private hookService: HookService,
        private conditionalService: ConditionalService
    ) {
        super(http, config);
        this.url = config.restHostCore + 'ad_reference/';
    }

    public load(idReference: string, idLanguage: string = null): Observable<AdReferenceDtoModel> {
        // Initialize Params Object
        let params = this.updateLanguageParam(null, idLanguage);
        return this.http.get(this.getUrlWithClient() + idReference + 'load', {params: params}).pipe(
            map(response => response as AdReferenceDtoModel)
        );
    }

    public loadReferences(references: string[], idLanguage: string = null): Observable<AdReferenceDtoModel[]> {
        // Initialize Params Object
        let params = this.updateLanguageParam(null, idLanguage);
        params = params.set('references', references.join(', '));

        return this.http.get(this.getUrlWithClient() + 'load', { params: params }).pipe(
            map(response => {
                const result: AdReferenceDtoModel[] = [];
                const values = response as AdReferenceDtoModel[];
                values.forEach(reference => {
                    result.push(new AdReferenceDtoModel(reference));
                });
                return result;
            })
        );
    }

    public loadTabledir(fields: AdFieldDtoModel[], reference: AdReferenceDtoModel, item?: any, idLanguage: string = null): Observable<AdReferenceKeyValueModel[]> {
        // Initialize Params Object
        let params = this.updateLanguageParam(null, idLanguage);
        params = params.append('_d', '' + new Date().getTime());

        // Sorting
        if (reference.refTable.sqlorderby) {
            const sortData: SortData[] = [];
            const orderby = reference.refTable.sqlorderby.split(',');
            orderby.forEach(ord => {
                const order = ord.trim().split(' ');
                sortData.push({
                    property: order[0],
                    direction: order.length === 2 ? (order[1].toLowerCase() === 'asc' ? SortOrder.ASC : SortOrder.DESC) : SortOrder.ASC
                });
            });
            params = params.append('sort', AdBaseService.getSortString(sortData));
        }

        const subject = new Subject<AdReferenceKeyValueModel[]>();

        // Execute Hook AD_QUERY_TABLE
        let q = '';
        this.hookService.execHook(
            AdHookNames.AD_QUERY_TABLE, {
                tableName: reference.refTable.tableName,
                constraints: q
            }
        ).then(response => {
            if (response.success) {
                response.results.forEach(r => q += ',' + r);
            }
            // Conditional WHERE
            if (!ToolService.isNullOrUndefined(reference.refTable.sqlwhere)) {
                let sqlWhere = reference.refTable.sqlwhere;
                sqlWhere = this.conditionalService.replaceSQLConditional(sqlWhere, fields, item);
                if (!this.conditionalService.isCompleteConditional(sqlWhere)) {
                    subject.next([]);
                    subject.complete();
                    return;
                }
                q += ', $extended$={' + sqlWhere + '}';
            }
            params = params.set('q', q);

            // Get values from server
            const url = this.config.restHostPath + reference.refTable.restPath + '/' + reference.refTable.tableName +
                (reference.refTable.tableName === 'ad_client' ? '' : ('/' + this.idClient));
            this.http.get(url, {params: params}).pipe(
                map(data => {
                    const responseData = data['content'] as any[];
                    if (ToolService.isNullOrUndefined(responseData)) {
                        return [];
                    }
                    const grouped = !ToolService.isNullOrUndefined(reference.refTable.idGroupedKey) && !ToolService.isNullOrUndefined(reference.refTable.idGroupedDisplay);
                    return responseData.map(current => {
                        const transformed = new AdReferenceKeyValueModel();
                        transformed.key = current[reference.refTable.keyName];
                        transformed.value = current[reference.refTable.displayName];
                        if (grouped) {
                            transformed.groupedKey = current[reference.refTable.groupedKeyName];
                            transformed.groupedValue = current[reference.refTable.groupedDisplayName];
                        }
                        return transformed;
                    })
                })
            ).subscribe(resp => {
                subject.next(resp as AdReferenceKeyValueModel[]);
                subject.complete();
            });
        });
        return subject;
    }
}
