import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';
import {AdRefListModel} from '../../models/ad/ad-ref-list.model';
import {AdBaseService} from './ad-base.service';
import {ToolService} from '../tool.service';
import {AppConfigService} from '../app-config.service';

export interface IAdRefListService {
    listReferenceLists(idReference: string): Observable<AdRefListModel[]>;
}

@Injectable({
    providedIn: 'root'
})
export class AdRefListService extends AdBaseService<AdRefListModel> implements IAdRefListService {
    private references = new Map<string, any[]>();

    constructor(
        http: HttpClient,
        config: AppConfigService
    ) {
        super(http, config);
        this.url = config.restHostCore + 'ad_ref_list/';
    }

    listReferenceLists(idReference: string): Observable<AdRefListModel[]> {
        if (this.references.has(idReference) && !ToolService.isNullOrUndefined(this.references.get(idReference))) {
            return of(this.references.get(idReference));
        }
        // Initialize Params Object
        let params = new HttpParams();
        params = params.append('q', 'idReference=' + idReference + ',active=1');
        return this.http.get(this.getUrlWithClient(), {params: params}).pipe(
            map(response => {
                const data = (response['content'] as AdRefListModel[]).sort(value => {
                    return value.seqno;
                });
                this.references.set(idReference, data);
                return response['content'] as AdRefListModel[];
            })
        );
    }

    getReferenceValue(idReference: string, value: string): string {
        if (this.references.has(idReference) && !ToolService.isNullOrUndefined(this.references.get(idReference))) {
            const found = this.references.get(idReference).find(current => {
                return current.value === value;
            });
            if (!ToolService.isNullOrUndefined(found)) {
                return found.name;
            }
            return value;
        }
    }

}
