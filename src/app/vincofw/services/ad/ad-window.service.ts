import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AdWindowDtoModel} from '../../models/dto/ad-window-dto.model';
import {AdTabMainDtoModel} from '../../models/dto/ad-tab-main-dto.model';
import {AdUserModel} from '../../models/ad/ad-user.model';
import {AdBaseService} from './ad-base.service';
import {AdTabService} from './ad-tab.service';
import {AppConfigService} from '../app-config.service';

@Injectable({
    providedIn: 'root'
})
export class AdWindowService extends AdBaseService<AdUserModel> {

    constructor(
        http: HttpClient,
        config: AppConfigService,
        private tabService: AdTabService
    ) {
        super(http, config);
        this.url = config.restHostCore + 'ad_window/';
    }

    public load(idWindow: string, idLanguage: string = null): Observable<AdWindowDtoModel> {
        // Initialize Params Object
        let params = this.updateLanguageParam(null, idLanguage);
        return this.http.get(this.getUrlWithClient() + idWindow + '/load', {params: params}).pipe(
            map(response => {
                const window = response as AdWindowDtoModel;
                window.mainTab.childTabs = response['mainTab']['childTabs'] as AdTabMainDtoModel[];
                this.tabService.processTab(window.mainTab);
                return window;
            })
        );
    }
}
