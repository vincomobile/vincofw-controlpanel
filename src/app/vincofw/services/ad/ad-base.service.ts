import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {PagedContentModel, SortData, SortOrder, SortParamModel} from '../../models/page.model';
import {AppConfigService} from '../app-config.service';
import {ToolService} from '../tool.service';
import {ControllerResultModel} from "../../models/controller-result.model";

@Injectable()
export class AdBaseService<T> {

    protected url: string;

    protected get idClient(): string {
        return this.config.idClient;
    }

    constructor(
        protected http: HttpClient,
        protected config: AppConfigService
    ) {
    }

    /**
     * Get the string representation of the SortData information to be sent to the backend
     *
     * @param sortData Sort information
     */
    public static getSortString(sortData: SortData[]): string {
        if (!ToolService.isNullOrUndefined(sortData)) {
            let str = '[';
            sortData.forEach((current, index) => {
                str += ('{ "property": "' + current.property + '", "direction": "' + SortOrder[current.direction] + '" }');
                if (index < sortData.length - 1) {
                    str += ',';
                }
            });
            str += ']';
            return str;
        }
        return null;
    }

    protected getQuery(model: T): string {
        return this.processQuery(model, false);
    }

    /**
     * Process query (constraints)
     *
     * @param model Filter model
     * @param exact Do exact match
     * @protected
     */
    protected processQuery(model: T, exact: boolean): string {
        let query = '';
        let hasFilterActive = false;
        const keys = Object.keys(model);
        keys.forEach(current => {
            const value = model[current];
            if (!ToolService.isNullOrUndefined(value) && value !== '') {
                if (current === 'active') {
                    hasFilterActive = true;
                }
                if ((ToolService.isNullOrUndefined(exact) || !exact) && typeof value === 'string' && value.indexOf('[') !== 0) {
                    query += (',' + current + '=%' + value + '%');
                } else {
                    query += (',' + current + '=' + value);
                }

            }
        });
        if (!hasFilterActive && keys.find(current => current === 'active')) {
            query += (',active=true');
        }
        return query;
    }

    /**
     * Make transformation to loaded model
     *
     * @param model
     */
    protected transformModel(model: T): void {

    }

    /**
     * Generates the correct URL by adding the client ID section
     */
    public getUrlWithClient(): string {
        return this.url + this.idClient + '/';
    }


    /**
     * Get generic server URL
     */
    protected getServerUrl(): string {
        return this.getUrlWithClient();
    }

    /**
     * Returns the list of elements
     *
     * @param params HttpParams
     * @param sortOrder Sort order
     * @param limit PageModel size. Defaults to 1000
     * @param page PageModel index. Defaults to 1
     */
    list(params: HttpParams, sortOrder: SortData[] = null, limit: number = 1000, page: number = 1): Observable<PagedContentModel<T>> {
        params = this.updateParams(params, sortOrder, limit, page);
        return this.http.get(this.getServerUrl(), { params: params }).pipe(
            map(response => {
                return this.mapPagedContent(response);
            })
        );
    }

    /**
     * Calls the GET method to retrieve the entity
     * @param id Entity Key
     * @param idClient
     */
    get(id: string, idClient: string = null): Observable<T> {
        let url = this.getServerUrl();
        if (!ToolService.isNullOrUndefined(idClient)) {
            url = this.url + idClient + '/';
        }
        return this.http.get(url + id, {}).pipe(
            map(response => {
                this.transformModel(response as T);
                return response as T;
            })
        );
    }

    /**
     * Calls the PUT method to update the entity
     * @param model Entity to be updated
     */
    update(model: T): Observable<void | string> {
        // Initialize Params Object
        return this.http.put(
            this.getServerUrl() + model['id'], model,
            {responseType: 'text'})
            .pipe(
                map(response => {
                    return response ? response as string : null;
                })
            );
    }

    /**
     * Calls the POST method to save the entity
     *
     * @param model Entity to be saved
     */
    save(model: T): Observable<any> {
        // Initialize Params Object
        return this.http.post(this.getServerUrl(), model, {
            responseType: 'text'
        });
    }

    /**
     * Delete table records
     *
     * @param entityIds Record identifier
     */
    delete(entityIds: string[]): Observable<any> {
        return this.http.post(this.getServerUrl() + '/delete_batch', entityIds);
    }

    /**
     * Sort item list
     *
     * @param model Sort parameters
     */
    sort(model: SortParamModel): Observable<ControllerResultModel> {
        return this.http.post(this.getServerUrl() + '/sort', model).pipe(
            map(response => {
                return response as ControllerResultModel;
            })
        );
    }

    /**
     * Maps the JSON response to object
     * @param response Mapped data
     */
    protected mapPagedContent(response: any): PagedContentModel<T> {
        const responseData = response['content'] as T[];
        responseData.forEach(value => {
            if (!ToolService.isNullOrUndefined(value['updated'])) {
                value['updated'] = new Date(value['updated']);
            }
            this.transformModel(value);
        });
        const pagedContent = new PagedContentModel<T>();
        pagedContent.content = responseData;
        pagedContent.firstPage = response['firstPage'] as boolean;
        pagedContent.lastPage = response['lastPage'] as boolean;
        pagedContent.number = response['number'] as number;
        pagedContent.totalPages = response['totalPages'] as number;
        pagedContent.totalElements = response['totalElements'] as number;
        pagedContent.numberOfElements = response['numberOfElements'] as number;
        pagedContent.size = response['size'] as number;
        return pagedContent;
    }

    /**
     * Update Http Params list
     *
     * @param params Http params
     * @param sortOrder Sort items
     * @param limit Items by page
     * @param page Page number
     */
    protected updateParams(params: HttpParams, sortOrder: SortData[], limit, page: number): HttpParams {
        if (ToolService.isNullOrUndefined(params)) {
            params = new HttpParams();
        }
        if (!ToolService.isNullOrUndefined(sortOrder)) {
            params = params.append('sort', AdBaseService.getSortString(sortOrder));
        }
        if (!ToolService.isNullOrUndefined(limit)) {
            params = params.append('limit', limit.toString());
        }
        if (!ToolService.isNullOrUndefined(page)) {
            params = params.append('page', page.toString());
        }
        return params;
    }

    /**
     * Update Http Params list with a language
     *
     * @param params Http params
     * @param idLanguage Language identifier
     * @protected
     */
    protected updateLanguageParam(params: HttpParams, idLanguage: string): HttpParams {
        if (ToolService.isNullOrUndefined(params)) {
            params = new HttpParams();
        }
        if (ToolService.isNullOrUndefined(idLanguage)) {
            idLanguage = this.config.idLanguage;
        }
        return params.append('idLanguage', idLanguage);
    }

}
