import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {RemoteCallModel} from '../../models/remote-call.model';
import {AdTerminalApiDtoModel, AdTerminalDtoModel} from '../../models/dto/ad-terminal-dto.model';
import {AdTerminalService} from './ad-terminal.service';
import {AppCacheService} from '../cache.service';
import {ToolService} from '../tool.service';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AdHardwareManagerService {

    constructor(
        private http: HttpClient,
        private terminalService: AdTerminalService,
        private cacheService: AppCacheService
    ) {
    }

    /**
     * Call to remote service
     *
     * @param apiName API name
     * @param params URL params
     * @param body Body
     */
    public remoteCall(apiName: string, params: any, body: any): Observable<RemoteCallModel> {
        const result = new RemoteCallModel(true);
        const terminalConfig = this.terminalService.getTerminalConfig();
        if (!ToolService.isNullOrUndefined(terminalConfig)) {
            const api = terminalConfig.apis.find(item => item.name === apiName);
            if (!ToolService.isNullOrUndefined(api)) {
                switch (api.method) {
                    case 'GET':
                        return this.http.get(
                            api.url, {
                                headers: this.getHeaders(terminalConfig, api),
                                params: params
                            }).pipe(
                                map(response => {
                                    result.data = response;
                                    return result;
                                })
                            );

                    case 'POST':
                        return this.http.post(
                            api.url, body, {
                                headers: this.getHeaders(terminalConfig, api),
                                params: params
                            }).pipe(
                                map(response => {
                                    result.data = response;
                                    return result;
                                })
                            );

                    default:
                        result.message = this.cacheService.getTranslation('AD_RemoteCallErrorMethodNotSupported', [api.method]);
                }
            } else {
                result.message = this.cacheService.getTranslation('AD_RemoteCallErrorNotFoundApi', [apiName]);
            }
        } else {
            result.message = this.cacheService.getTranslation('AD_RemoteCallErrorTerminalUndefined');
        }
        return of(result);
    }

    private getHeaders(terminalConfig: AdTerminalDtoModel, api: AdTerminalApiDtoModel): HttpHeaders {
        const result = {};
        if (api.authorization === 'HTTP_BASIC') {
            result['Authorization'] = `Basic ${btoa(terminalConfig.hmUser + ':' + terminalConfig.hmPassword)}`;
        }
        if (api.extraHeaders) {
            const items = api.extraHeaders.split('\n');
            items.forEach(header => {
                const pair = header.split('=');
                if (pair && pair.length >= 2) {
                    const key = pair.shift();
                    result[key] = pair.join('=');
                }
            });
        }
        return new HttpHeaders(result);
    }
}
