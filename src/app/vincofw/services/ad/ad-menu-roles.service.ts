import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ControllerResultModel} from '../../models/controller-result.model';
import {AdBaseModel} from '../../models/ad/ad-base.model';
import {AdBaseService} from './ad-base.service';
import {AppConfigService} from '../app-config.service';

@Injectable({
    providedIn: 'root'
})
export class AdMenuRolesService extends AdBaseService<AdBaseModel> {

    constructor(
        http: HttpClient,
        config: AppConfigService
    ) {
        super(http, config);
        this.url = config.restHostCore + 'ad_menu_roles/' + this.idClient;
    }

    saveMenu(params: any): Observable<ControllerResultModel> {
        return this.http.post(this.getUrlWithClient() + 'save_menu', params).pipe(
            map(response => {
                return response as ControllerResultModel;
            })
        );
    }

}
