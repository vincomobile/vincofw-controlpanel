import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AdClientModel} from '../../models/ad/ad-client.model';
import {AdClientDtoModel} from '../../models/dto/ad-client-dto.model';
import {AdBaseService} from './ad-base.service';
import {AppConfigService} from '../app-config.service';

@Injectable({
    providedIn: 'root'
})
export class AdClientService extends AdBaseService<AdClientModel> {

    constructor(
        http: HttpClient,
        config: AppConfigService
    ) {
        super(http, config);
        this.url = config.restHostCore + 'ad_client';
    }

    getUrlWithClient(): string {
        return this.url  + '/';
    }

    getClientForUrl(url: string): Observable<AdClientDtoModel> {
        let params = new HttpParams();
        params = params.append('url', url);
        return this.http.get(this.getUrlWithClient() + 'client_for_url', { params: params }).pipe(
            map(response => {
                return response as AdClientDtoModel;
            })
        );
    }
}
