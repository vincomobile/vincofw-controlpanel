import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AdBaseModel} from '../../models/ad/ad-base.model';
import {AdMultiSelectValueModel} from '../../models/ad/ad-multi-select-value.model';
import {AdBaseService} from './ad-base.service';
import {AppConfigService} from '../app-config.service';

@Injectable({
    providedIn: 'root'
})
export class AdFieldService extends AdBaseService<AdBaseModel> {

    constructor(
        http: HttpClient,
        config: AppConfigService
    ) {
        super(http, config);
        this.url = config.restHostCore + 'ad_field/';
    }

    multiselect(idField: string, parentFieldId: string, rowKey: string): Observable<AdMultiSelectValueModel[]> {
        let params = new HttpParams();
        params = params.append('id', idField);
        params = params.append('parentFieldId', parentFieldId);
        params = params.append('rowKey', rowKey);
        return this.http.get(this.getUrlWithClient() + idField + '/multiselect', { params: params }).pipe(
            map(resp => resp['content'] as AdMultiSelectValueModel[])
        );
    }

}
