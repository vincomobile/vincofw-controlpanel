import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AdReferenceModel} from '../../models/ad/ad-reference.model';
import {AdPreferenceValueModel} from '../../models/ad/ad-preference-value.model';
import {AdPermissionModel} from '../../models/ad/ad-permission.model';
import {AppConfigService} from '../app-config.service';
import {AdBaseService} from './ad-base.service';
import {ToolService} from '../tool.service';

export interface IAdPreferenceService {
    permissions(idRole: string): Observable<AdPermissionModel[]>;
    preference(name: string, idRole: string, params: string): Observable<AdPreferenceValueModel>;
    preferences(idRole: string): Observable<AdPreferenceValueModel[]>;
}

@Injectable({
    providedIn: 'root'
})
export class AdPreferenceService extends AdBaseService<AdReferenceModel> implements IAdPreferenceService {

    constructor(
        http: HttpClient,
        config: AppConfigService
    ) {
        super(http, config);
        this.url = config.restHostCore + 'ad_preference/';
    }

    permissions(idRole: string): Observable<AdPermissionModel[]> {
        let params = new HttpParams();
        params = params.set('idRole', idRole);
        return this.http.get(this.getUrlWithClient() + 'permissions', { params: params }).pipe(
            map(response => {
                return response['properties'].permissions as AdPermissionModel[];
            })
        );
    }

    preference(name: string, idRole: string, params: string = null): Observable<AdPreferenceValueModel> {
        let args = new HttpParams();
        args = args.set('name', name);
        args = args.set('idRole', idRole);
        if (!ToolService.isNullOrUndefined('params')){
            args = args.set('params', params);
        }
        return this.http.get(this.getUrlWithClient() + 'preference', { params: args }).pipe(
            map(response => {
                return response['properties'].preference as AdPreferenceValueModel;
            })
        );
    }

    preferences(idRole): Observable<AdPreferenceValueModel[]> {
        let args = new HttpParams();
        args = args.set('idRole', idRole);
        return this.http.get(this.getUrlWithClient() + 'preferences', { params: args }).pipe(
            map(response => {
                return response['properties'].preferences as AdPreferenceValueModel[];
            })
        );
    }
}
