import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ControllerResultModel} from '../../models/controller-result.model';
import {AdFieldGridModel} from '../../models/ad/ad-field-grid.model';
import {AdBaseService} from './ad-base.service';
import {AppConfigService} from '../app-config.service';

@Injectable({
    providedIn: 'root'
})
export class AdFieldGridService extends AdBaseService<AdFieldGridModel> {

    constructor(
        http: HttpClient,
        config: AppConfigService
    ) {
        super(http, config);
        this.url = config.restHostCore + 'ad_field_grid/';
    }

    saveColumns(idTab: string, idUser: string, fields: string): Observable<ControllerResultModel> {
        let params = new HttpParams();
        params = params.append('idTab', idTab);
        params = params.append('idUser', idUser);
        params = params.append('fields', fields);
        return this.http.post(this.getUrlWithClient() + 'save', null, { params: params }).pipe(
            map(resp => resp as ControllerResultModel)
        );
    }

    deleteColumns(idTab: string, idUser: string): Observable<any> {
        let params = new HttpParams();
        params = params.append('idTab', idTab);
        params = params.append('idUser', idUser);
        return this.http.delete(this.getUrlWithClient() + 'delete', { params: params });
    }
}
