import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ControllerResultModel} from '../../models/controller-result.model';
import {AdProcessDtoModel} from '../../models/dto/ad-process-dto.model';
import {AdProcessResponse} from '../../models/ad/ad-process.model';
import {AdBaseService} from './ad-base.service';
import {AppConfigService} from '../app-config.service';

@Injectable({
    providedIn: 'root'
})
export class AdProcessService extends AdBaseService<AdProcessDtoModel> {

    constructor(
        http: HttpClient,
        config: AppConfigService
    ) {
        super(http, config);
        this.url = config.restHostCore + 'ad_process/';
    }

    public load(idProcess: string, idLanguage: string = null): Observable<AdProcessDtoModel> {
        // Initialize Params Object
        let params = this.updateLanguageParam(null, idLanguage);
        return this.http.get(this.getUrlWithClient() + idProcess + '/load', { params: params }).pipe(
            map(response => {
                return response as AdProcessDtoModel;
            })
        );
    }

    public exec(idProcess: string, idProcessExec: string, paramValues: string, idLanguage: string = null): Observable<AdProcessResponse> {
        const params = this.getParams(paramValues, idLanguage);
        return this.http.post(
            this.getUrlWithClient() + 'exec/' + idProcess + '/' + idProcessExec, null,
            { params: params, responseType: 'blob', observe: 'response' }
        ).pipe(
            map(response => {
                return {
                    body: response.body as Blob,
                    fileName: response.headers.get('X-Filename')
                };
            })
        );
    }

    public execHtml(idProcess: string, idProcessExec: string, paramValues: string, idLanguage: string = null): Observable<string> {
        const params = this.getParams(paramValues, idLanguage);
        return this.http.post(
            this.getUrlWithClient() + 'exec/' + idProcess + '/' + idProcessExec, null,
            { params: params, responseType: 'text' }
        ).pipe(
            map(response => {
                return response as string;
            })
        );
    }

    public cancelExec(idProcess: string, idProcessExec: string): Observable<ControllerResultModel> {
        return this.http.post(this.getUrlWithClient() + '/cancel_exec/' + idProcess + '/' + idProcessExec, null).pipe(
            map(response => {
                return response as ControllerResultModel;
            })
        );
    }

    public schedule(idProcess: string, idClient, idUser: string, cron: string): Observable<ControllerResultModel> {
        let params = new HttpParams();
        params = params.append('idUser', idUser);
        params = params.append('cron', cron);
        return this.http.post(this.url + idClient + '/schedule/' + idProcess, null, { params: params }).pipe(
            map(response => {
                return response as ControllerResultModel;
            })
        );
    }

    public clearSchedule(idProcess: string): Observable<void> {
        return this.http.delete(this.getUrlWithClient() + 'clear_schedule/' + idProcess).pipe(
            map(() => {
                return null;
            })
        );
    }

    private getParams(paramValues: string, idLanguage: string): HttpParams {
        // Initialize Params Object
        let params = this.updateLanguageParam(null, idLanguage);
        params = params.append('params', paramValues);
        return params;
    }
}
