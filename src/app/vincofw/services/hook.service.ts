import {Injectable} from '@angular/core';
import {HookExecResult, HookModel} from '../models/hook.model';
import {ToolService} from './tool.service';

@Injectable({
    providedIn: 'root'
})
export class HookService {

    hooks: HookModel[] = [];

    constructor() {

    }

    /**
     * Register a Hook
     *
     * @param hook Hook
     */
    registerHook(hook: HookModel): void {
        console.info('Register hook: ' + hook.identifier());
        const found = this.hooks.find(h => h.id === hook.id);
        if (!found) {
            this.hooks.push(hook);
        }
    }

    /**
     * Unregister a Hook
     *
     * @param id Hook identifier
     */
    unregisterHook(id: string): void {
        const indx = this.hooks.findIndex(hook => hook.id === id);
        if (indx >= 0) {
            console.info('Remove hook: ' + this.hooks[indx].identifier());
            this.hooks.splice(indx, 1);
        } else {
            console.error('Not found hook: ' + id);
        }
    }

    /**
     * Get a list of hooks
     *
     * @param name Hook name
     * @param args Arguments
     * @returns {Array}
     */
    getHooks(name: string, args: any): Promise<any>[] {
        const promises = [];
        this.hooks.forEach(hook => {
            if (hook.name === name) {
                promises.push(hook.handler(args).then());
            }
        });
        return promises;
    }

    /**
     * Excecute register hooks
     *
     * @param name Hook name
     * @param args Arguments
     */
    execHook(name, args): Promise<HookExecResult> {
        const execHooks = this.getHooks(name, args);
        return new Promise((resolve, reject) => {
            if (execHooks.length > 0) {
                Promise.all(execHooks)
                    .then(data => {
                        resolve({ success: true, results: data.filter(d => !ToolService.isNullOrUndefined(d)) });
                    })
                    .catch(error => {
                        console.warn('Reject Hook: ' + name);
                        if (error) {
                            console.warn(error);
                        }
                        reject(error);
                    });
            } else {
                resolve({ success: true, results: [] });
            }
        });
    }

}
