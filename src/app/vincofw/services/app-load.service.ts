import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {EventAppLoadType} from '../models/event/event-app-load.model';
import {BaseSettingsModel} from '../models/base-settings.models';
import {LocalStoreService} from './local-store.service';
import {AuthService} from './auth.service';
import {AppCacheService} from './cache.service';
import {AppLoadInterface} from './interface.service';
import {AdClientService} from './ad/ad-client.service';
import {environment} from '../../../environments/environment';
import {APP_CONFIG} from '../../extensions/config/app-config.model';
import {GlobalEventsService} from './global-events.service';
import {AppFontawesomeService} from './app-fontawesome.service';
import {FW_ICON_DEFINITION} from '../models/fa-icons-definition';

@Injectable({
    providedIn: 'root'
})
export class AppLoadService implements AppLoadInterface {

    private themeName: string = null;

    constructor(
        private http: HttpClient,
        private fontawesomeService: AppFontawesomeService,
        private settings: BaseSettingsModel,
        private localStore: LocalStoreService,
        private authService: AuthService,
        private cacheService: AppCacheService,
        private clientService: AdClientService,
        private globalEventsService: GlobalEventsService
    ) {
        this.settings.ID_LANGUAGE = this.localStore.getItem('idLanguage') || APP_CONFIG.ID_LANGUAGE;
        this.settings.LANGUAGE = this.localStore.getItem('language') || APP_CONFIG.LANGUAGE;
        this.settings.COUNTRY_CODE = this.localStore.getItem('countryCode') || APP_CONFIG.COUNTRY_CODE;
        this.globalEventsService.formFieldOnChangeFunction$.subscribe(data => {
            if (data.functionName === 'materialColorChange') {
                const isColorVariant = data.field.columnName.endsWith('Variant');
                const otherFieldName = isColorVariant ? data.field.columnName.substring(0, data.field.columnName.indexOf('Variant')) : data.field.columnName + 'Variant';
                const viewFieldName = (isColorVariant ? otherFieldName : data.field.columnName) + 'View';
                const viewColor = data.fields.find(fld => fld.columnName === viewFieldName);
                const otherField = data.fields.find(fld => fld.columnName === otherFieldName);
                if (viewColor && otherField) {
                    viewColor.caption = isColorVariant ? data.item[otherFieldName] + '-' + data.value : data.value + '-' + data.item[otherFieldName];
                    let value = {};
                    value[viewColor.columnName] = viewColor.caption + '-bg';
                    data.form.patchValue(value);
                }
            }
        });
    }

    /**
     * Initial application load
     *
     * @param data Data load in previous load services
     */
    load(data: any): Promise<any> {
        return new Promise((resolve, reject) => {
            console.log(`AppLoadService.load:: Start app initialization`);
            this.authService.initUserLoad().then(() => {
                this.loadMessages(login => {
                    if (!this.authService.isLogged()) {
                        this.getClientForUrl(resolve);
                    } else {
                        const client = this.authService.getClientGUI();
                        this.setLoadStatus(EventAppLoadType.ChangeTheme, client.guiColorTheme === 'custom' ? client.nick : client.guiColorTheme);
                        this.setLoadStatus(EventAppLoadType.ChangeClient, client);
                        resolve(client);
                    }
                }, reject);
            }).catch(() => {
                this.loadMessages(() => {
                    this.getClientForUrl(resolve);
                }, reject);
            });
        });
    }

    /**
     * Add Font Awesome Icons to library
     */
    addFontAwesomeIcons(): void {
        this.fontawesomeService.addFontAwesomeIcons({
            module: 'Vinco Mobile FW',
            iconDefinition: FW_ICON_DEFINITION
        });
    }

    /**
     * Send event for load status subject
     *
     * @param event Event type
     * @param value Event value
     */
    setLoadStatus(event: EventAppLoadType, value: any = null): void {
        this.globalEventsService.loadEvents.next({ eventType: event, value: value });
    }

    /**
     * Change application theme
     *
     * @param themeName Theme name
     * @param force Force theme change
     */
    changeTheme(themeName: string, force: boolean = false): void {
        if (force || this.themeName === null) {
            const webUrl = window.location.origin + environment.contextPath;
            this.themeName = themeName;
            console.log(`AppLoadService.changeTheme:: ${themeName}`);
            // Change theme CSS
            const themeAsset = document.getElementById('themeAsset');
            const themeCss = `${webUrl}/app/extensions/assets/themes/${themeName}.css`;
            if (themeAsset) {
                this.checkCss(themeCss).subscribe(exists => {
                    if (exists) {
                        // @ts-ignore
                        themeAsset.href = themeCss;
                    }
                })
            } else {
                console.error(`AppLoadService.changeTheme:: ${themeName}. Can not find HTML element: themeAsset`);
            }
            // Change client CSS
            const clientAsset = document.getElementById('clientAsset');
            const clientCss = `${webUrl}/app/extensions/assets/clients/${themeName}.css`;
            if (clientAsset) {
                this.checkCss(clientCss).subscribe(exists => {
                    if (exists) {
                        // @ts-ignore
                        clientAsset.href = clientCss;
                    }
                })
            } else {
                console.error(`AppLoadService.changeTheme:: ${themeName}. Can not find HTML element: clientAsset`);
            }
        }
    }

    /**
     * Get default client to current URL
     *
     * @param resolve Successful callback
     * @private
     */
    getClientForUrl(resolve): void {
        let url = environment.webSite ? environment.webSite : document.location.hostname;
        console.log(`AppLoadService.getClientForUrl:: ${url}`);
        this.clientService.getClientForUrl(url).subscribe(client => {
            if (client != null) {
                this.authService.setClientGUI(client);
                this.setLoadStatus(EventAppLoadType.ChangeTheme, client.nick);
                this.setLoadStatus(EventAppLoadType.ChangeClient, client);
            }
            resolve(client);
        });
    }

    /**
     * Check if CSS file exist on server
     *
     * @param cssFile Css file path
     * @private
     */
    private checkCss(cssFile: string): Observable<boolean> {
        return this.http.get(cssFile, { responseType: 'text' }).pipe(
            map(() => {
                return true;
            }),
            catchError(() => {
                return of(false);
            })
        );
    }

    /**
     * Load message from server
     *
     * @param resolve Successful callback
     * @param reject Failure callback
     * @private
     */
    private loadMessages(resolve, reject): void {
        this.cacheService.getMessages(true).then(() => {
            console.log(`AppLoadService.loadMessages:: Message loaded`);
            if (!this.localStore.getItem('reloading')) {
                const userInfo = this.authService.getUserInfo();
                if (userInfo) {
                    if (!userInfo.rememberMe) {
                        this.authService.clearCredentials();
                    }
                }
            } else {
                this.localStore.removeItem('reloading');
            }
            this.setLoadStatus(EventAppLoadType.Messages);
            resolve();
        })
        .catch(error => {
            console.error(error);
            reject();
        });
    }

}
