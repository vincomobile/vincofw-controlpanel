import {EventEmitter, Injectable} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {EventChartTypeModel} from '../models/event/event-chart.model';
import {EventFieldOnchangefunctionModel} from '../models/event/event-field-onchangefunction.model';
import {EventProcessExecuteModel, EventProcessOpenModel} from '../models/event/event-process-execute.model';
import {EventTabHeaderMode, EventTabHeaderModel, EventTabHeaderType} from '../models/event/event-tab-header.model';
import {AdFieldDtoModel} from '../models/dto/ad-field-dto.model';
import {AdProcessLogDtoModel} from '../models/dto/ad-process-log-dto.model';
import {EventAppLoad} from '../models/event/event-app-load.model';

@Injectable({
    providedIn: 'root'
})
export class GlobalEventsService {

    loadEvents = new Subject<EventAppLoad>();

    toggleUserProfile: EventEmitter<void> = new EventEmitter<void>();
    toggleDrawer: EventEmitter<void> = new EventEmitter<void>();

    // TODO: Define classes or interfaces for Event Objects

    public appStarted$: ReplaySubject<void> = new ReplaySubject<void>(1, 1000);

    public chartChangeType$: EventEmitter<EventChartTypeModel> = new EventEmitter<EventChartTypeModel>();

    public closeAllTabs$: EventEmitter<void> = new EventEmitter<void>();

    public executeCode$: EventEmitter<any> = new EventEmitter<any>();

    public formFieldOnChangeFunction$: EventEmitter<EventFieldOnchangefunctionModel> = new EventEmitter<EventFieldOnchangefunctionModel>();

    public itemChanged$: EventEmitter<any> = new EventEmitter<any>();

    public menuLoaded$: EventEmitter<void> = new EventEmitter<void>();

    public processExecuted$: EventEmitter<EventProcessExecuteModel> = new EventEmitter<EventProcessExecuteModel>();
    public processLog$: EventEmitter<AdProcessLogDtoModel> = new EventEmitter<AdProcessLogDtoModel>();
    public processOpen$: EventEmitter<EventProcessOpenModel> = new EventEmitter<EventProcessOpenModel>();

    public reloadWindow$: EventEmitter<string> = new EventEmitter<string>();

    public selectedTabChange$: EventEmitter<any> = new EventEmitter<any>();

    public tabHeaderRecordEvent$: EventEmitter<EventTabHeaderModel> = new EventEmitter<EventTabHeaderModel>();

    constructor() {
    }

    /**
     * Global notification: App started and ready
     */
    public notifyAppStarted(): void {
        console.log('GlobalEventsService.notifyAppStarted ...');
        this.appStarted$.next();
    }

    /**
     * Global notification: Change the chart type
     *
     * @param idChart Chart identifier
     * @param previousChartType Previous chart type
     */
    public notifyChartChangeType(idChart: string, previousChartType: string): void {
        this.chartChangeType$.emit({
            idChart: idChart,
            previousChartType: previousChartType
        });
    }

    /**
     * Global notification: Close all open tabs
     */
    public notifyCloseAllTabs(): void {
        this.closeAllTabs$.emit();
    }

    /**
     * Global notification: Execute Javascript/TypeScript code
     *
     * @param jsCode: Code name identifier
     * @param execFrom: Execute form Datatable (LIST) or form (FORM)
     * @param items: Selected items
     */
    public notifyExecuteCode(jsCode: string, execFrom: string, items: any[]): void {
        this.executeCode$.emit({
            name: jsCode,
            execFrom: execFrom,
            items: items
        });
    }

    /**
     * Global notification: Menu is loaded
     *
     * @param functionName: 'onchangefunction' value
     * @param isNew: Is a new record
     * @param form: Form
     * @param item: Item values
     * @param value: Change value
     * @param field: Field changed
     * @param fields: All form fields
     */
    public notifyFormFieldOnChangeFunction(functionName: string, isNew: boolean, form: FormGroup, item: any, value: any, field: AdFieldDtoModel, fields: AdFieldDtoModel[]): void {
        this.formFieldOnChangeFunction$.emit({
            functionName: functionName,
            isNew: isNew,
            form: form,
            item: item,
            value: value,
            field: field,
            fields: fields
        });
    }

    /**
     * Change form editor item (Ex.: Navigate to: First, Previous, Next, Last)
     *
     * @param idTab Emitter tab identifier
     * @param item Item
     */
    public notifyItemChanged(idTab: string, item: any): void {
        this.itemChanged$.emit({ idTab: idTab, item: item });
    }

    /**
     * Global notification: Menu is loaded
     */
    public notifyMenuLoaded(): void {
        this.menuLoaded$.emit();
    }

    /**
     * Global notification: Reload tab request
     *
     * @param idProcess: Process identifier
     * @param success: If process execution is correct
     */
    public notifyProcessExecuted(idProcess: string, success: boolean): void {
        this.processExecuted$.emit({
            idProcess: idProcess,
            success: success
        });
    }

    /**
     * Global notification: Process execution log
     *
     * @param log: Log information
     */
    public notifyProcessLog(log: AdProcessLogDtoModel): void {
        this.processLog$.emit(log);
    }

    /**
     * Global notification: Process open
     *
     * @param idProcess: Process identifier
     * @param buttonType: Button type
     * @param item: Form item
     */
    public notifyProcessOpen(idProcess: string, buttonType: string, item: any = null): void {
        this.processOpen$.emit({
            idProcess: idProcess,
            buttonType: buttonType,
            item: item
        });
    }

    /**
     * Global notification: Reload tab request
     *
     * @param idWindow: Window identifier
     */
    public requestReloadedWindow(idWindow: string): void {
        this.reloadWindow$.emit(idWindow);
    }

    /**
     * Change selected tab (Form edition)
     *
     * @param idFormTab Form tab identifier
     * @param idTab Selected tab identifier
     */
    public notifySelectedTabChange(idFormTab: string, idTab: string): void {
        this.selectedTabChange$.emit({ idFormTab: idFormTab, idTab: idTab });
    }

    /**
     * Press tab header button
     *
     * @param event Event type
     * @param idTab Selected tab identifier
     */
    public notifyTabHeaderRecordEvent(event: EventTabHeaderType, idTab: string): void {
        this.tabHeaderRecordEvent$.emit(new EventTabHeaderModel(event, EventTabHeaderMode.Notify, idTab));
    }

    /**
     * Emit tab header event (simulate push button)
     *
     * @param event Event type
     * @param idTab Selected tab identifier
     * @param caption Caption for tab header
     */
    public emitTabHeaderRecordEvent(event: EventTabHeaderType, idTab: string, caption: string = null): void {
        this.tabHeaderRecordEvent$.emit(new EventTabHeaderModel(event, EventTabHeaderMode.Emit, idTab, caption));
    }

}
