import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {EMPTY, Observable, of, Subject} from 'rxjs';
import {JwtHelperService} from '@auth0/angular-jwt';
import {AuthLoginResponse, AuthRoleModel, AuthUserModel} from '../models/auth-user.model';
import {ControllerResultModel} from '../models/controller-result.model';
import {AdPermissionModel} from '../models/ad/ad-permission.model';
import {AdClientModel} from '../models/ad/ad-client.model';
import {ToolService} from './tool.service';
import {LocalStoreService} from './local-store.service';
import {AppConfigService} from './app-config.service';
import {AdUserService} from './ad/ad-user.service';
import {AdLanguageService} from './ad/ad-language.service';
import {AdClientService} from './ad/ad-client.service';
import {AdPreferenceService} from './ad/ad-preference.service';
import {AdTerminalService} from './ad/ad-terminal.service';
import {AdClientDtoModel} from '../models/dto/ad-client-dto.model';
import {GlobalEventsService} from './global-events.service';
import {EventAppLoadType} from '../models/event/event-app-load.model';

import * as moment_ from 'moment';

const moment = moment_;

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private loginModifiedSource = new Subject<boolean>();
    private client: AdClientModel = new AdClientModel();
    private clientGUI: AdClientDtoModel;
    private url: string;
    private cachePermissions: AdPermissionModel[] = [];

    loginModified$ = this.loginModifiedSource.asObservable();

    constructor(
        private http: HttpClient,
        private localStore: LocalStoreService,
        private userService: AdUserService,
        private languageService: AdLanguageService,
        private clientService: AdClientService,
        private config: AppConfigService,
        private preferenceService: AdPreferenceService,
        private terminalService: AdTerminalService,
        private globalEventsService: GlobalEventsService
    ) {
        console.log('AuthService created ...');
        this.url = this.config.restHostLogin;
    }

    /******************************************************************************************************************
     * Public methods
     *****************************************************************************************************************/

    /**
     * Checks if the role matches the active role
     *
     * @param property Property to check
     * @returns {boolean} True if user has the permission and is allowed
     */
    public hasPermission(property: string): boolean {
        return this.cachePermissions.findIndex(value => value.property === property && value.allow) >= 0;
    }

    /**
     * Load permission for role
     */
    public loadPermissions(): Promise<void> {
        return new Promise(resolve => {
            this.preferenceService.permissions(this.selectedRole).subscribe(response => {
                this.cachePermissions = response;
                resolve();
            });
        });
    }

    /**
     * Change password
     *
     * @param oldPassword Old password
     * @param newPassword New password
     * @param security Security level
     */
    public changePassword(oldPassword: string, newPassword: string, security: string): Observable<ControllerResultModel> {
        if (!this.isLogged()) {
            return of({ success: false, errCode: -1, message: 'not_logged', properties: null });
        }

        const path = this.getLoggedIdClient() + '/' + this.getUserInfo().idUserLogged;
        const url = this.config.restHostCore + 'ad_user/' + path + '/change_password';
        let params = new HttpParams();
        params = params.append('size', '' + newPassword.length)
            .append('security', security)
            .append('oldPassword', ToolService.encryptPassword(path, oldPassword))
            .append('newPassword', ToolService.encryptPassword(path, newPassword));
        return this.http.post(url, null, { params: params }).pipe(
            map((response: any) => {
                if (response.success) {
                    const userInfo = this.getUserInfo();
                    userInfo.crytpo = ToolService.encryptPassword(this.config.secretKey, newPassword);
                    this.localStore.setItem('userInfo', JSON.stringify(userInfo));
                }
                return response as ControllerResultModel;
            })
        );
    }

    /**
     * Initialize user loading
     */
    public initUserLoad(): Promise<any> {
        return new Promise((resolve, reject) => {
            const userInfo = this.getUserInfo();
            const jwtToken =  this.localStore.getItem('jwtToken');
            if (userInfo && jwtToken) {
                this.config.idClient = userInfo.idClient;
                if (this.isExpiredToken()) {
                    this.localStore.removeItem('expireAt');
                    this.localStore.removeItem('jwtToken');
                    if (userInfo.crytpo) {
                        const password = ToolService.decryptPassword(this.config.secretKey, userInfo.crytpo);
                        this.getJWT(userInfo.idClient, userInfo.username, password, userInfo.rememberMe, userInfo.loginValues).subscribe(() => {
                            this.loadPermissionsAndCheckTerminal(resolve, reject);
                        },
                        error => {
                            this.clearCredentials();
                            reject(false);
                        });
                    } else {
                        this.clearCredentials();
                        resolve(false);
                    }
                } else {
                    this.loadPermissionsAndCheckTerminal(resolve, reject);
                }
            } else {
                resolve(false);
            }
        });
    }

    /**
     * Get JWT authentication token
     *
     * @param idClient Client identifier
     * @param username User name
     * @param password User password
     * @param rememberMe Set to true to store do autologin
     * @param loginValues Custom values
     */
    public getJWT(idClient: string, username: string, password: string, rememberMe: boolean = false, loginValues: string[] = null): Observable<string> {
        return this.http.post(
            this.config.restHostCore + 'authenticate',
            {
                idClient: idClient,
                username: username,
                password: ToolService.encryptPassword(username, password),
                loginValues: loginValues
            }).pipe(
                map(response => {
                    let token: string = response['token'];
                    const helper = new JwtHelperService();
                    const decodedToken = helper.decodeToken(token);
                    if (decodedToken) {
                        const userInfo = this.getUserInfo() || new AuthUserModel();
                        userInfo.idUserLogged = decodedToken.sub;
                        userInfo.idClient = idClient;
                        userInfo.name = decodedToken.name;
                        userInfo.email = decodedToken.email;
                        userInfo.username = username.indexOf('$') > 0 ? username.split('$')[1] : username;
                        userInfo.crytpo = ToolService.encryptPassword(this.config.secretKey, password);
                        userInfo.rememberMe = rememberMe;
                        userInfo.loginValues = decodedToken.loginValues;
                        userInfo.privileges = decodedToken.privileges;
                        if (!userInfo.roles) {
                            userInfo.roles = [];
                        }
                        this.localStore.setItem('jwtToken', token);
                        this.localStore.setItem('userInfo', JSON.stringify(userInfo));
                        this.localStore.setItem('expireAt', JSON.stringify(decodedToken.exp));
                    } else {
                        console.log('Invalid authentication token');
                        token = null;
                    }
                    return token;
                }),
                catchError(err => {
                    console.log(err);
                    return of(null);
                })
            );
    }

    /**
     * Performs the authentication login logic using store login information
     */
    public autoLogin(): Promise<boolean> {
        return new Promise<boolean>((resolve) => {
            const userInfo = this.getUserInfo();
            if (userInfo && this.isExpiredToken()) {
                this.localStore.removeItem('expireAt');
                this.localStore.removeItem('jwtToken');
                if (userInfo.crytpo) {
                    const password = ToolService.decryptPassword(this.config.secretKey, userInfo.crytpo);
                    this.getJWT(userInfo.idClient, userInfo.username, password, userInfo.rememberMe, userInfo.loginValues).subscribe(() => {
                            resolve(true);
                        },
                        () => {
                            resolve(false);
                        }
                    );
                } else {
                    resolve(false);
                }
            } else {
                resolve(false);
            }
        });
    }

    /**
     * Performs the authentication login logic
     *
     * @param username User name
     * @param idRole Role identifier
     * @param idLanguage Language identifier
     */
    public login(username: string, idRole: string = null, idLanguage: string = null): Promise<AuthLoginResponse> {
        return new Promise((resolve, reject) => {
            let params = new HttpParams();
            params = params.append('login', username);
            this.http.get(this.url + 'login/privileges/', { params: params }).subscribe(
                response => {
                    const responseProperties = response['properties'];
                    const userInfo = this.getUserInfo();
                    userInfo.roles = responseProperties.roles;
                    userInfo.photoUrl = responseProperties.user.photoUrl && responseProperties.user.photoUrl.indexOf('http') === 0 ? responseProperties.user.photoUrl : null;
                    userInfo.defaultRole = responseProperties.user.defaultIdRole;
                    userInfo.defaultLanguage = responseProperties.user.defaultIdLanguage;
                    this.selectedRole = idRole ? idRole : userInfo.defaultRole;
                    if (userInfo.roles && userInfo.roles.length === 0) {
                        this.clearCredentials();
                        reject({ rolesEmpty: true });
                    } else {
                        userInfo.idClient = userInfo.roles.find(current => current.idRole === this.selectedRole).idClient;
                        this.config.idClient = userInfo.idClient;
                        this.localStore.setItem('userInfo', JSON.stringify(userInfo));
                        this.languageService.getClientLanguages(userInfo.idClient, true).subscribe(() => {
                            this.languageService.idLanguage = idLanguage ? idLanguage : userInfo.defaultLanguage;
                            this.getLoggedClient(true).then(() => {
                                this.globalEventsService.loadEvents.next({ eventType: EventAppLoadType.ChangeTheme, value: this.clientGUI.nick });
                                this.globalEventsService.loadEvents.next({ eventType: EventAppLoadType.ChangeClient, value: this.clientGUI });
                                const loginResponse = new AuthLoginResponse();
                                loginResponse.isLoggedIn = true;
                                loginResponse.serverResponse = responseProperties;
                                resolve(loginResponse);
                            });
                        });
                    }
                },
                error => {
                    this.clearCredentials();
                    reject(error);
                }
            );
        });
    }

    /**
     * Performs the authentication logout logic
     */
    public logout(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.config.idClient = '';
            this.http.get(this.config.restHostLogin + 'login/logout', {})
                .subscribe(
                    data => {
                        this.clearCredentials();
                        resolve(data);
                    },
                    error => {
                        this.clearCredentials();
                        reject(error);
                        return EMPTY;
                    }
                );
        });
    }

    /**
     * Notify login event
     *
     * @param success Success login or not
     */
    public notifyLogin(success: boolean): void {
        this.loginModifiedSource.next(success);
    }

    /**
     * Clear user credentials
     */
    public clearCredentials(): void {
        this.cachePermissions = [];
        this.clientGUI = null;
        this.client = new AdClientModel();
        this.localStore.removeItem('userInfo');
        this.localStore.removeItem('jwtToken');
        this.localStore.removeItem('expireAt');
        this.localStore.removeItem('selectedRole');
        this.loginModifiedSource.next(false);
    }

    /**
     * Check if user is logged
     *
     * @param emitEvent If emit login modified event
     */
    public isLogged(emitEvent: boolean = false): boolean {
        const token = this.localStore.getItem('jwtToken');
        const result = !ToolService.isNullOrUndefined(token)
            && !ToolService.isNullOrUndefined(this.localStore.getItem('expireAt')) && moment().unix() <= this.getExpiration();
        if (emitEvent) {
            this.loginModifiedSource.next(result);
        }
        return result;
    }

    /**
     * Get user information
     */
    public getUserInfo(): AuthUserModel {
        const userInfo = this.localStore.getItem('userInfo');
        return userInfo ? JSON.parse(userInfo) : null;
    }

    /**
     * Check if current user has a privilege
     *
     * @param priv Privilege
     */
    public hasPrivilege(priv: string): boolean {
        if (this.isLogged()) {
            const userInfo = this.getUserInfo();
            return !ToolService.isNullOrUndefined(userInfo.privileges.find(p => p === priv));
        }
        return false;
    }

    /**
     * Get client identifier for logged user and role
     */
    public getLoggedIdClient(): string {
        if (this.isLogged()) {
            const userInfo = this.getUserInfo();
            if (!ToolService.isNullOrUndefined(userInfo) && !ToolService.isNullOrUndefined(userInfo.roles) && userInfo.roles.length > 0) {
                const selected = userInfo.roles.find(current => {
                    return current.idRole === this.selectedRole;
                });
                if (!ToolService.isNullOrUndefined(selected)){
                    return selected.idClient;
                }
                return this.getUserInfo().roles[0].idClient;
            }
        }
        return null;
    }

    /**
     * Get user identifier for logged user
     */
    public getLoggedIdUser(): string {
        return this.isLogged() ? this.getUserInfo().idUserLogged : null;
    }

    /**
     * Get client information for logged user and role
     */
    public getLoggedClient(force: boolean = false): Promise<AdClientModel> {
        return new Promise((resolve, reject) => {
            const loggedIdClient = this.getLoggedIdClient();
            if (!force && this.client.idClient === loggedIdClient) {
                resolve(this.client);
            } else {
                if (ToolService.isNullOrUndefined(loggedIdClient)) {
                    resolve(null);
                } else {
                    this.clientService.get(loggedIdClient).subscribe(result => {
                        this.client = result;
                        this.clientGUI = {
                            idClient: this.client.idClient,
                            name: this.client.name,
                            nick: this.client.nick,
                            urlPattern: this.client.urlPattern,
                            logo: this.client.logo,
                            logoFaviIcon: this.client.logoFaviIcon,
                            guiButton: this.client.guiButton,
                            guiFormField: this.client.guiFormField,
                            guiColorTheme: this.client.guiColorTheme,
                            guiWidth: this.client.guiWidth,
                            guiUx: this.client.guiUx,
                            toolbarPosition: this.client.toolbarPosition,
                            navbarPosition: this.client.navbarPosition,
                            navbarPrimaryColor: this.client.navbarPrimaryColor,
                            navbarPrimaryColorVariant: this.client.navbarPrimaryColorVariant,
                            navbarSecondaryColor: this.client.navbarSecondaryColor,
                            navbarSecondaryColorVariant: this.client.navbarSecondaryColorVariant,
                            primaryColor: this.client.primaryColor,
                            primaryColorVariant: this.client.primaryColorVariant,
                            secondaryColor: this.client.secondaryColor,
                            secondaryColorVariant: this.client.secondaryColorVariant,
                            msgInfoColor: this.client.msgInfoColor,
                            msgInfoColorVariant: this.client.msgInfoColorVariant,
                            msgSuccessColor: this.client.msgSuccessColor,
                            msgSuccessColorVariant: this.client.msgSuccessColorVariant,
                            msgWarningColor: this.client.msgWarningColor,
                            msgWarningColorVariant: this.client.msgWarningColorVariant,
                            msgErrorColor: this.client.msgErrorColor,
                            msgErrorColorVariant: this.client.msgErrorColorVariant,
                            footerHidden: this.client.footerHidden,
                            footerPosition: this.client.footerPosition
                        };
                        resolve(result);
                    }, error => {
                        reject(error);
                    });
                }
            }
        });
    }

    public getClientGUI(): AdClientDtoModel {
        return this.clientGUI;
    }

    public setClientGUI(clientGUI: AdClientDtoModel) {
        this.localStore.setItem('clientGUI', JSON.stringify(clientGUI));
        this.clientGUI = clientGUI;
    }

    /**
     * Get default client parameters
     */
    getClientDefault(): AdClientDtoModel {
        const client = new AdClientDtoModel();
        client.guiButton = 'mat-raised-button';
        client.guiFormField = 'standard';
        client.guiColorTheme = 'indigo-pink';
        client.guiWidth = 'fullwidth';
        client.toolbarPosition = 'fixed';
        client.navbarPosition = 'left';
        client.navbarPrimaryColor = 'blue';
        client.navbarPrimaryColorVariant = '700';
        client.navbarSecondaryColor = 'blue';
        client.navbarSecondaryColorVariant = '900';
        client.footerHidden = false;
        client.footerPosition = 'fixed';
        return client;
    }

    /**
     * Init the recover password flow
     *
     * @param idClient Client identifier
     * @param email User Email
     */
    public recoverPassword(idClient: string, email: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.languageService.getClientLanguages(idClient).subscribe(result => {
                const language = result.find(current => {
                    return current.idLanguage === this.config.idLanguage;
                });
                let languageCode = null;
                if (!ToolService.isNullOrUndefined(language)) {
                    languageCode = language.iso2;
                }
                this.userService.recover(idClient, email, languageCode).subscribe(resultRecover => {
                    resolve(resultRecover);
                }, error => {
                    console.error(error);
                    reject(error);
                });
            }, error => {
                console.error(error);
                reject(error);
            });
        });

    }

    public isExpiredToken(): boolean {
        const expiration = this.localStore.getItem('expireAt');
        if (!ToolService.isNullOrUndefined(expiration)) {
            return moment().unix() > this.getExpiration();
        }
        return true;
    }

    getExpiration(): any {
        const expiration = this.localStore.getItem('expireAt');
        return JSON.parse(expiration);
    }

    public getActiveRole(): AuthRoleModel {
        return this.getUserInfo().roles.find( r => r.idRole === this.selectedRole);
    }

    public set selectedRole(idRole: string){
        this.localStore.setItem('selectedRole', idRole);
    }

    public get selectedRole(): string{
        return this.localStore.getItem('selectedRole');
    }

    private loadPermissionsAndCheckTerminal(resolve, reject): void {
        this.loadPermissions().then(() => {
            const ta = this.hasPermission('TerminalAuthentication');
            this.terminalService.checkTerminalAuthentication(ta).then(success => {
                if (success) {
                    this.getLoggedClient()
                        .then(() => {
                            resolve(true);
                        })
                        .catch(() => {
                            this.clearCredentials();
                            reject(false);
                        });
                } else {
                    this.clearCredentials();
                    reject(false);
                }
            });
        });
    }

}
