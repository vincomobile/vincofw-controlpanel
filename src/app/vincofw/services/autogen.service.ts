import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AdTableDtoModel} from '../models/dto/ad-table-dto.model';
import {AdBaseModel} from '../models/ad/ad-base.model';
import {SortData} from '../models/page.model';
import {ControllerResultModel} from '../models/controller-result.model';
import {AdBaseService} from './ad/ad-base.service';
import {AppConfigService} from './app-config.service';

@Injectable({
    providedIn: 'root'
})
export class AutogenService extends AdBaseService<AdBaseModel> {

    private table: AdTableDtoModel;

    constructor(
        http: HttpClient,
        config: AppConfigService
    ) {
        super(http, config);
    }

    /**
     * Bind the service to real table
     *
     * @param table Table model
     */
    public bindedService(table: AdTableDtoModel): AutogenService {
        const service = new AutogenService(this.http, this.config);
        service.table = table;
        return service;
    }

    /**
     * Get generic server URL
     */
    protected getServerUrl(): string {
        if (this.table.name !== 'ad_client') {
            const prefix = this.table.name.substring(0, this.table.name.indexOf('_'));
            const version = this.config.settings.TABLE_PREFIX_VERSIONS.find(p => p === prefix);
            return this.config.restHostPath + this.table.restPath + '/' + this.table.name + '/' + this.config.idClient +
                (version ? '/v' + this.table.version : '') + '/';
        } else {
            return this.config.restHostPath + this.table.restPath + '/' + this.table.name + '/';
        }
    }

    /**
     * Export table records
     *
     * @param params HttpParams
     * @param sortOrder Sort order
     * @param idTab Tab identifier
     * @param idUser User identifier
     */
    exportData(params: HttpParams = new HttpParams(), sortOrder: SortData[] = null, idTab: string, idUser: string): Observable<Blob> {
        params = this.updateParams(params, sortOrder, null, null);
        params = params.append('idUser', idUser);
        return this.http.get(this.getServerUrl() + 'export/' + idTab, { params: params, responseType: 'blob' });
    }

    /**
     * Delete image
     *
     * @param id Row identifier
     * @param field Field name
     * @param fileName File name
     */
    deleteImage(id: string, field: string, fileName: string): Observable<void> {
        return this.http.delete(this.getServerUrl() + 'delete_image/' + id + '/' + field + '/' + fileName).pipe(
            map(response => null)
        );
    }

    /**
     * Save multiple records
     *
     * @param entity Sort parameters
     * @param entityIds Record identifier
     */
    saveMultiple(entity: AdBaseModel, entityIds: string[]): Observable<ControllerResultModel> {
        let params: HttpParams = new HttpParams();
        entityIds.forEach(id => params = params.append('ids', id));
        return this.http.post(this.getServerUrl() + 'save_multiple', entity, { params: params }).pipe(
            map(response => {
                return response as ControllerResultModel;
            })
        );
    }

    /**
     * Get audit information
     *
     * @param idRow Row identifier
     */
    loadRowAudit(idRow: string): Observable<ControllerResultModel> {
        let params = new HttpParams();
        params = params.append('id', idRow);
        return this.http.get(this.getServerUrl() + 'row_audit', { params: params }).pipe(
            map(response => response as ControllerResultModel)
        );
    }

}
