import {ComponentFactoryResolver, Injectable} from '@angular/core';
import {VincoHostDirective} from '../directives/vinco-host.directive';
import {
    ComponentCategory,
    ComponentModel,
    ComponentRegisterModel,
    VincoComponent
} from '../models/component-register.model';
import {NotFoundComponent} from '../components/ui/not-found/not-found.component';

@Injectable({
  providedIn: 'root'
})
export class ComponentsService {

    private register: ComponentRegisterModel = new ComponentRegisterModel();

    constructor(private componentFactoryResolver: ComponentFactoryResolver) {
    }

    setComponent(category: ComponentCategory, component: ComponentModel): void {
        console.log(`Set component: ${category} - ${component.component.name}`);
        this.register.setComponent(category, component);
    }

    setCustomComponent(name: string, component: ComponentModel): void {
        console.log(`Set custom component: ${name} - ${component.component.name}`);
        this.register.setCustomComponent(name, component);
    }

    loadComponent(host: VincoHostDirective, category: ComponentCategory): void {
        let fwComp = this.register.getComponent(category);
        if (fwComp == null) {
            this.register.setComponent(ComponentCategory.NotFound, new ComponentModel(NotFoundComponent, { name: category }));
            fwComp = this.register.getComponent(ComponentCategory.NotFound);
        }
        this.loadComp(host, fwComp, { name: category });
    }

    loadCustomComponent(host: VincoHostDirective, name: string, data: any): void {
        let fwComp = this.register.getCustomComponent(name);
        if (fwComp == null) {
            this.register.setComponent(ComponentCategory.NotFound, new ComponentModel(NotFoundComponent, { name: name }));
            fwComp = this.register.getComponent(ComponentCategory.NotFound);
        }
        this.loadComp(host, fwComp, data);
    }

    private loadComp(host: VincoHostDirective, fwComp: ComponentModel, data: any): void {
        fwComp.data = data;
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(fwComp.component);
        const viewContainerRef = host.viewContainerRef;
        viewContainerRef.clear();
        const componentRef = viewContainerRef.createComponent<VincoComponent>(componentFactory);
        componentRef.instance.data = fwComp.data;
    }
}
