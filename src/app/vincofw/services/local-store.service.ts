import {Injectable} from '@angular/core';
import {Page, SortData} from '../models/page.model';
import {AppConfigService} from './app-config.service';
import {ToolService} from './tool.service';

@Injectable({
    providedIn: 'root'
})
export class LocalStoreService {

    constructor(private config: AppConfigService) {

    }

    /**
     * Set item in local store
     *
     * @param key Key
     * @param value Value
     */
    setItem(key: string, value: string): void {
        localStorage.setItem(this.config.appName + '_' + key, value);
    }

    /**
     * Remove item from local store
     *
     * @param key Key
     */
    removeItem(key: string): void {
        localStorage.removeItem(this.config.appName + '_' + key);
    }

    /**
     * Get item from local storage
     *
     * @param key Key
     */
    getItem(key: string): string {
        return localStorage.getItem(this.config.appName + '_' + key);
    }

    /**
     * Save filters and page information to local store
     *
     * @param idTab Tab identifier
     * @param filterValues Filter values
     * @param page Page information
     * @param sortData Sort column information
     */
    public saveTabInfo(idTab: string, filterValues: any, page: Page, sortData: SortData): void {
        const key = 'tabInfo_' + idTab;
        const value = this.getItem(key);
        const info = !ToolService.isNullOrUndefined(value) ? JSON.parse(value) : { filters: null, page: null, sortData: null };
        if (!ToolService.isNullOrUndefined(filterValues)) {
            info.filters = { ...filterValues};
            for (let objKey in info.filters) {
                if (info.filters.hasOwnProperty(objKey)) {
                    if (info.filters[objKey] instanceof Object && info.filters[objKey]._isAMomentObject) {
                        if (info.filters[objKey].isValid()) {
                            info.filters[objKey] = info.filters[objKey].format('DD/MM/YYYY');
                        } else {
                            info.filters[objKey] = '';
                        }
                    }
                }
            }
        }
        if (!ToolService.isNullOrUndefined(page)) {
            info.page = page;
        }
        if (!ToolService.isNullOrUndefined(sortData)) {
            info.sortData = sortData;
        }
        this.setItem(key, JSON.stringify(info));
    }

    /**
     * Load filters from local store
     *
     * @param idTab Tab identifier
     */
    public loadFilters(idTab: string): any {
        const value = this.getItem('tabInfo_' + idTab);
        return value ? JSON.parse(value).filters : { };
    }

    /**
     * Load page from local store (return a new page if not found)
     *
     * @param idTab Tab identifier
     */
    public loadPage(idTab: string): Page {
        const value = this.getItem('tabInfo_' + idTab);
        const storeData = value ? JSON.parse(value) : null;
        return storeData && storeData.page ? storeData.page : new Page();
    }

    /**
     * Load sort from local store
     *
     * @param idTab Tab identifier
     */
    public loadSort(idTab: string): SortData {
        const value = this.getItem('tabInfo_' + idTab);
        const storeData = value ? JSON.parse(value) : null;
        return storeData && storeData.sortData ? storeData.sortData : null;
    }
}
