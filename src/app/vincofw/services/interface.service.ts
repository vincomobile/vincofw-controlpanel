import {IconDefinition} from '@fortawesome/fontawesome-common-types';

export interface FontAwesomeIcons {
    module: string;
    iconDefinition: IconDefinition[];
}

export interface AppLoadInterface {
    /**
     * Initial application load
     *
     * @param data Data load in previous load services
     */
    load(data: any): Promise<any>;

    /**
     * Add Font Awesome Icons to library
     */
    addFontAwesomeIcons(): void;
}
