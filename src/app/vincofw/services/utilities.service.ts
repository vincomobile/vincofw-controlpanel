import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpParams} from '@angular/common/http';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AdColumnDtoModel} from '../models/dto/ad-column-dto.model';
import {AdFieldDtoModel} from '../models/dto/ad-field-dto.model';
import {AdReferenceKeyValueModel} from '../models/dto/ad-reference-key-value.model';
import {AdReferenceDtoModel} from '../models/dto/ad-reference-dto.model';
import {AdTabDtoModel} from '../models/dto/ad-tab-dto.model';
import {AdTableActionDtoModel} from '../models/dto/ad-table-action-dto.model';
import {AdProcessParamDtoModel} from '../models/dto/ad-process-param-dto.model';
import {SortData} from "../models/page.model";
import {AdBaseModel} from "../models/ad/ad-base.model";
import {AdFormEditorModel} from "../models/ad/ad-form-editor.model";
import {ToolService} from "./tool.service";
import {AppCacheService} from './cache.service';
import {AutogenService} from './autogen.service';
import {AuthService} from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class UtilitiesService {

    constructor(
        private authService: AuthService,
        private cacheService: AppCacheService,
        private autogenService: AutogenService
    ) {

    }

    public static TABLES = {
        AdColumn: 'ff80808150dba1800150dba69dee0018',
        AdField: 'ff80808151382f2a0151385c9296000d',
        AdFieldGroup: 'ff80808151382f2a0151385d18e20024',
        AdProcessLog: 'ff808081514cd18b01514ce7b60f0018'
    };

    /**
     * Format server errors
     *
     * @param response Information error
     * @param allColumns Table fields
     * @param allFields All field list
     */
    public getValidationsErrors(response: HttpErrorResponse, allColumns: AdColumnDtoModel[], allFields: AdFieldDtoModel[]): string {
        let errMsg = '';
        if (response && 400 <= response.status && response.status < 500) {
            try {
                let errors = typeof response.error === 'string' ? JSON.parse(response.error) : response.error;
                let fieldErrors = errors.fieldErrors || [];
                let objectErrors = errors.objectErrors || [];
                if (!errors.fieldErrors && errors.id) {
                    const info = JSON.parse(errors.id);
                    fieldErrors = info.fieldErrors || [];
                    objectErrors = info.objectErrors || [];
                }
                if (fieldErrors.length === 0 && objectErrors.length === 0) {
                    errMsg = this.cacheService.getTranslation('AD_ErrValidationServerError');
                } else {
                    const msgCount = fieldErrors.length + objectErrors.length;
                    errMsg += msgCount > 1 ? '<ul>' : '';
                    fieldErrors.forEach(fldErr => {
                        const column = allColumns.find(col => {
                            return col.name === fldErr.field;
                        });
                        const extraMessageInfo = this.getServerFieldErrorExtraInfo(fldErr.message, column);
                        const field = column ? this.getFieldForColumn(column, allFields) : null;
                        errMsg +=
                            (msgCount > 1 ? '<li>' : '') +
                            this.getServerFieldError(fldErr.code, fldErr.message, ['<b style="color: yellow">' + (field ? field.caption : fldErr.field) + '</b>']) +
                            (extraMessageInfo ? '<div>' + extraMessageInfo + '</div>' : '') +
                            (msgCount > 1 ? '</li>' : '');
                    });
                    objectErrors.forEach(objErr => {
                        let msg = '';
                        if (objErr.code === 'Text') {
                            msg = objErr.message;
                        } else if (objErr.code === 'Message') {
                            msg = this.cacheService.getTranslation(objErr.message);
                        } else {
                            this.getServerObjectError(objErr.code);
                        }
                        errMsg += (msgCount > 1 ? '<li>' : '') + msg + (msgCount > 1 ? '</li>' : '');
                    });
                    errMsg += msgCount > 1 ? '</ul>' : '';
                }
            } catch (e) {
                errMsg = this.cacheService.getTranslation('AD_ErrValidationServerError');
            }
        } else {
            errMsg = this.getHttpServerError(response);
        }
        return errMsg;
    }

    /**
     * Get eror information about http errors
     *
     * @param error http error
     */
    public getHttpServerError(error: HttpErrorResponse): string {
        if (error && error.status) {
            console.error(`Server Error: ${error.statusText}`);
            switch (error.status) {
                case 400:
                    return this.cacheService.getTranslation('AD_HttpServerErrorInvalidRequest');

                case 401:
                    return this.cacheService.getTranslation('AD_HttpServerErrorAuthorizationRequired');

                case 403:
                case 404:
                case 408:
                case 410:
                    return this.cacheService.getTranslation('AD_HttpServerErrorNotFound');

                case 500:
                    return this.cacheService.getTranslation('AD_HttpServerErrorInternalServerError');

                case 502:
                case 503:
                case 504:
                    return this.cacheService.getTranslation('AD_HttpServerErrorGatewayTimeout');

                default:
                    return this.cacheService.getTranslation('AD_HttpServerErrorNotProcessed', [error.status, error.statusText]);
            }
        } else {
            return this.cacheService.getTranslation('AD_HttpServerErrorUnknow');
        }
    }

    /**
     * Get field link to column
     *
     * @param column Column
     * @param allFields All field list
     */
    public getFieldForColumn(column: AdColumnDtoModel, allFields: AdFieldDtoModel[]): AdFieldDtoModel {
        return allFields.find(fld => fld.idColumn === column.idColumn);
    }

    /**
     * Get column by name
     *
     * @param name Column name
     * @param allColumns Column list
     */
    public getColumnByName(name: string, allColumns: any[]): AdColumnDtoModel {
        return allColumns.find((col: AdColumnDtoModel | AdProcessParamDtoModel) => col.name === name);
    }

    /**
     * Get sorted visible columns in grid
     *
     * @param allColumns Column list
     * @param allFields Field list
     * @param actions Table actions list
     */
    public getGridColumns(allColumns: AdColumnDtoModel[], allFields: AdFieldDtoModel[], actions: AdTableActionDtoModel[]): AdColumnDtoModel[] {
        const visibleFields = allFields.filter(field => {
            return field.showingrid;
        });
        let visibleColumns = allColumns.filter(column => {
            return visibleFields.find(field => {
                return field.idColumn === column.idColumn;
            });
        });
        visibleColumns = visibleColumns.sort((first, second) => {
            if (first.relatedField.gridSeqno < second.relatedField.gridSeqno) {
                return -1;
            }
            if (first.relatedField.gridSeqno > second.relatedField.gridSeqno) {
                return 1;
            }
            return 0;
        });
        if (actions && actions.length > 0) {
            const actionsField = new AdFieldDtoModel();
            actionsField.caption = this.cacheService.getTranslation('AD_labelColumnActions');
            actionsField.sortable = false;
            const actionsRef = new AdReferenceDtoModel({
                idReference: '$actions$',
                rtype: 'ACTIONS'
            });
            const actionsCol = new AdColumnDtoModel();
            actionsField.relatedColumn = actionsCol;
            actionsCol.relatedField = actionsField;
            actionsCol.name = '$actions$';
            actionsCol.reference = actionsRef;
            visibleColumns.push(actionsCol);
        }
        return visibleColumns;
    }

    /**
     * Get name for identifier columns in grid
     *
     * @param allColumns Column list
     * @param allFields Field list
     */
    public getIdentifierColumn(allColumns: AdColumnDtoModel[], allFields: AdFieldDtoModel[]): string {
        let field;
        if (this.getColumnByName('identifier', allColumns)) {
            field = 'identifier';
        } else if (this.getColumnByName('name', allColumns)) {
            field = 'name';
        } else if (this.getColumnByName('value', allColumns)) {
            field = 'value';
        } else {
            const columns = this.getGridColumns(allColumns, allFields, null);
            field = columns[0].name;
        }
        return field;
    }

    /**
     * Get extra information for server field error
     *
     * @param error Error
     * @param column Column information
     */
    private getServerFieldErrorExtraInfo(error: string, column: AdColumnDtoModel): string {
        const errors = error.split('.');
        switch (errors[0]) {
            case 'Size':
                let message = '';
                switch (column.ctype){
                    case 'STRING':
                        if (column.lengthMin) {
                            message += ('<div>' + this.cacheService.getTranslation('AD_ErrValidationSizeLengthMin', [column.lengthMin]) + '</div>');
                        }
                        if (column.lengthMax) {
                            message += ('<div>' + this.cacheService.getTranslation('AD_ErrValidationSizeLengthMax', [column.lengthMax]) + '</div>');
                        }
                        break;
                    default:
                        if (column.relatedField.valuemin) {
                            message += ('<div>' + this.cacheService.getTranslation('AD_ErrValidationSizeMin', [column.relatedField.valuemin]) + '</div>');
                        }
                        if (column.relatedField.valuemax) {
                            message += ('<div>' + this.cacheService.getTranslation('AD_ErrValidationSizeMax', [column.relatedField.valuemax]) + '</div>');
                        }
                        break;
                }
                return message;
            default:
                return '';
        }
    }

    /**
     * Get server field error
     *
     * @param code Error code
     * @param error Error
     * @param args Message arguments
     */
    private getServerFieldError(code: string, error: string, args: any[]): string {
        const errors = code.split('.');
        switch (errors[0]) {
            case 'NotNull':
                return this.cacheService.getTranslation('AD_ErrValidationNotNull', args);
            case 'Email':
                return this.cacheService.getTranslation('AD_ErrValidationEmail', args);
            case 'Size':
                return this.cacheService.getTranslation('AD_ErrValidationSize', args);
            case 'Unique':
                return this.cacheService.getTranslation('AD_ErrValidationUnique', args);
            case 'FkNotFound':
                return this.cacheService.getTranslation('AD_ErrValidationFkNotFound', args);
            case 'FkNotDelete':
                return this.cacheService.getTranslation('AD_ErrValidationFkNotDelete', args);
            case 'DataTruncation':
                return this.cacheService.getTranslation('AD_ErrValidationDataTruncation', args);
            case 'UploadFile':
                return this.cacheService.getTranslation('AD_ErrValidationUploadFile', args);
            case 'UploadFileSize':
                args.push(errors[3]);
                return this.cacheService.getTranslation('AD_ErrValidationUploadFileSize', args);
            case 'UploadFileType':
                args.push(errors[3]);
                return this.cacheService.getTranslation('AD_ErrValidationUploadFileType', args);
            case 'CheckValue':
                return this.cacheService.getTranslation(error, args);
            case 'InvalidValue':
                return this.cacheService.getTranslation(error, errors.splice(4, errors.length - 3));
        }
        return error;
    }

    /**
     * Get server object error
     *
     * @param error Error
     */
    private getServerObjectError(error: string): string {
        switch (error) {
            case 'Unique':
                return this.cacheService.getTranslation('AD_ErrValidationContraintUnique');
            case 'InvalidValue':
                return this.cacheService.getTranslation('AD_ErrValidationInvalidValue');
            case 'Unknow':
                return this.cacheService.getTranslation('AD_ErrValidationUnknow');
        }
        return error;
    }

    /**
     * Build form control
     *
     * @param rtype Reference type
     * @param mandatory Mandatory validator
     * @param valuemin Min value validator
     * @param valuemax Max value validator
     * @param lengthMin Min length validator
     * @param lengthMax Max length validator
     */
    public buildFormControl(rtype: string, mandatory: boolean, valuemin: number, valuemax: number, lengthMin: number, lengthMax: number): FormControl {
        const validators = [];
        const formCtrl = new FormControl();
        // Validator: Required
        if (mandatory && rtype !== 'PASSWORD') {
            validators.push(Validators.required);
        }
        // Validator: Min and Max
        if (rtype === 'INTEGER' || rtype === 'NUMBER') {
            if (!ToolService.isNullOrUndefined(valuemin)) {
                validators.push(Validators.min(Number(valuemin)));
            }
            if (!ToolService.isNullOrUndefined(valuemax)) {
                validators.push(Validators.max(Number(valuemax)));
            }
        }
        // Validator: Min length and Max length
        if (rtype === 'STRING' || rtype === 'TEXT' || rtype === 'HTML' || rtype === 'EMAIL' || rtype === 'PASSWORD') {
            if (!ToolService.isNullOrUndefined(lengthMin)) {
                validators.push(Validators.minLength(lengthMin));
            }
            if (!ToolService.isNullOrUndefined(lengthMax)) {
                validators.push(Validators.maxLength(lengthMax));
            }
        }
        if (validators.length > 0) {
            formCtrl.setValidators(validators);
        }
        return formCtrl;
    }

    /**
     * Get value using the refence type
     *
     * @param reference Reference
     * @param value Value
     */
    public getValueByReference(reference: AdReferenceDtoModel, value): string {
        switch (reference.rtype) {
            case 'DATE':
                return ToolService.getMomentDate(value).format('DD/MM/YYYY');

            case 'TIME':
                return ToolService.getMomentDate(value).format('HH:mm:ss');

            case 'TIMESTAMP':
                return ToolService.getMomentDate(value).format('DD/MM/YYYY HH:mm:ss');

            case 'PASSWORD':
                return '*******';
        }
        return value;
    }

    /**
     * Get form value to send to server
     *
     * @param value Form value
     * @param field Related field
     */
    public getFormValue(value: any, field: AdFieldDtoModel): any {
        if (ToolService.isNullOrUndefined(value)) {
            return null;
        }
        const reference = field.relatedColumn.reference;
        switch (reference.rtype) {
            case 'DATE':
                const date = ToolService.getMomentDate(value);
                return date ? date.format('YYYY-MM-DD') : null;

            case 'TIME':
                const time = ToolService.getMomentDate(value);
                return time ? time.format('HH:mm:ss') : null;

            case 'TIMESTAMP':
                const timestamp = ToolService.getMomentDate(value);
                return timestamp ? timestamp.format('YYYY-MM-DDTHH:mm:ss.SSS') + 'Z' : null;

            case 'PASSWORD':
                return ToolService.encryptPassword(`${this.authService.getLoggedIdClient()}/${this.authService.getLoggedIdUser()}`, value);

            case 'TABLEDIR':
                if (field.filterType === 'MULTISELECT') {
                    return value ? value.join(';') : null;
                }
                return value && value.trim() !== '' ? value : null;

            case 'TABLE':
                if (typeof value === 'string') {
                    return value && value.trim() !== '' ? value : null;
                }
                return value.key && value.key.trim() !== '' ? value.key : null;

            case 'LIST':
                if (reference.multiple) {
                    if (field.readonly) {
                        return value;
                    }
                    return value.join(',');
                }
                if (field.readonly) {
                    const refValue = reference.refList.find(rl => rl.name === value);
                    if (refValue) {
                        return refValue.value;
                    }
                }
                return value;

        }
        if (ToolService.isEmptyValue(value) && !field.relatedColumn.mandatory) {
            value = null;
        }
        return value;
    }

    /**
     * Set form value
     *
     * @param autogenForm Form
     * @param key Form field
     * @param reference Reference
     * @param readonly Read only field
     * @param currentItem Current form values
     * @param relatedColumn Related column
     */
    public setFormValue(autogenForm: FormGroup, key: string, reference: AdReferenceDtoModel, readonly: boolean, currentItem: any, relatedColumn: AdColumnDtoModel): Promise<void> {
        return new Promise((resolve) => {
            switch (reference.rtype) {
                case 'TIME':
                case 'DATE':
                case 'TIMESTAMP':
                    const value = ToolService.getMomentDate(currentItem[key]);
                    if (value != null && readonly) {
                        const fmt = reference.rtype === 'TIME' ? 'HH:mm:ss' : (reference.rtype === 'DATE' ? 'DD/MM/YYYY' : 'DD/MM/YYYY HH:mm:ss');
                        autogenForm.get(key).setValue(value.format(fmt));
                    } else {
                        autogenForm.get(key).setValue(reference.rtype === 'TIME' ? (value ? value.format('HH:mm') : '') : value);
                    }
                    resolve();
                    break;

                case 'PASSWORD':
                    autogenForm.get(key).setValue('');
                    resolve();
                    break;

                case 'TABLE':
                    if (typeof currentItem[key] === 'string') {
                        this.cacheService.getReferenceValue(currentItem[key], reference.idReference, currentItem, relatedColumn).then(value => {
                            if (readonly) {
                                autogenForm.get(key).setValue(value);
                            } else {
                                const option = new AdReferenceKeyValueModel();
                                option.key = currentItem[key];
                                option.value = currentItem[key] ? value : '';
                                autogenForm.get(key).setValue(option);
                            }
                            resolve();
                        });
                    } else {
                        autogenForm.get(key).setValue(currentItem[key]);
                        resolve();
                    }
                    break;

                case 'TABLEDIR':
                case 'LIST':
                    if (readonly) {
                        this.cacheService.getReferenceValue(currentItem[key], reference.idReference, currentItem, relatedColumn).then(value => {
                            autogenForm.get(key).setValue(value);
                            resolve();
                        });
                    } else {
                        if (reference.multiple && currentItem[key]) {
                            autogenForm.get(key).setValue(currentItem[key].split(','));
                        } else {
                            autogenForm.get(key).setValue(currentItem[key]);
                        }
                        resolve();
                    }
                    break;

                default:
                    autogenForm.get(key).setValue(currentItem[key]);
                    resolve();
            }
        });
    }

    /**
     * Get filters condition
     *
     * @param filters Filters
     * @param columns Column references
     * @param strict Allways filter exact value
     */
    public getFilterParams(filters: any, columns: any[], strict: boolean = false): string {
        let q = '';
        if (!ToolService.isNullOrUndefined(filters)) {
            // Single filters
            for (let current in filters) {
                if (filters.hasOwnProperty(current)) {
                    if (!ToolService.isEmptyValue(filters[current])) {
                        let singleFilter = true;
                        let value = filters[current];
                        let column;
                        if (current.indexOf('$from$') === -1 && current.indexOf('$to$') === -1) {
                            column = this.getColumnByName(current, columns);
                            if (!ToolService.isNullOrUndefined(column) && (column.ranged || (column.relatedField && column.relatedField.filterRange))) {
                                singleFilter = false;
                            }
                        } else {
                            singleFilter = false;
                        }

                        if (singleFilter) {
                            // Single filters
                            if (current === '$search$') {
                                value = strict ? value : '%' + value + '%';
                            } else {
                                if (!ToolService.isNullOrUndefined(column)) {
                                    switch (column.reference.rtype) {
                                        case 'STRING':
                                        case 'TEXT':
                                        case 'HTML':
                                            value = strict ? value : '%' + value + '%';
                                            break;

                                        case 'TABLE':
                                            value = typeof value === 'string' ? value : value.key;
                                            break;

                                        case 'DATE':
                                            value = ToolService.getMomentDate(value).format('DD/MM/YYYY');
                                            break;

                                        case 'TIME':
                                        case 'TIMESTAMP':
                                            value = ToolService.getMomentDate(value).format('DD/MM/YYYYTHH:mm:ss');
                                            break;

                                        default:
                                            if (column.relatedField && column.relatedField.filterType === 'MULTISELECT'
                                                && (column.reference.rtype === 'LIST' || column.reference.rtype === 'TABLEDIR')
                                            ) {
                                                value = value.length === 0 ? null :  '[' + (value instanceof Array ? value.join(';') : value) + ']';
                                            }
                                    }
                                } else {
                                    value = null;
                                }
                            }
                            if (!ToolService.isEmptyValue(value)) {
                                q += (q === '' ? '' : ',') + (current === '$search$' ? 'search' : current) + '=' + value;
                            }
                        } else if (!ToolService.isNullOrUndefined(column)) {
                            // Range filters
                            let values = value.split('~');
                            if (values.length < 2) {
                                values.push('');
                            }
                            values[0] = values[0].trim();
                            values[1] = values[1].trim();
                            if (values[0] !== '' || values[1] !== '') {
                                q += (q === '' ? '' : ',') + column.name + '=' +  values[0] + '~' + values[1];
                            }
                        }
                    }
                }
            }
            // Range filters for dates and timestamp
            columns.filter(col => col.relatedField && col.relatedField.filterRange).forEach(column => {
                if (column.reference.rtype === 'DATE' || column.reference.rtype === 'TIMESTAMP') {
                    let value = null;
                    const rangeFrom = filters['$from$' + column.name];
                    const rangeTo = filters['$to$' + column.name];
                    if (!ToolService.isEmptyValue(rangeFrom) || !ToolService.isEmptyValue(rangeTo)) {
                        if (!ToolService.isEmptyValue(rangeFrom)) {
                            if (column.reference.rtype === 'DATE' || column.reference.rtype === 'TIMESTAMP') {
                                value = ToolService.getMomentDate(rangeFrom).format('DD/MM/YYYY') + '~';
                            } else {
                                value = rangeFrom + '~';
                            }
                        }
                        if (!ToolService.isEmptyValue(rangeTo)) {
                            if (value === null) {
                                value = '~';
                            }
                            if (column.reference.rtype === 'DATE' || column.reference.rtype === 'TIMESTAMP') {
                                value += ToolService.getMomentDate(rangeTo).format('DD/MM/YYYY');
                            } else {
                                value += rangeTo;
                            }
                        }
                        q += (q === '' ? '' : ',') + column.name + '=' + value;
                    }
                }
            });
        }
        return q;
    }

    /**
     * Check if column is filtered by range
     *
     * @param column Column information
     */
    public isColumnRangeFilter(column: AdColumnDtoModel): boolean {
        return column.relatedField.filterRange
            && (column.reference.rtype === 'DATE' || column.reference.rtype === 'TIMESTAMP');
    }

    /**
     * Add a field to tab
     *
     * @param tab Tab information
     * @param field Field information
     * @param column Column information
     */
    public addTabField(tab: AdTabDtoModel, field: AdFieldDtoModel, column: AdColumnDtoModel): void {
        if (column) {
            field.relatedColumn = column;
            tab.table.columns.push(column);
        }
        tab.fields.push(field);
        if (field.idFieldGroup === null) {
            tab.ungroupedFields.push(field);
        } else {
            const group = tab.fieldGroups.find(grp => grp.idFieldGroup === field.idFieldGroup);
            if (group) {
                group.fields.push(field);
            } else {
                console.error('Not found group: ' + field.idFieldGroup);
            }
        }
    }

    /** Load table content
     *
     * @param idTable Table identifier
     * @param params Query parameters
     * @param sortOrder Sort criteria
     */
    public loadTableContent(idTable: string, params: any, sortOrder?: SortData[]): Promise<AdBaseModel[]> {
        return new Promise((resolve, reject) => {
            this.cacheService.loadTableInfo(idTable).subscribe(table => {
                if (table) {
                    const autogen = this.autogenService.bindedService(table);
                    let parameters = new HttpParams();
                    for (let objKey in params) {
                        if (params.hasOwnProperty(objKey)) {
                            parameters = parameters.append(objKey, params[objKey]);
                        }
                    }
                    autogen.list(parameters, sortOrder).subscribe(response => {
                        resolve(response.content);
                    });
                } else {
                    console.error('Not found table: ' + idTable);
                    reject(this.cacheService.getTranslation('AD_ProcessTableErrId', [idTable]));
                }
            });
        });
    }

    /**
     * Load table row
     *
     * @param idTable Table identifier
     * @param idRow Row identifier
     */
    public loadTableRow(idTable, idRow): Promise<any> {
        return new Promise((resolve, reject) => {
            if (ToolService.isNullOrUndefined(idRow)) {
                resolve(null);
            }
            this.cacheService.loadTableInfo(idTable).subscribe(table => {
                if (table) {
                    const autogen = this.autogenService.bindedService(table);
                    autogen.get(idRow).subscribe(response => {
                        resolve(response);
                    });
                } else {
                    console.error('Not found table: ' + idTable);
                    reject(this.cacheService.getTranslation('AD_ProcessTableErrId', [idTable]));
                }
            });
        });
    }

    /**
     * Get item values to save
     *
     * @param form Form
     * @param item Model
     */
    public getItemToSave(form: FormGroup, item: any): any {
        form.updateValueAndValidity();
        for (let objKey in form.controls) {
            if (form.controls.hasOwnProperty(objKey)) {
                form.controls[objKey].markAsTouched();
                form.controls[objKey].markAsDirty();
            }
        }
        if (form.valid) {
            const itemToSave: any = {};
            for (let objKey in item) {
                if (item.hasOwnProperty(objKey)) {
                    if (form.get(objKey) !== null) {
                        itemToSave[objKey] = form.value[objKey];
                    } else {
                        itemToSave[objKey] = item[objKey];
                    }
                }
            }
            return itemToSave;
        }
        return null;
    }

    /**
     * Add field dependency
     *
     * @param editor Editor
     * @param deps Dependencies
     * @param property Dependency type
     */
    public addFieldDependency(editor: AdFormEditorModel | AdFieldDtoModel, deps: string[], property: string): void {
        deps.forEach(dep => {
            if (!editor[property].find(lnst => dep === lnst)) {
                editor[property].push(dep);
            }
        });
    }

    /**
     * Initialize form with item model values
     *
     * @param form Form
     * @param item Model
     */
    public initFormValues(form: FormGroup, item: any): void {
        const value = {};
        for (let objKey in form.controls) {
            if (form.controls.hasOwnProperty(objKey)) {
                value[objKey] = item[objKey];
            }
        }
        form.patchValue(value);
    }

}
